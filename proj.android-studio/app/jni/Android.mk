LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos/audio/include)
$(call import-add-path,$(LOCAL_PATH))

LOCAL_MODULE := MyGame_shared

LOCAL_MODULE_FILENAME := libMyGame

LOCAL_SRC_FILES := hellocpp/main.cpp \
../../../Classes/AppDelegate.cpp \
../../../Classes/Engine/Service/Shop/GameStore.cpp \
../../../Classes/Engine/Service/Shop/GameStoreProduct.cpp \
../../../Classes/Engine/Service/GameService/StateManager.cpp \
../../../Classes/Engine/Service/GameService/GooglePlayServicesImpl.cpp \
../../../Classes/Engine/Service/GameService/PluginGPGCppHelper.cpp \
../../../Classes/Engine/JsonParser/JsonParser.cpp \
../../../Classes/Engine/Audio/SimpleAudioEngineWrapper.cpp \
../../../Classes/Engine/Locator/UILocator.cpp \
../../../Classes/Engine/Locator/ControllerLocator.cpp \
../../../Classes/Engine/Locator/ServiceLocator.cpp \
../../../Classes/Engine/Controller/GameController.cpp \
../../../Classes/Engine/Controller/InputController.cpp \
../../../Classes/Engine/Controller/ConfigController.cpp \
../../../Classes/Engine/Controller/DaoController.cpp \
../../../Classes/Engine/Singleton/EngineSingleton.cpp \
../../../Classes/Engine/Utils/Observer.cpp \
../../../Classes/Engine/Utils/GameEvent.cpp \
../../../Classes/Engine/Factory/GameEventFactory.cpp \
../../../Classes/UI/DashboardUI.cpp \
../../../Classes/UI/PopUpUI.cpp \
../../../Classes/UI/ShopUI.cpp \
../../../Classes/UI/GameBoardUI.cpp \
../../../Classes/UI/CardSelectUI.cpp \
../../../Classes/UI/CardSelectWaitUI.cpp \
../../../Classes/UI/HudUI.cpp \
../../../Classes/UI/RewardUI.cpp \
../../../Classes/UI/ResultUI.cpp \
../../../Classes/UI/LoadingUI.cpp \
../../../Classes/UI/LoadingSpinnerUI.cpp \
../../../Classes/UI/ChestSlotUI.cpp \
../../../Classes/UI/ConfirmPurchaseUI.cpp \
../../../Classes/UI/ResourceDisplayUI.cpp \
../../../Classes/UI/CardCollectionUI.cpp \
../../../Classes/UI/ChestDetailsPopUpUI.cpp \
../../../Classes/Game/GameBoard.cpp \
../../../Classes/Game/Player.cpp \
../../../Classes/Game/Card.cpp \
../../../Classes/Game/CardCollection.cpp \
../../../Classes/Game/Chest/Chest.cpp \
../../../Classes/Game/Chest/SilverChest.cpp \
../../../Classes/Game/Chest/FreeChest.cpp \
../../../Classes/Game/Chest/ChestSlot.cpp \
../../../Classes/Game/Factory/ChestFactory.cpp \
../../../Classes/Game/Factory/CardFactory.cpp

LOCAL_CPPFLAGS := -DSDKBOX_ENABLED
LOCAL_LDLIBS := -landroid \
-llog
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../Classes
LOCAL_WHOLE_STATIC_LIBRARIES := PluginGPG \
sdkbox \
gpg-1 \
PluginGoogleAnalytics \
PluginIAP \
android_native_app_glue

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
$(call import-module, ./sdkbox)
$(call import-module, ./plugingpg)
$(call import-module, ./gpg)
$(call import-module, ./plugingoogleanalytics)
$(call import-module, ./pluginiap)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
