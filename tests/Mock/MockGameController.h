//
//  MockGameController.h
//  Argon
//
//  Created by David Morton on 19/01/2017.
//
//

#ifndef MockGameController_h
#define MockGameController_h

#include "gmock/gmock.h"
#include "Base/GameDefines.h"
#include "Controller/IGameController.h"

class MockGameController : public IGameController {
public:
  MOCK_METHOD0(Update, void());
  MOCK_METHOD0(GetGameTimeSeconds, int());
};

#endif /* MockGameController_h */
