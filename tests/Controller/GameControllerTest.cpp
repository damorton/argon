// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "Engine/Controller/GameController.h"

using ::testing::Return;

TEST(GameControllerTest, GetGametimeMilliseconds) {
  GameController gameController;
  int gameTimeSeconds = gameController.GetGameTimeSeconds();
  ASSERT_EQ(kGameSessionTimeSeconds, gameTimeSeconds);
}
