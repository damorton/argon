// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "gtest/gtest.h"
#include "Engine/Utils/Utils.h"

TEST(UtilsTest, to_str_CanConvertIntZeroToString) {
  std::string result = Utils::to_str(0);
  ASSERT_EQ("0", result);
}

TEST(UtilsTest, to_str_CanConvertPositiveIntToString) {
  std::string result = Utils::to_str(1);
  ASSERT_EQ("1", result);
}

TEST(UtilsTest, to_str_CanConvertNegativeIntToString) {
  std::string result = Utils::to_str(-1);
  ASSERT_EQ("-1", result);
}

TEST(UtilsTest, to_str_CanConvertFloatToString) {
  float floatNum = 123.987;
  std::string result = Utils::to_str(floatNum);
  ASSERT_EQ("123.987", result);
}

TEST(UtilsTest, to_str_CanConvertDoubleToString) {
  double doubleNum = 123.987;
  std::string result = Utils::to_str(doubleNum);
  ASSERT_EQ("123.987", result);
}

TEST(UtilsTest, to_int_CanConvertZeroStringToInt) {
  int result = Utils::to_int("0");
  ASSERT_EQ(0, result);
}

TEST(UtilsTest, to_int_CanConvertPositiveStringToInt) {
  int result = Utils::to_int("1");
  ASSERT_EQ(1, result);
}

TEST(UtilsTest, to_int_CanConvertNegativeStringToInt) {
  int result = Utils::to_int("-1");
  ASSERT_EQ(-1, result);
}

TEST(UtilsTest, vec_to_string_CanConvertVecToString) {
  std::vector<uint8_t> vectorOfBytes = {'t', 'e', 's', 't'};
  std::string result = Utils::vec_to_string(vectorOfBytes);
  ASSERT_EQ("test", result);
}

TEST(UtilsTest, str_to_vector_CanConvertStringToVec) {
  std::string stringValue = "test";
  std::vector<uint8_t> result = Utils::str_to_vector(stringValue);
  ASSERT_EQ(4, result.size());
}

TEST(UtilsTest, milli_to_sec_CanConvertPositiveUnsignedLongLongToSeconds) {
  unsigned long long milliseconds = 30000;
  unsigned long long result = Utils::milli_to_sec(milliseconds);
  ASSERT_EQ(30, result);
}

TEST(UtilsTest, milli_to_sec_CanConvertZeroToSeconds) {
  unsigned long long milliseconds = 0;
  unsigned long long result = Utils::milli_to_sec(milliseconds);
  ASSERT_EQ(0, result);
}
