// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "gtest/gtest.h"
#include "Game/GameSession.h"

TEST(GameSessionTest, testCanSavePositiveGoldInSession) {
  GameSession gameSession;
  gameSession.SaveGold(50);
  int result = gameSession.Gold();
  ASSERT_EQ(50, result);
}

TEST(GameSessionTest, testCannotSaveNegativeGoldInSession) {
  GameSession gameSession;
  gameSession.SaveGold(-50);
  int result = gameSession.Gold();
  ASSERT_TRUE(result >= 0);
  ASSERT_EQ(0, result);
}

TEST(GameSessionTest, testCannotSaveNegativeGoldInSessionWhenBalanceGreatherThanZero) {
  GameSession gameSession;
  gameSession.SaveGold(50);
  int result = gameSession.Gold();
  ASSERT_TRUE(result > 0);
  gameSession.SaveGold(-70);
  result = gameSession.Gold();
  ASSERT_EQ(50, result);
}

TEST(GameSessionTest, SaveAndReadGemsPositiveTest) {
  GameSession gameSession;
  gameSession.SaveGems(25);
  int result = gameSession.Gems();
  ASSERT_EQ(25, result);
}

TEST(GameSessionTest, SaveAndReadGemsNegativeTest) {
  GameSession gameSession;
  gameSession.SaveGems(-25);
  int result = gameSession.Gems();
  ASSERT_EQ(0, result);
}

TEST(GameSessionTest, SaveGameTimeMilliSecondsTest) {
  GameSession gameSession;
  gameSession.SaveGameTimeMilliseconds(45000);
  unsigned long long result = gameSession.GameTimeMilliseconds();
  ASSERT_EQ(45000, result);
}

TEST(GameSessionTest, GameTimeSecondsTest) {
  GameSession gameSession;
  gameSession.SaveGameTimeMilliseconds(45000);
  int result = gameSession.GameTimeSeconds();
  ASSERT_EQ(45, result);
}

TEST(GameSessionTest, SerializeGameSessionTest) {
  GameSession gameSession;
  gameSession.SaveGold(50);
  gameSession.SaveGems(10);
  gameSession.SaveGameTimeMilliseconds(45000);
  std::string expectedResult = "{\"gamesession\":[{\"gold\":50},{\"gems\":10},{\"gametime\":45}]}";
  std::string result = gameSession.SerializeGameSession();
  ASSERT_EQ(expectedResult, result);
}
