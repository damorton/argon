# Argon

Cocos2dx game written in C++ for iOS / Android

Design: Rebecca Maxwell @becamaxwell  
Programming: David Morton @Beautifwhale  
Art: -

# Environment

Install dependencies in the order listed

- NDK r10c
- Android SDK
- Cocos2dx v3.10

## NDK r10c

- Download NDK r10c

```
ndk_r10c(October 2014)

Windows 32-bit : http://dl.google.com/android/ndk/android-ndk-r10c-windows-x86.exe

Windows 64-bit : http://dl.google.com/android/ndk/android-ndk-r10c-windows-x86_64.exe

Mac OS X 32-bit : http://dl.google.com/android/ndk/android-ndk-r10c-darwin-x86.bin

Mac OS X 64-bit : http://dl.google.com/android/ndk/android-ndk-r10c-darwin-x86_64.bin

Linux 32-bit (x86) : http://dl.google.com/android/ndk/android-ndk-r10c-linux-x86.bin

Linux 64-bit (x86) : http://dl.google.com/android/ndk/android-ndk-r10c-linux-x86_64.bin
```

- Unzip to `dev` directory

## Android SDK

- Download Android SDK tools ONLY, no need for Android Studio (Bottom of the page) https://developer.android.com/studio/index.html
- On the command line run `android` to open the SDK Manager

- Deselect all and select
```
Tools
- Android SDK tools
- Android SDK Build-tools
Android 5.0.1
- SDK Platform
- ARM EABI v7a System Image
Extras
- Google USB Driver (Windows)
```

## Cocos2dx v3.10

- Download Cocos2dx v3.10 http://www.cocos2d-x.org/download/version
- Unzip to `dev` directory
- `cd dev` and run `setup.py`

# Building

- Connect Android device to PC/Laptop
- (Command line) Navigate to the project directory `cd <project-directory>`
- Run `cocos run -p android -m release`
- APK will be built into `Project/bin/release/android/`
