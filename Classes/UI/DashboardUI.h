// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_DASHBOARD_UI_H_
#define UI_DASHBOARD_UI_H_

#include "cocos/cocos2d.h"
#ifdef SDKBOX_ENABLED
#include "PluginGPG/PluginGPG.h"
#endif
#include "UI/ChestSlotUI.h"
#include "SimpleAudioEngine.h"
#include "Engine/Utils/Observer.h"

class ResourceDisplayUI;

class DashboardUI : public cocos2d::Layer, public Observer {
public:
  static cocos2d::Scene* createScene();
  virtual bool init() override;
  virtual void onEnterTransitionDidFinish() override;
  void menuCloseCallback(cocos2d::Ref* pSender);
  void ActivatePlayButton();
  virtual void OnNotify(GameEvent *event) override;
  
  CREATE_FUNC(DashboardUI);
private:
  void Shop(cocos2d::Ref *sender);
  void Play(cocos2d::Ref *sender);
  void Delete(cocos2d::Ref *sender);
  void Quit(cocos2d::Ref *sender);
  void CardCollection(cocos2d::Ref *sender);
  void SendReliableMsg(cocos2d::Ref *sender);
  void update(float dt) override;
  void DeactivatePlayButton();

  ResourceDisplayUI *resourceLabelContainer;
  
  cocos2d::Sprite *chestSlotOne;
  cocos2d::Sprite *chestSlotTwo;
  cocos2d::Sprite *chestSlotThree;
  cocos2d::Sprite *chestSlotFour;
  
  float resourceLabelHeight;
  
  cocos2d::Label *messageLabel;
  cocos2d::MenuItemLabel *playButtonLabel;
  cocos2d::MenuItemLabel *deleteButtonLabel;
  cocos2d::MenuItemLabel *quitButtonLabel;
  cocos2d::MenuItemLabel *viewCardCollectionButtonLabel;
  cocos2d::Size visibleSize;
  cocos2d::Vec2 origin;
  
  ChestSlotUI *chestSlotUI;
};

#endif 
