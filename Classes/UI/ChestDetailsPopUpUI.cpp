// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/ChestDetailsPopUpUI.h"
#include "Engine/Singleton/EngineSingleton.h"
#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#endif
#include "Game/Chest/Chest.h"
#include "Game/Chest/ChestSlot.h"

void ChestDetailsPopUpUI::PopUpInit() {
  auto backgroundSprite = Sprite::create(kGreyTransparentBackground);
  backgroundSprite->setPosition(screenCentre);
  this->addChild(backgroundSprite);
  
  // Open with gems button
  openWithGemsButton = MenuItemImage::create(kOpenWithGemsButtonBackgroundImage, kOpenWithGemsButtonBackgroundImage, CC_CALLBACK_1(ChestDetailsPopUpUI::OpenWithGems, this));
  openWithGemsButton->setPosition((innerPopupSprite->getContentSize().width / 2) - (openWithGemsButton->getContentSize().width / 2) - kPaddingSmall,
                                  -(innerPopupSprite->getContentSize().height / 2) + (openWithGemsButton->getContentSize().height / 2) + kPaddingSmall);
  
  const float openWithGemsButtonContentPercentagePosX = 0.35;
  auto openWithGemsButtonLabel = cocos2d::Label::createWithTTF("OPEN NOW", kFontType, kFontSizeSmall);
  openWithGemsButtonLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  openWithGemsButtonLabel->setPosition(openWithGemsButton->getContentSize().width * openWithGemsButtonContentPercentagePosX, openWithGemsButton->getContentSize().height * 0.75 );
  openWithGemsButton->addChild(openWithGemsButtonLabel);
  
  openWithGemsCostLabel = cocos2d::Label::createWithTTF("0", kFontType, kFontSizeMedium);
  openWithGemsCostLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  openWithGemsCostLabel->setPosition(openWithGemsButton->getContentSize().width * openWithGemsButtonContentPercentagePosX, openWithGemsButton->getContentSize().height * 0.37);
  openWithGemsButton->addChild(openWithGemsCostLabel);
  
  // Unlock timer button
  startUnlockTimerButton = MenuItemImage::create(kStartUnlockTimerButtonBackgroundImage, kStartUnlockTimerButtonBackgroundImage, CC_CALLBACK_1(ChestDetailsPopUpUI::StartUnlockTimer, this));
  startUnlockTimerButton->setPosition(-(innerPopupSprite->getContentSize().width / 2) + (startUnlockTimerButton->getContentSize().width / 2) + kPaddingSmall,
                                      -(innerPopupSprite->getContentSize().height / 2) + (startUnlockTimerButton->getContentSize().height / 2) + kPaddingSmall);
  
  const float startUnlockTimerButtonContentPercentagePosX = 0.50;
  startUnlockTimerButtonLabel = cocos2d::Label::createWithTTF("START UNLOCK", kFontType, kFontSizeSmallPlus);
  startUnlockTimerButtonLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  startUnlockTimerButtonLabel->setPosition(startUnlockTimerButton->getContentSize().width * startUnlockTimerButtonContentPercentagePosX, startUnlockTimerButton->getContentSize().height * 0.75);
  startUnlockTimerButton ->addChild(startUnlockTimerButtonLabel);
  
  startUnlockTimeLabel = cocos2d::Label::createWithTTF("00h 00min", kFontType, kFontSizeMedium);
  startUnlockTimeLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  startUnlockTimeLabel->setPosition(startUnlockTimerButton->getContentSize().width * startUnlockTimerButtonContentPercentagePosX, startUnlockTimerButton->getContentSize().height * 0.37);
  startUnlockTimerButton->addChild(startUnlockTimeLabel);
  
  auto menu = cocos2d::Menu::create(openWithGemsButton, startUnlockTimerButton, nullptr);
  this->addChild(menu);
  
  // Time remaining
  timeRemainingLabel = cocos2d::Label::createWithTTF("00h 00min", kFontType, kFontSizeSmallPlus);
  timeRemainingLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  cocos2d::Point timeRemainingLabelPosition = cocos2d::Point(screenCentre.x -(innerPopupSprite->getContentSize().width / 2) +
                                                             (timeRemainingLabel->getContentSize().width / 2) +
                                                             kPaddingSmall,
                                                             screenCentre.y -(innerPopupSprite->getContentSize().height / 2) +
                                                             (openWithGemsButton->getContentSize().height / 2) +
                                                             kPaddingSmall);
  timeRemainingLabel->setPosition(startUnlockTimerButton->getContentSize().width * 0.50, startUnlockTimerButton->getContentSize().height * 0.50);
  timeRemainingLabel->setVisible(false);
  startUnlockTimerButton->addChild(timeRemainingLabel);
  
  
  auto touchListener = EventListenerTouchOneByOne::create();
  touchListener->setSwallowTouches(true);
  touchListener->onTouchBegan = CC_CALLBACK_2(ChestDetailsPopUpUI::onTouchBegan, this);
  touchListener->onTouchEnded = CC_CALLBACK_2(ChestDetailsPopUpUI::onTouchEnded, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
  
  selectedChest = nullptr;
  
  CCLOG("ChestDetailsWidgetUI initialised");
}

void ChestDetailsPopUpUI::Update() {
  if(selectedChest != nullptr) {
    if(selectedChest->GetState() == Chest::CHEST_UNLOCKED) {
      timeRemainingLabel->setString(Utils::seconds_to_time_format(selectedChest->TimeRemainingSeconds()));
    }
  }
}

bool ChestDetailsPopUpUI::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) {
  return true;
}

void ChestDetailsPopUpUI::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event* event) {
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  if(!innerPopupSprite->getBoundingBox().containsPoint(touch->getLocation())){
    this->setVisible(false);
  }
}

void ChestDetailsPopUpUI::SetSelectedChestSlot(ChestSlot *chestSlot) {
  selectedChestSlot = chestSlot;
  selectedChest = chestSlot->CurrentChest().get();
  CloneChestImage();
  UpdateOpenWithGemsButton();
  if(selectedChest->GetState() == Chest::CHEST_LOCKED) {
    DisplayStartUnlockTimerButton();
  } else {
    DisplayRemainingTimeLabel();
  }
}

void ChestDetailsPopUpUI::CloneChestImage() {
  auto chestImage =  cocos2d::Sprite::createWithSpriteFrame(selectedChest->Sprite()->getSpriteFrame());
  chestImage->setPosition(screenCentre.x, screenCentre.y + innerPopupSprite->getContentSize().height / 2);
  chestImage->setScale(0.5f);
  this->addChild(chestImage);
}

void ChestDetailsPopUpUI::UpdateOpenWithGemsButton() {
  int costToOpen = selectedChest->CostToOpen();
  openWithGemsCostLabel->setString(Utils::to_str(costToOpen));
  int currentGems = Utils::to_int(ControllerLocator::GetConfigController()->ConfigurationValue(kGems));
  if(costToOpen > currentGems){
    openWithGemsCostLabel->setColor(cocos2d::Color3B::RED);
  }
}

void ChestDetailsPopUpUI::DisplayStartUnlockTimerButton() {
  std::string timeToUnlock = Utils::seconds_to_time_format(selectedChest->UnlockWaitTimeSeconds());
  startUnlockTimeLabel->setString(timeToUnlock);
  startUnlockTimeLabel->setVisible(true);
}

void ChestDetailsPopUpUI::DisplayRemainingTimeLabel() {
  timeRemainingLabel->setVisible(true);
  startUnlockTimerButton->setEnabled(false);
  startUnlockTimerButtonLabel->setVisible(false);
  startUnlockTimeLabel->setVisible(false);
}

void ChestDetailsPopUpUI::OpenWithGems(cocos2d::Ref *sender) {
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  CCLOG("Open with Gems");
  if(ControllerLocator::GetConfigController()->CheckGemAmount(selectedChest->CostToOpen())) {
    ControllerLocator::GetConfigController()->UpdatePlayerGems(-selectedChest->CostToOpen());
    selectedChest->SetReadyToOpen();
    this->setVisible(false);
    
    std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::EVENT_SAVE_GAME_CONFIG);
    EngineSingleton::GetInstance()->Notify(gameEvent.get());
    
    gameEvent = GameEventFactory::Create(GameEvent::EVENT_GAME_MODEL_UPDATED);
    EngineSingleton::GetInstance()->Notify(gameEvent.get());
  }
}

void ChestDetailsPopUpUI::StartUnlockTimer(cocos2d::Ref *sender) {
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  CCLOG("Starting unlock timer");
  if(selectedChest) {
    selectedChest->Unlock();
    this->setVisible(false);
  }
}
