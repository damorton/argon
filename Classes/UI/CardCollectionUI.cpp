// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/CardCollectionUI.h"
#include "Engine/Singleton/EngineSingleton.h"

Scene* CardCollectionUI::createScene() {
  // 'scene' is an autorelease object
  auto scene = Scene::create();

  // 'layer' is an autorelease object
  auto layer = CardCollectionUI::create();

  // add layer as a child to scene
  scene->addChild(layer);

  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool CardCollectionUI::init() {
  CCLOG("CardCollectionUI::init()");
  //////////////////////////////
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }

  visibleSize = Director::getInstance()->getVisibleSize();
  origin = Director::getInstance()->getVisibleOrigin();
  cardCollection = EngineSingleton::GetInstance()->GetCardCollection();
  
  // Background image
  auto sprite = Sprite::create(kCardCollectionUIBackgroundImage);
  sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
  this->addChild(sprite);

  auto dashboardButtonLabel = cocos2d::Label::createWithTTF("Dashboard", kFontType, kFontSizeMedium);
  dashboardButtonLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  auto openDashboardButton = MenuItemLabel::create(dashboardButtonLabel, CC_CALLBACK_1(CardCollectionUI::OpenDashboard, this));
  openDashboardButton->setColor(cocos2d::Color3B::WHITE);
  Menu *menu = Menu::create(openDashboardButton, nullptr);
  menu->alignItemsVerticallyWithPadding(10);
  menu->setPosition(origin.x + visibleSize.width / 2, openDashboardButton->getContentSize().height);
  this->addChild(menu);

  // Scroll view
  Size scrollViewFrameSize = Size(visibleSize.width, visibleSize.height - (openDashboardButton->getContentSize().height * 2.0f));
  cardCollectionScrollView = ui::ScrollView::create();
  cardCollectionScrollView->setSwallowTouches(false);
  cardCollectionScrollView->setContentSize(scrollViewFrameSize);
  cardCollectionScrollView->setPosition(Vec2(origin.x, origin.y + (openDashboardButton->getContentSize().height * 2.0f)));
  cardCollectionScrollView->setDirection(ui::ScrollView::Direction::VERTICAL);
  scrollViewContainerSize = Size(scrollViewFrameSize.width, scrollViewFrameSize.height * 2);
  this->addChild(cardCollectionScrollView);
  
  // Register for touch events
  auto touchListener = EventListenerTouchOneByOne::create();
  touchListener->onTouchBegan = CC_CALLBACK_2(CardCollectionUI::onTouchBegan, this);
  touchListener->onTouchEnded = CC_CALLBACK_2(CardCollectionUI::onTouchEnded, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, cardCollectionScrollView);

  this->scheduleUpdate();

  DisplayCardCollection();
  return true;
}

void CardCollectionUI::update(float dt) {
  EngineSingleton::GetInstance()->Update(dt);
}

bool CardCollectionUI::onTouchBegan(Touch* touch, Event* event) {
  return true;
}

void CardCollectionUI::onTouchEnded(Touch* touch, Event* event) {
  
}

void CardCollectionUI::DisplayCardCollection() {
  auto sprite = cocos2d::Sprite::create(kCardNeutralFilename);
  Size cardSize = sprite->getContentSize();
  float cardHeight = cardSize.height;
  float dynamicHeightCalc = ((cardCollection->UnlockedCards()->size() / kMaxNumberOfCardsInCollectionRow) * cardHeight) + cardHeight;
  float scrollViewContainerHeight = (dynamicHeightCalc < visibleSize.height) ? visibleSize.height : dynamicHeightCalc;
  cardCollectionScrollView->setInnerContainerSize(cocos2d::Size(visibleSize.width, scrollViewContainerHeight));
  
  float padding = 20.0f;
  float cardRowWidth = (cardSize.width * kMaxNumberOfCardsInCollectionRow) + (padding * 3.0f);
  float widthDifference = scrollViewContainerSize.width - cardRowWidth;
  float initialCardPositionX = (widthDifference / 2.0f) + (cardSize.width / 2.0f);
  float cardSpacing = cardSize.width + padding;
  
  cocos2d::Vec2 cardPosition(initialCardPositionX, scrollViewContainerHeight - (cardSize.height / 2.0f + padding + (widthDifference / 2.0f)));
  
  for (int card = 0; card < cardCollection->UnlockedCards()->size(); card++) {
    if (card != 0 && card % kMaxNumberOfCardsInCollectionRow == 0) {
      cardPosition.y -= cardHeight + padding;
      cardPosition.x = initialCardPositionX;
    }
    
    cocos2d::Sprite *cardSprite = cardCollection->UnlockedCards()->at(card)->GetSprite();
    cardSprite->setParent(nullptr);
    cardSprite->setPosition(cardPosition);
    cardCollectionScrollView->addChild(cardSprite);
    cardPosition.x += cardSpacing;
  }
}

void CardCollectionUI::OpenDashboard(cocos2d::Ref *sender) {
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  auto dashboard = DashboardUI::createScene();
  cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.0f, dashboard, cocos2d::Color3B::BLACK));
}
