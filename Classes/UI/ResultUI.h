// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_RESULT_LAYER_H_
#define UI_RESULT_LAYER_H_

#include "cocos/cocos2d.h"

USING_NS_CC;

class Chest;

class ResultUI: public cocos2d::Layer {
public:
  static cocos2d::Scene* createScene();
  virtual bool init() override;
  virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*) override;
  virtual void onTouchEnded(cocos2d::Touch*, cocos2d::Event*) override;
  void CloseResult(cocos2d::Ref *sender);
  CREATE_FUNC(ResultUI);
  
private:
  cocos2d::Label *resultsLabel;
  cocos2d::MenuItemLabel *closeResultButton;
  cocos2d::MenuItemLabel *finishButton;
  std::shared_ptr<Chest> rewardChest;
};

#endif
