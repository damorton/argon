// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/ChestSlotUI.h"
#include "UI/ChestDetailsPopUpUI.h"
#include "Engine/Singleton/EngineSingleton.h"
#include "Game/Chest/ChestSlot.h"
#include "Engine/Utils/GameEvent.h"

ChestSlotUI::~ChestSlotUI(){
  chestSlots.clear();
}

Scene* ChestSlotUI::createScene() {
  // 'scene' is an autorelease object
  auto scene = Scene::create();

  // 'layer' is an autorelease object
  auto layer = ChestSlotUI::create();

  // add layer as a child to scene
  scene->addChild(layer);

  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool ChestSlotUI::init() {
  CCLOG("ChestSlotUI::init()");
  if (!Layer::init()) {
    return false;
  }
  
  visibleSize = Director::getInstance()->getVisibleSize();
  origin = Director::getInstance()->getVisibleOrigin();
  chestDetailsPopUpUI = nullptr;
  
  auto touchListener = EventListenerTouchOneByOne::create();
  touchListener->onTouchBegan = CC_CALLBACK_2(ChestSlotUI::onTouchBegan, this);
  touchListener->onTouchEnded = CC_CALLBACK_2(ChestSlotUI::onTouchEnded, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

  AddChestSlots();
  DisplayPlayerChestsInChestSlots();
  this->schedule(schedule_selector(ChestSlotUI::Update), 0.3f);
  return true;
}

void ChestSlotUI::AddChestSlots() {
  std::shared_ptr<ChestSlot> chestSlotTemp = ChestSlot::Create(kChestSlotFilename);
  Size cardSize = chestSlotTemp->Sprite()->getContentSize();
  
  float padding = 10.0f;
  float cardRowWidth = (cardSize.width * kMaxNumberOfChests) + (padding * (kMaxNumberOfChests - 1));
  float widthDifference = (visibleSize.width - cardRowWidth) / 2;
  float initialProductPositionX = (cardSize.width / 2.0f) + widthDifference;
  float productSpacing = cardSize.width + padding;
  
  cocos2d::Vec2 productPosition(initialProductPositionX, visibleSize.height * 0.15f);
  
  for(int iChestSlot = 0; iChestSlot < kMaxNumberOfChests; iChestSlot++) {
    std::shared_ptr<ChestSlot> chestSlot = ChestSlot::Create(kChestSlotFilename);
    chestSlots.push_back(chestSlot);
    cocos2d::Sprite *chestSlotSprite = chestSlot->Sprite();
    chestSlotSprite->setPosition(origin.x + productPosition.x, origin.y + productPosition.y);
    this->addChild(chestSlotSprite);
    productPosition.x += productSpacing;
  }
}

void ChestSlotUI::DisplayPlayerChestsInChestSlots() {
  unsigned long numberOfChestsSaved = EngineSingleton::GetInstance()->Chests()->size();
  for(int chestIndex = 0; chestIndex < numberOfChestsSaved; chestIndex++) {
    std::shared_ptr<Chest> currentChest = EngineSingleton::GetInstance()->Chests()->at(chestIndex);
    ChestSlot *currentChestSlot = chestSlots.at(chestIndex).get();
    cocos2d::Sprite *chestSprite = currentChest->Sprite();
    if(chestSprite){
      
      // Remove and re-add
      //currentChestSlot->Sprite()->removeChildByName(chestSprite->getName());
      chestSprite->setParent(nullptr);
      currentChestSlot->Sprite()->addChild(chestSprite);
      chestSprite->setPosition(currentChestSlot->Sprite()->getContentSize() / 2);
      chestSprite->setScale(0.3f);
      
      // Set chest for slot
      currentChestSlot->SetChest(currentChest);
      
      // Chest slot labels
      cocos2d::Label *slotLabel = currentChestSlot->SlotLabel();
      int posX = currentChestSlot->Sprite()->getPosition().x;
      int posY = currentChestSlot->Sprite()->getPosition().y + (currentChestSlot->Sprite()->getContentSize().height) / 2;
      slotLabel->setPosition(posX, posY);
      this->removeChild(slotLabel);
      this->addChild(slotLabel);
    }
  }
}

void ChestSlotUI::Update(float dt){
  for(auto chestSlot : chestSlots) {
    chestSlot->Update();
  }
  
  if(chestDetailsPopUpUI != nullptr) {
    if(!chestDetailsPopUpUI->isVisible()) {
      chestDetailsPopUpUI->removeFromParent();
      chestDetailsPopUpUI = nullptr;
    } else {
      chestDetailsPopUpUI->Update();
    }
  }
}

bool ChestSlotUI::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) {
  return true;
}

void ChestSlotUI::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event* event) {
  cocos2d::Vec2 touchCoordinates = touch->getLocation();
  for(auto chestSlot : chestSlots) {
    if(chestSlot->CurrentChest()){
      if(chestSlot->Sprite()->getBoundingBox().containsPoint(touchCoordinates)) {
        OpenChest(chestSlot.get());
        CCLOG("Chest slot selected");
      }
    }
  }
}

void ChestSlotUI::OpenChest(ChestSlot *chestSlot) {
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  if(chestSlot->CurrentChest()->GetState() == Chest::CHEST_READY_TO_OPEN){
    cocos2d::Scene *rewardUI = RewardUI::createScene();
    cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.0f, rewardUI, cocos2d::Color3B::BLACK));
    UILocator::GetRewardUI()->SetSelectedChest(chestSlot->CurrentChest());
    std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::EVENT_GAME_MODEL_UPDATED);
    EngineSingleton::GetInstance()->Notify(gameEvent.get());
  } else {
    CreateChestDetailsPopUpAndInit(chestSlot);
  }
}

void ChestSlotUI::CreateChestDetailsPopUpAndInit(ChestSlot *chestSlot) {
  chestDetailsPopUpUI = ChestDetailsPopUpUI::create();
  chestDetailsPopUpUI->SetSelectedChestSlot(chestSlot);
  chestDetailsPopUpUI->setVisible(true);
  chestDetailsPopUpUI->Update();
  this->addChild(chestDetailsPopUpUI, 0, "chestDetails");
}

void ChestSlotUI::OnNotify(GameEvent *event){
  if(event->Type() == GameEvent::EVENT_GAME_MODEL_UPDATED){
    DisplayPlayerChestsInChestSlots();
  }
}
