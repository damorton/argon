// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/ConfirmPurchaseUI.h"
#include "Engine/Service/Shop/GameStoreProduct.h"
#include "Engine/Locator/ServiceLocator.h"
#include "Engine/Locator/ControllerLocator.h"
#include "Engine/Locator/UILocator.h"

ConfirmPurchaseUI::~ConfirmPurchaseUI() {

}

void ConfirmPurchaseUI::PopUpInit(){
  auto backgroundSprite = Sprite::create(kGreyTransparentBackground);
  backgroundSprite->setPosition(screenCentre);
  this->addChild(backgroundSprite);
  
  productTitleLabel = cocos2d::Label::createWithTTF("Product Title", kFontType, kFontSizeMediumLarge);
  productTitleLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  productTitleLabel->setPosition(screenCentre.x, screenCentre.y + (innerPopupSprite->getContentSize().height * 0.30f));
  this->addChild(productTitleLabel);
  
  productDetailsLabel = cocos2d::Label::createWithTTF("Product details", kFontType, kFontSizeMedium);
  productDetailsLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  productDetailsLabel->setPosition(screenCentre);
  //this->addChild(productDetailsLabel);
  
  auto touchListener = EventListenerTouchOneByOne::create();
  touchListener->setSwallowTouches(true);
  touchListener->onTouchBegan = CC_CALLBACK_2(ConfirmPurchaseUI::onTouchBegan, this);
  touchListener->onTouchEnded = CC_CALLBACK_2(ConfirmPurchaseUI::onTouchEnded, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

  confirmButton = MenuItemImage::create(kConfirmButtonBackgroundImage, kConfirmButtonBackgroundImage, CC_CALLBACK_1(ConfirmPurchaseUI::Confirm, this));
  confirmButton->setPosition((innerPopupSprite->getContentSize().width / 2) - (confirmButton->getContentSize().width / 2) - kPaddingSmall,
                              -(innerPopupSprite->getContentSize().height / 2) + (confirmButton->getContentSize().height / 2) + kPaddingSmall);
  
  productCostLabel = cocos2d::Label::createWithTTF("0", kFontType, kFontSizeMedium);
  productCostLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  productCostLabel->setPosition(confirmButton->getContentSize().width * 0.50f, confirmButton->getContentSize().height * 0.50f);
  confirmButton->addChild(productCostLabel);
  
  cancelButton = MenuItemImage::create(kCancelButtonBackgroundImage, kCancelButtonBackgroundImage, CC_CALLBACK_1(ConfirmPurchaseUI::Cancel, this));
  cancelButton->setPosition(-(innerPopupSprite->getContentSize().width / 2) + (cancelButton->getContentSize().width / 2) + kPaddingSmall,
                            -(innerPopupSprite->getContentSize().height / 2) + (cancelButton->getContentSize().height / 2) + kPaddingSmall);
  
  cancelLabel = cocos2d::Label::createWithTTF("Cancel", kFontType, kFontSizeMedium);
  cancelLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  cancelLabel->setPosition(cancelButton->getContentSize().width * 0.50f, cancelButton->getContentSize().height * 0.50f);
  cancelButton->addChild(cancelLabel);
  
  auto menu = cocos2d::Menu::create(confirmButton, cancelButton, nullptr);
  this->addChild(menu);
}

bool ConfirmPurchaseUI::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) {
  return true;
}

void ConfirmPurchaseUI::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event* event) {
}

void ConfirmPurchaseUI::Confirm(cocos2d::Ref *sender){
  CCLOG("Purchase confirmed");
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  ServiceLocator::GameStore()->Purchase(activeProduct->Name());
  this->removeFromParent();
}

void ConfirmPurchaseUI::Cancel(cocos2d::Ref *sender){
  CCLOG("Purchase cancelled");
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  this->removeFromParent();
}

void ConfirmPurchaseUI::SetProduct(GameStoreProduct *product){
  activeProduct = product;
  DisplayProductInformation();
}

void ConfirmPurchaseUI::DisplayProductInformation(){
  productTitleLabel->setString(activeProduct->Title());
  productDetailsLabel->setString(activeProduct->Name());
  productCostLabel->setString(activeProduct->Price());
  cocos2d::Sprite *currencyImage = activeProduct->CurrencySprite();
  if(currencyImage){
    cocos2d::Sprite *currencyImageClone = cocos2d::Sprite::createWithTexture(currencyImage->getTexture());
    currencyImageClone->setPosition(productCostLabel->getPosition().x + (productCostLabel->getContentSize().width / 2) + (currencyImageClone->getContentSize().width / 2), productCostLabel->getPosition().y);
    confirmButton->addChild(currencyImageClone);
  }
  
  if(activeProduct->GoldCost() > 0){
    if(!ControllerLocator::GetConfigController()->CheckGoldAmount(activeProduct->GoldCost())){
      productCostLabel->setColor(cocos2d::Color3B::RED);
      confirmButton->setEnabled(false);
    }
  } else if(activeProduct->GemCost() > 0) {
    if(!ControllerLocator::GetConfigController()->CheckGemAmount(activeProduct->GemCost())){
      productCostLabel->setColor(cocos2d::Color3B::RED);
      confirmButton->setEnabled(false);
    }
  }
}
