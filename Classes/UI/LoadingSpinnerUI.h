// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_LOADING_SPINNER_UI_H_
#define UI_LOADING_SPINNER_UI_H_

#include "cocos2d.h"

class LoadingSpinnerUI: public cocos2d::Layer {
public:
  virtual bool init() override;
  virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*) override;

  CREATE_FUNC(LoadingSpinnerUI);
};

#endif
