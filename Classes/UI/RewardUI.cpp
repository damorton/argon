// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/RewardUI.h"
#include "Engine/Singleton/EngineSingleton.h"
#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#endif
#include "Game/Card.h"

RewardUI::~RewardUI() {
  rewardCards.clear();
  selectedChest.reset();
}

Scene* RewardUI::createScene() {
  // 'scene' is an autorelease object
  auto scene = Scene::create();

  // 'layer' is an autorelease object
  auto layer = RewardUI::create();

  // add layer as a child to scene
  scene->addChild(layer);

  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool RewardUI::init() {
  CCLOG("RewardUI::init()");
  if (!Layer::init()) {
    return false;
  }

  UILocator::Register(this);
  
  rewardState = INITIALISED;
  
  // Register for touch events
  auto touchListener = EventListenerTouchOneByOne::create();
  touchListener->onTouchBegan = CC_CALLBACK_2(RewardUI::onTouchBegan, this);
  touchListener->onTouchEnded = CC_CALLBACK_2(RewardUI::onTouchEnded, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

  visibleSize = Director::getInstance()->getVisibleSize();
  origin = Director::getInstance()->getVisibleOrigin();
  screenCentre = Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);
  std::string defaultFont(kFontType);

  // Background image
  auto sprite = Sprite::create(kRewardUIBackgroundImage);
  sprite->setPosition(screenCentre);
  this->addChild(sprite, 0);

  titleLabel = Label::createWithTTF("Title", kFontType, kFontSizeLarge);
  titleLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  titleLabel->setPosition(origin.x + visibleSize.width / 2, origin.y + visibleSize.height - 80);
  titleLabel->setString("Reward UI");
  //addChild(titleLabel);
  
  float goldCoinSpritePositionX = visibleSize.width * 0.3f;
  float goldCoinSpritePositionY = visibleSize.height * 0.60f;
  goldCoinSprite = cocos2d::Sprite::create(kGoldCoinFilename);
  goldCoinSprite->setScale(0.7f);
  goldCoinSprite->setPosition(cocos2d::Vec2(origin.x + goldCoinSpritePositionX, goldCoinSpritePositionY));
  goldCoinSprite->setVisible(false);
  this->addChild(goldCoinSprite);
  
  float gemSpritePositionX = visibleSize.width * 0.6f;
  float gemSpritePositionY = visibleSize.height * 0.60f;
  gemSprite = cocos2d::Sprite::create(kGemFilename);
  gemSprite->setScale(0.7f);
  gemSprite->setPosition(cocos2d::Vec2(origin.x + gemSpritePositionX, gemSpritePositionY));
  gemSprite->setVisible(false);
  this->addChild(gemSprite);
  
  this->scheduleUpdate();

  sdkbox::PluginGoogleAnalytics::logScreen("Reward UI");
  sdkbox::PluginGoogleAnalytics::logEvent("User", "Reward", ServiceLocator::GooglePlayService()->GooglePlayUsername(), 1);
  return true;
}

void RewardUI::SetSelectedChest(std::shared_ptr<Chest> chest) {
  selectedChest = chest;
  ApplyRewardsToPlayerData(selectedChest.get());
  RemoveChestFromEngine(selectedChest.get());
}

void RewardUI::onEnterTransitionDidFinish(){
  if(selectedChest){
    cocos2d::Sprite *chestSprite = selectedChest->Sprite();
    chestSprite->setScale(1.0f);
    chestSprite->setPosition(screenCentre.x, screenCentre.y * 0.75f);
    this->removeChildByName(chestSprite->getName());
    chestSprite->setParent(nullptr);
    this->addChild(selectedChest->Sprite());
  }
}

void RewardUI::update(float dt) {
  EngineSingleton::GetInstance()->Update(dt);
}

void RewardUI::Finish() {
  auto dashboard = DashboardUI::createScene();
  cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.0f, dashboard, cocos2d::Color3B::BLACK));
}

bool RewardUI::onTouchBegan(Touch* touch, Event* event) {
  return true;
}

void RewardUI::onTouchEnded(Touch* touch, Event* event) {
  if(rewardState == INITIALISED) {
    ControllerLocator::GetAudioController()->PlaySoundEffect(kOpenChestSoundEffect);
    cocos2d::ParticleSystemQuad *particleSystem = cocos2d::ParticleSystemQuad::create(kCardPlacedParicleEffect);
    particleSystem->setAutoRemoveOnFinish(true);
    particleSystem->setPosition(selectedChest->Sprite()->getPosition());
    this->addChild(particleSystem);

    UpdateResourceLabels(selectedChest.get());
    
    rewardState = REWARD_CLAIMED;
    
    std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::EVENT_SAVE_GAME_CONFIG);
    EngineSingleton::GetInstance()->Notify(gameEvent.get());
  } else if(rewardState == REWARD_CLAIMED) {
    ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
    Finish();
  }
}

void RewardUI::UpdateResourceLabels(Chest *rewardChest) {
  DisplayStars(rewardChest);
  DisplayCards(rewardChest);
  DisplayGold(rewardChest);
  DisplayGems(rewardChest);
}

void RewardUI::ApplyRewardsToPlayerData(Chest *rewardChest) {
  ControllerLocator::GetConfigController()->ApplyRewardsToPlayerData(rewardChest);
}

void RewardUI::RemoveChestFromEngine(Chest *chest) {
  EngineSingleton::GetInstance()->RemoveChest(chest);
}

void RewardUI::DisplayStars(Chest *rewardChest) {
  CCLOG("Player won %d stars", rewardChest->Stars());
  // TODO display stars on reward UI
}

void RewardUI::DisplayCards(Chest *rewardChest) {
  float cardSpacingX = visibleSize.width / (static_cast<float>(rewardChest->Cards()->size()) + 1.0f);
  float cardPositionX = origin.x + cardSpacingX;
  float cardPositionY = visibleSize.height * 0.75f;

  for(int cardIndex = 0; cardIndex < rewardChest->Cards()->size(); cardIndex++) {
    Card::CardData cardData = rewardChest->Cards()->at(cardIndex)->GetCardData();
    std::shared_ptr<Card> card = CardFactory::CreateCard(cardData);
    rewardCards.push_back(card);
    card->GetSprite()->setPosition(cocos2d::Vec2(cardPositionX, cardPositionY));
    this->addChild(card->GetSprite());
    cardPositionX += cardSpacingX;
  }
  sdkbox::PluginGoogleAnalytics::logEvent("Chest", "Opened", "Cards in chest", static_cast<int>(rewardChest->Cards()->size()));
}

void RewardUI::DisplayGold(Chest *rewardChest) {
  std::string goldAmount = Utils::to_str(rewardChest->Gold());
  cocos2d::Label *goldLabel = cocos2d::Label::createWithTTF(goldAmount, kFontType, kFontSizeMedium);
  goldLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  cocos2d::Vec2 goldLabelPosition = cocos2d::Vec2(goldCoinSprite->getPosition().x + (goldCoinSprite->getContentSize().width / 2) + (goldLabel->getContentSize().width / 2), goldCoinSprite->getPosition().y);
  goldLabel->setPosition(goldLabelPosition);
  this->addChild(goldLabel);
  goldCoinSprite->setVisible(true);
  sdkbox::PluginGoogleAnalytics::logEvent("Chest", "Opened", "Gold in chest", rewardChest->Gold());
}

void RewardUI::DisplayGems(Chest *rewardChest) {
  std::string gemAmount = Utils::to_str(rewardChest->Gems());
  cocos2d::Label *gemLabel = cocos2d::Label::createWithTTF(gemAmount, kFontType, kFontSizeMedium);
  gemLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  cocos2d::Vec2 gemPosition = cocos2d::Vec2(gemSprite->getPosition().x + (gemSprite->getContentSize().width / 2) + (gemLabel->getContentSize().width / 2), gemSprite->getPosition().y);
  gemLabel->setPosition(gemPosition);
  this->addChild(gemLabel);
  gemSprite->setVisible(true);
  sdkbox::PluginGoogleAnalytics::logEvent("Chest", "Opened", "Gems in chest", rewardChest->Gems());
}


