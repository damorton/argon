// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_RESOURCE_DISPLAY_UI_H_
#define UI_RESOURCE_DISPLAY_UI_H_

#include "cocos2d.h"
#include "UI/PopUpUI.h"
#include "Engine/Utils/Observer.h"

class ResourceDisplayUI : public cocos2d::Layer, public Observer {
public:
  virtual ~ResourceDisplayUI();
  virtual bool init() override;
  virtual void OnNotify(GameEvent *event) override;
  CREATE_FUNC(ResourceDisplayUI);
  
private:
  void UpdateResourceLabels();
  void OpenShop();
  void BuyGems(cocos2d::Ref *sender);
  void BuyGold(cocos2d::Ref *sender);
  
  cocos2d::Sprite *starSprite;
  cocos2d::Label *starLabel;
  
  cocos2d::Sprite *goldSprite;
  cocos2d::Label *goldLabel;
  
  cocos2d::Sprite *gemSprite;
  cocos2d::Label *gemLabel;
  
  cocos2d::Size visibleSize;
};

#endif
