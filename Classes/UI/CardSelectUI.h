// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_CARD_SELECT_UI_H_
#define UI_CARD_SELECT_UI_H_

#include "cocos/cocos2d.h"
#include "cocos/ui/UIScrollView.h"
#include "Engine/Utils/Observer.h"

class CardSelectUI: public cocos2d::Layer, public Observer {
public:
  static cocos2d::Scene* createScene();
  virtual bool init() override;
  virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*) override;
  virtual void onTouchEnded(cocos2d::Touch*, cocos2d::Event*) override;
  cocos2d::MenuItemLabel *GetReadyButton();
  void TimeoutClock(float dt);
  virtual void OnNotify(GameEvent *event) override;
  CREATE_FUNC(CardSelectUI);

private:
  void DisplayCardCollection();
  virtual void update(float dt) override;
  void StartGame(cocos2d::Ref *sender);
  void DeselectAllCards();

  cocos2d::MenuItemLabel *readyButton;
  cocos2d::Label *timeoutClockLabel;
  cocos2d::ui::ScrollView *cardSelectionScrollView;
  int timoutClockValue;
};

#endif
