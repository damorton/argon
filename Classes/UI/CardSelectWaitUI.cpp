// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/CardSelectWaitUI.H"
#include "Engine/Singleton/EngineSingleton.h"

Scene* CardSelectWaitUI::createScene() {
  // 'scene' is an autorelease object
  auto scene = Scene::create();

  // 'layer' is an autorelease object
  auto layer = CardSelectWaitUI::create();

  // add layer as a child to scene
  scene->addChild(layer);

  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool CardSelectWaitUI::init() {
  CCLOG("CardSelectWaitUI::init()");
  //////////////////////////////
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }

  auto visibleSize = Director::getInstance()->getVisibleSize();
  Vec2 origin = Director::getInstance()->getVisibleOrigin();
  Size size = Director::getInstance()->getWinSize();
  Vec2 screenCenter(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);

  // Background image
  auto sprite = Sprite::create(kGreyTransparentBackground);
  sprite->setPosition(screenCenter);
  this->addChild(sprite, 0);
  
  // Register for touch events
  auto touchListener = EventListenerTouchOneByOne::create();
  touchListener->onTouchBegan = CC_CALLBACK_2(CardSelectWaitUI::onTouchBegan, this);
  touchListener->setSwallowTouches(true); // Do not propogate touch event to lower layers
  _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

  // Labels
  auto loadingMessage = Label::createWithTTF("Waiting for opponents...", kFontType, kFontSizeMedium);
  loadingMessage->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  loadingMessage->setPosition(screenCenter);
  addChild(loadingMessage);

  this->scheduleUpdate();
  return true;
}

bool CardSelectWaitUI::onTouchBegan(cocos2d::Touch*, cocos2d::Event*) {
  return true;
}

