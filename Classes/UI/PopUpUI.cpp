// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/PopUpUI.h"
#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#endif

PopUpUI::~PopUpUI() {
  
}

// on "init" you need to initialize your instance
bool PopUpUI::init() {
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }

  visibleSize = Director::getInstance()->getVisibleSize();
  origin = Director::getInstance()->getVisibleOrigin();
  screenCentre = Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);

  innerPopupSprite = Sprite::create(kWidgetContentBackgroundImage);
  innerPopupSprite->setPosition(screenCentre);
  this->addChild(innerPopupSprite);
  
  PopUpInit();
  CCLOG("WidgetUI initialised");
  return true;
}
