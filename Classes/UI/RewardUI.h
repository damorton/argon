// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_REWARD_UI_H_
#define UI_REWARD_UI_H_

#include "cocos/cocos2d.h"
#include "Game/Chest/Chest.h"

USING_NS_CC;

class Card;

class RewardUI: public cocos2d::Layer {
public:
  virtual ~RewardUI();
  static cocos2d::Scene* createScene();
  virtual bool init() override;
  virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*) override;
  virtual void onTouchEnded(cocos2d::Touch*, cocos2d::Event*) override;
  virtual void onEnterTransitionDidFinish() override;
  void SetSelectedChest(std::shared_ptr<Chest> chest);
  CREATE_FUNC(RewardUI);

private:
  enum RewardState {
    INITIALISED,
    REWARD_CLAIMED
  };
  void UpdateResourceLabels(Chest *rewardChest);
  void update(float dt) override;
  void OpenChest();
  void Finish();
  void DisplayStars(Chest *rewardChest);
  void DisplayCards(Chest *rewardChest);
  void DisplayGold(Chest *rewardChest);
  void DisplayGems(Chest *rewardChest);
  void ApplyRewardsToPlayerData(Chest *rewardChest);
  void RemoveChestFromEngine(Chest *chest);
  
  RewardState rewardState;
  cocos2d::Label *titleLabel;
  cocos2d::Size visibleSize;
  cocos2d::Vec2 origin;
  cocos2d::Vec2 screenCentre;
  cocos2d::Sprite *goldCoinSprite;
  cocos2d::Sprite *gemSprite;
  std::shared_ptr<Chest> selectedChest;
  std::vector<std::shared_ptr<Card>> rewardCards;
};

#endif
