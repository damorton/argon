// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_CARD_SELECT_WAIT_UI_H_
#define UI_CARD_SELECT_WAIT_UI_H_

#include "cocos2d.h"

USING_NS_CC;

class CardSelectWaitUI: public cocos2d::Layer {
public:
  static cocos2d::Scene* createScene();

  virtual bool init() override;
  virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*) override;

  CREATE_FUNC(CardSelectWaitUI);
};

#endif
