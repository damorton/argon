// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_CARD_COLLECTION_UI_H_
#define UI_CARD_COLLECTION_UI_H_

#include "cocos/cocos2d.h"
#include "cocos/ui/UIScrollView.h"
#include "Game/CardCollection.h"

USING_NS_CC;

class CardCollectionUI: public cocos2d::Layer {
public:
  static cocos2d::Scene* createScene();
  virtual bool init() override;
  virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*) override;
  virtual void onTouchEnded(cocos2d::Touch*, cocos2d::Event*) override;
  CREATE_FUNC(CardCollectionUI);
private:
  virtual void update(float dt) override;
  void DisplayCardCollection();
  void OpenDashboard(cocos2d::Ref *sender);

  cocos2d::Size scrollViewContainerSize;
  ui::ScrollView *cardCollectionScrollView;
  CardCollectionInterface *cardCollection;
  cocos2d::Size visibleSize;
  cocos2d::Vec2 origin;
};

#endif
