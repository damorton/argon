// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/LoadingSpinnerUI.H"
#include "Engine/Singleton/EngineSingleton.h"

// on "init" you need to initialize your instance
bool LoadingSpinnerUI::init() {
  CCLOG("LoadingSpinnerUI::init()");
  //////////////////////////////
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }

  auto visibleSize = Director::getInstance()->getVisibleSize();
  Vec2 origin = Director::getInstance()->getVisibleOrigin();
  Size size = Director::getInstance()->getWinSize();
  Vec2 screenCentre(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);

  // Background image
  auto sprite = Sprite::create(kGreyTransparentBackground);
  sprite->setPosition(screenCentre);
  this->addChild(sprite, 0);
  
  // Register for touch events
  auto touchListener = EventListenerTouchOneByOne::create();
  touchListener->onTouchBegan = CC_CALLBACK_2(LoadingSpinnerUI::onTouchBegan, this);
  touchListener->setSwallowTouches(true); // Do not propogate touch event to lower layers
  _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

  // Add spinner
  cocos2d::ParticleSystemQuad *particleSystem = cocos2d::ParticleSystemQuad::create(kLoadingSpinnerParicleEffect);
  particleSystem->setPosition(screenCentre);
  this->addChild(particleSystem);

  this->scheduleUpdate();
  return true;
}

bool LoadingSpinnerUI::onTouchBegan(cocos2d::Touch*, cocos2d::Event*) {
  return true;
}

