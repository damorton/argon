// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_POPUP_UI_H_
#define UI_POPUP_UI_H_

#include "cocos/cocos2d.h"
#include "GameDefines.h"

USING_NS_CC;

class PopUpUI : public cocos2d::Layer {
public:
  virtual ~PopUpUI();
  virtual bool init() override;
  
protected:
  virtual void PopUpInit() = 0;
  
  cocos2d::Sprite *innerPopupSprite;
  cocos2d::Size visibleSize;
  cocos2d::Vec2 origin;
  cocos2d::Vec2 screenCentre;
};

#endif
