// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_HUD_UI_H_
#define UI_HUD_UI_H_

#include "cocos/cocos2d.h"

USING_NS_CC;

class HudUI: public cocos2d::Layer {
public:
  static cocos2d::Scene* createScene();

  virtual bool init() override;
  cocos2d::Label *GetTimeLabel();
  void update(float dt) override;
  CREATE_FUNC(HudUI);

private:
  cocos2d::Label *timeLabel;
};

#endif
