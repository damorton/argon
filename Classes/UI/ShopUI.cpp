// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/ShopUI.h"
#include "Engine/Singleton/EngineSingleton.h"
#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#endif
#include "UI/ConfirmPurchaseUI.h"
#include "Game/Factory/CardFactory.h"
#include "UI/ResourceDisplayUI.h"

ShopUI::~ShopUI(){
}

Scene* ShopUI::createScene() {
  // 'scene' is an autorelease object
  auto scene = Scene::create();
  scene->setName(kShopUIName);
  
  // 'layer' is an autorelease object
  auto layer = ShopUI::create();

  // add layer as a child to scene
  scene->addChild(layer);

  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool ShopUI::init() {
  CCLOG("ShopUI::init()");
  //////////////////////////////
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }
  
  visibleSize = Director::getInstance()->getVisibleSize();
  origin = Director::getInstance()->getVisibleOrigin();
  screenCentre = Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);

  // Background image
  auto sprite = Sprite::create(kShopUIBackgroundImage);
  sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
  this->addChild(sprite);
  
  resourceLabelContainer = ResourceDisplayUI::create();
  resourceLabelContainer->setPosition(screenCentre.x, visibleSize.height - kPaddingLarge);
  this->addChild(resourceLabelContainer, 1);
  
  auto toDashboardLabel = cocos2d::Label::createWithTTF("Dashboard", kFontType, kFontSizeMedium);
  toDashboardLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  openDashboardButton = MenuItemLabel::create(toDashboardLabel, CC_CALLBACK_1(ShopUI::OpenDashboard, this));
  openDashboardButton->setColor(cocos2d::Color3B::WHITE);

  Menu *menu = Menu::create(openDashboardButton, nullptr);
  menu->alignItemsVerticallyWithPadding(10);
  menu->setPosition(origin.x + visibleSize.width / 2, openDashboardButton->getContentSize().height);
  this->addChild(menu);

  // Scroll view
  Size scollFrameSize = Size(visibleSize.width, visibleSize.height - (openDashboardButton->getContentSize().height * 4.5f));
  selectionScrollView = ui::ScrollView::create();
  selectionScrollView->setSwallowTouches(false);
  selectionScrollView->setContentSize(scollFrameSize);
  selectionScrollView->setPosition(Vec2(origin.x, origin.y + (openDashboardButton->getContentSize().height * 2.0f)));
  selectionScrollView->setDirection(ui::ScrollView::Direction::VERTICAL);
  scrollViewContainerSize = Size(scollFrameSize.width, scollFrameSize.height * 2);
  this->addChild(selectionScrollView);

  auto touchListener = EventListenerTouchOneByOne::create();
  touchListener->onTouchBegan = CC_CALLBACK_2(ShopUI::onTouchBegan, this);
  touchListener->onTouchEnded = CC_CALLBACK_2(ShopUI::onTouchEnded, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, selectionScrollView);
  this->scheduleUpdate();
  PopulateShop();
  sdkbox::PluginGoogleAnalytics::logScreen("Game Store UI");
  return true;
}

void ShopUI::PopulateShop() {
  storeProducts = ServiceLocator::GameStore()->Products();
  DisplayStoreProducts(selectionScrollView, storeProducts);
}

void ShopUI::DisplayStoreProducts(ui::ScrollView *selectionScrollView, std::vector<std::shared_ptr<GameStoreProduct>> *products) {
  auto productBackgroundSprite = cocos2d::Sprite::create(kProductBackgroundFilename);
  Size productSize = productBackgroundSprite->getContentSize();
  float productHeight = productSize.height;
  
  // Calculate height based on number of cards in the collection
  float dynamicHeightCalc = ((products->size() / kMaxNumberOfProductsInCollectionRow) * productHeight) + (productHeight / 2);
  
  // Set the container height to either the visible height or the dynamic height, whatever is larger
  float scrollViewContainerHeight = (dynamicHeightCalc < visibleSize.height) ? visibleSize.height : dynamicHeightCalc;
  selectionScrollView->setInnerContainerSize(cocos2d::Size(visibleSize.width, scrollViewContainerHeight));
  
  cocos2d::Size scrollViewContainerSize = selectionScrollView->getContentSize();
  
  float padding = 10.0f;
  float productRowWidth = (productSize.width * kMaxNumberOfProductsInCollectionRow) + (padding * 3.0f);
  float widthDifference = scrollViewContainerSize.width - productRowWidth;
  float initialProductPositionX = (widthDifference / 2.0f) + (productSize.width / 2.0f);
  float productSpacing = productSize.width + padding;
  
  cocos2d::Vec2 productPosition(initialProductPositionX, scrollViewContainerHeight - (productSize.height / 2.0f + padding + (widthDifference / 2.0f)));
  
  for (int product = 0; product < products->size(); product++) {
    if (product != 0 && product % kMaxNumberOfProductsInCollectionRow == 0) {
      productPosition.y -= productHeight + padding;
      productPosition.x = initialProductPositionX;
    }
    
    GameStoreProduct *storeProduct = products->at(product).get();
    cocos2d::Sprite *productBackgroundSprite = storeProduct->Sprite();
    productBackgroundSprite->setParent(nullptr);
    selectionScrollView->addChild(productBackgroundSprite);
    productBackgroundSprite->setPosition(productPosition);
    
    if(storeProduct->GetId() == "card") {
      DisplayCard(storeProduct);
    }else if(storeProduct->GetId() == "chest") {
      DisplayChest(storeProduct);
    } else if(storeProduct->GetId() == "gold") {
      DisplayGold(storeProduct);
    } else {
      DisplayGems(storeProduct);
    }
    
    // Product name
    auto productLabel = cocos2d::Label::createWithTTF(Utils::to_str(storeProduct->Title()), kFontType, kFontSizeSmall);
    productLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
    productLabel->setPosition(productBackgroundSprite->getContentSize().width * 0.50f, productBackgroundSprite->getContentSize().height * 0.90f);
    productBackgroundSprite->addChild(productLabel);
    
    productPosition.x += productSpacing;
  }
}

void ShopUI::DisplayCard(GameStoreProduct *product){
  auto productBackgroundSprite = product->Sprite();
  auto productCost = cocos2d::Label::createWithTTF(Utils::to_str(product->GoldCost()), kFontType, kFontSizeMedium);
  productCost->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  productCost->setPosition(productBackgroundSprite->getContentSize().width * 0.50f, productBackgroundSprite->getContentSize().height * 0.10f);
  productBackgroundSprite->addChild(productCost);
  
  auto goldImage = cocos2d::Sprite::create(kGoldShopImageFilename);
  goldImage->setPosition(productCost->getPosition().x + (productCost->getContentSize().width / 2) + (goldImage->getContentSize().width / 2), productCost->getPosition().y);
  productBackgroundSprite->addChild(goldImage);
  product->SetCurrencySprite(goldImage);
  
  auto cardSprite = product->StoredCard()->get()->GetSprite();
  cardSprite->setParent(nullptr);
  cardSprite->setPosition(productBackgroundSprite->getContentSize() / 2);
  productBackgroundSprite->addChild(cardSprite);
}

void ShopUI::DisplayChest(GameStoreProduct *product) {
  auto productSprite = product->Sprite();
  auto productCost = cocos2d::Label::createWithTTF(Utils::to_str(product->GemCost()), kFontType, kFontSizeMedium);
  productCost->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  productCost->setPosition(productSprite->getContentSize().width * 0.50f, productSprite->getContentSize().height * 0.75f);
  productSprite->addChild(productCost);
  auto gemImage = cocos2d::Sprite::create(kGemShopImageFilename);
  gemImage->setPosition(productCost->getPosition().x + (productCost->getContentSize().width / 2) + (gemImage->getContentSize().width / 2), productCost->getPosition().y);
  productSprite->addChild(gemImage);
  product->SetCurrencySprite(gemImage);
}

void ShopUI::DisplayGold(GameStoreProduct *product) {
  auto productSprite = product->Sprite();
  auto productCost = cocos2d::Label::createWithTTF(Utils::to_str(product->GemCost()), kFontType, kFontSizeMedium);
  productCost->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  productCost->setPosition(productSprite->getContentSize().width * 0.50f, productSprite->getContentSize().height * 0.75f);
  productSprite->addChild(productCost);
  auto gemImage = cocos2d::Sprite::create(kGemShopImageFilename);
  gemImage->setPosition(productCost->getPosition().x + (productCost->getContentSize().width / 2) + (gemImage->getContentSize().width / 2), productCost->getPosition().y);
  productSprite->addChild(gemImage);
  auto goldAmount = cocos2d::Label::createWithTTF(Utils::to_str(product->Value()), kFontType, kFontSizeMedium);
  goldAmount->setPosition(productSprite->getContentSize().width * 0.50f, productSprite->getContentSize().height * 0.55f);
  productSprite->addChild(goldAmount);
  product->SetCurrencySprite(gemImage);
}

void ShopUI::DisplayGems(GameStoreProduct *product) {
  auto productSprite = product->Sprite();
  auto productPrice = cocos2d::Label::createWithTTF(Utils::to_str(product->Price()), kFontType, kFontSizeMedium);
  productPrice->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  productPrice->setPosition(productSprite->getContentSize().width * 0.50f, productSprite->getContentSize().height * 0.75f);
  productSprite->addChild(productPrice);
  auto gemAmount = cocos2d::Label::createWithTTF(Utils::to_str(product->Value()), kFontType, kFontSizeMedium);
  gemAmount->setPosition(productSprite->getContentSize().width * 0.50f, productSprite->getContentSize().height * 0.55f);
  productSprite->addChild(gemAmount);
}

void ShopUI::update(float dt) {
  EngineSingleton::GetInstance()->Update(dt);
}

void ShopUI::OpenDashboard(cocos2d::Ref *sender) {
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  auto dashboard = DashboardUI::createScene();
  cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.0f, dashboard, cocos2d::Color3B::BLACK));
}

bool ShopUI::onTouchBegan(Touch* touch, Event* event) {
  touchPoint = touch->getLocation();
  return true;
}

void ShopUI::onTouchEnded(Touch* touch, Event* event) {
  cocos2d::Vec2 touchCoordinates = touch->getLocation();
  
  // When the touch event ends check to see if it is within a small square around the initial
  // touch position. If it is then the player means to select and item in the shop. If the touch ended
  // event is not close to the initial touch event the player is scrolling the shop and did not
  // intend to select an item.
  cocos2d::Rect touchArea(touchCoordinates.x - 20, touchCoordinates.y - 20, 40, 40);
  if(touchArea.containsPoint(touchPoint)){
    if(selectionScrollView->getBoundingBox().containsPoint(touchPoint)){
      cocos2d::Point touchLocation = selectionScrollView->getInnerContainer()->convertToNodeSpace(touchCoordinates);
      for(auto product : *storeProducts) {
        if(product->Sprite()->getBoundingBox().containsPoint(touchLocation)){
          ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
          auto confirmationPopUp = ConfirmPurchaseUI::create();
          confirmationPopUp->SetProduct(product.get());
          this->addChild(confirmationPopUp);
        }
      }
    }
  }
}
