// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/HudUI.h"
#include "Engine/Locator/ControllerLocator.h"
#include "Engine/Utils/Utils.h"
#include "GameDefines.h"

Scene* HudUI::createScene() {
  // 'scene' is an autorelease object
  auto scene = Scene::create();

  // 'layer' is an autorelease object
  auto layer = HudUI::create();

  // add layer as a child to scene
  scene->addChild(layer);

  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool HudUI::init() {
  CCLOG("HudUI::init()");
  //////////////////////////////
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }
  
  auto visibleSize = Director::getInstance()->getVisibleSize();
  Vec2 origin = Director::getInstance()->getVisibleOrigin();
  cocos2d::Vec2 screenCentre(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);

  int timeInSeconds = kGameSessionTimeSeconds;
  timeLabel = Label::createWithTTF(Utils::to_str(timeInSeconds), kFontType, kFontSizeLarge);
  timeLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  timeLabel->setPosition(origin.x + visibleSize.width / 2, origin.y + visibleSize.height - (timeLabel->getContentSize().height / 2));
  this->addChild(timeLabel);

  this->scheduleUpdate();

  return true;
}

void HudUI::update(float dt){
  if(ControllerLocator::GetGameController()){
    if(!ControllerLocator::GetGameController()->IsGameTimerStopped()) {
      timeLabel->setString(Utils::to_str(ControllerLocator::GetGameController()->GetGameTimeSeconds()));
    }
  }
}

cocos2d::Label *HudUI::GetTimeLabel() {
  return timeLabel;
}

