// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_GAME_BOARD_UI_H_
#define UI_GAME_BOARD_UI_H_

#include "cocos/cocos2d.h"

class GameBoardUI: public cocos2d::Layer {
public:
  static cocos2d::Scene* createScene();

  virtual bool init() override;
  cocos2d::MenuItemLabel *GetQuitButton() {
    return quitButtonLabel;
  }
  
  virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) override;
  virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event) override;
  virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event) override;
  CREATE_FUNC(GameBoardUI);

private:
  void QuitGame(cocos2d::Ref *sender);
  virtual void update(float dt) override;
  void LeaveGame(cocos2d::Ref *sender);
  void ClockTimer(float dt);

  cocos2d::MenuItemLabel *quitButtonLabel;
  
  cocos2d::Label *localPlayerUsernameLabel;
  cocos2d::Label *opponentPlayerUsernameLabel;
};

#endif
