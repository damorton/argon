// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_LOADING_UI_H_
#define UI_LOADING_UI_H_

#include "cocos2d.h"
#include "Engine/Utils/Observer.h"

USING_NS_CC;

class LoadingUI: public cocos2d::Layer, public Observer {
public:
  static cocos2d::Scene* createScene();
  virtual bool init() override;
  virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*) override;
  virtual void OnNotify(GameEvent *event) override;
  CREATE_FUNC(LoadingUI);

private:
  void update(float dt) override;
  
  cocos2d::Label *loadingMessage;
};

#endif
