// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_SHOP_UI_H_
#define UI_SHOP_UI_H_

#include "cocos/cocos2d.h"
#include "cocos/ui/UIScrollView.h"

USING_NS_CC;

class GameStoreProduct;
class ResourceDisplayUI;

class ShopUI: public cocos2d::Layer {
public:
  virtual ~ShopUI();
  static cocos2d::Scene* createScene();
  virtual bool init() override;
  virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*) override;
  virtual void onTouchEnded(cocos2d::Touch*, cocos2d::Event*) override;
  CREATE_FUNC(ShopUI);

private:
  virtual void update(float dt) override;
  void OpenDashboard(cocos2d::Ref *sender);
  void PopulateShop();
  void DisplayStoreProducts(ui::ScrollView *selectionScrollView, std::vector<std::shared_ptr<GameStoreProduct>> *products);
  void DisplayCard(GameStoreProduct *product);
  void DisplayChest(GameStoreProduct *product);
  void DisplayGold(GameStoreProduct *product);
  void DisplayGems(GameStoreProduct *product);
  
  cocos2d::MenuItemLabel *openDashboardButton;
  cocos2d::Label *openDashboardButtonLabel;
  cocos2d::Size scrollViewContainerSize;
  ui::ScrollView *selectionScrollView;
  cocos2d::Size visibleSize;
  cocos2d::Vec2 origin;
  cocos2d::Vec2 screenCentre;
  ResourceDisplayUI *resourceLabelContainer;
  std::vector<std::shared_ptr<GameStoreProduct>> *storeProducts;
  cocos2d::Point touchPoint;
};

#endif
