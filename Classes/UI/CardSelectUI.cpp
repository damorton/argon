// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/CardSelectUI.h"
#include "Engine/Singleton/EngineSingleton.h"
#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#endif

Scene* CardSelectUI::createScene() {
  // 'scene' is an autorelease object
  auto scene = Scene::create();

  // 'layer' is an autorelease object
  auto layer = CardSelectUI::create();

  // add layer as a child to scene
  scene->addChild(layer);

  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool CardSelectUI::init() {
  CCLOG("CardSelectUI::init()");
  //////////////////////////////
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }

  Size visibleSize = Director::getInstance()->getVisibleSize();
  Vec2 origin = Director::getInstance()->getVisibleOrigin();

  // Background image
  auto sprite = Sprite::create(kCardCollectionUIBackgroundImage);
  sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
  this->addChild(sprite);

  auto readyLabel = cocos2d::Label::createWithTTF("Start Game", kFontType, kFontSizeLarge);
  readyLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  readyButton = MenuItemLabel::create(readyLabel, CC_CALLBACK_1(CardSelectUI::StartGame, this));
  readyButton->setColor(cocos2d::Color3B::WHITE);
  readyButton->setEnabled(false);
  Menu *menu = Menu::create(readyButton, nullptr);
  menu->alignItemsVerticallyWithPadding(10);
  menu->setPosition(origin.x + visibleSize.width / 2, readyButton->getContentSize().height);
  this->addChild(menu);

  // Scroll view
  Size scrollFrameSize = Size(visibleSize.width, visibleSize.height - (readyButton->getContentSize().height * 2.0f));
  cardSelectionScrollView = ui::ScrollView::create();
  cardSelectionScrollView->setSwallowTouches(false);
  cardSelectionScrollView->setContentSize(scrollFrameSize);
  cardSelectionScrollView->setPosition(Vec2(origin.x, origin.y + (readyButton->getContentSize().height * 2.0f)));
  cardSelectionScrollView->setDirection(ui::ScrollView::Direction::VERTICAL);
  this->addChild(cardSelectionScrollView);

  // Register for touch events
  auto touchListener = EventListenerTouchOneByOne::create();
  touchListener->onTouchBegan = CC_CALLBACK_2(CardSelectUI::onTouchBegan, this);
  touchListener->onTouchEnded = CC_CALLBACK_2(CardSelectUI::onTouchEnded, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, cardSelectionScrollView);

  timoutClockValue = kCardSelectTimeout;
  timeoutClockLabel = cocos2d::Label::createWithTTF(Utils::to_str(timoutClockValue), kFontType, kFontSizeLarge);
  timeoutClockLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  timeoutClockLabel->setPosition(origin.x + visibleSize.width / 2, readyButton->getPosition().y + readyButton->getContentSize().height + timeoutClockLabel->getContentSize().height);
  timeoutClockLabel->setColor(cocos2d::Color3B::WHITE);
  this->addChild(timeoutClockLabel);
  
  this->scheduleUpdate();
  this->schedule(schedule_selector(CardSelectUI::TimeoutClock), 1.0f);
  
  DisplayCardCollection();
  sdkbox::PluginGoogleAnalytics::logScreen("Card Select UI");
  return true;
}

void CardSelectUI::TimeoutClock(float dt) {
  timoutClockValue--;
  timeoutClockLabel->setString(Utils::to_str(timoutClockValue));
  if(timoutClockValue <= 0) {
    EngineSingleton::GetInstance()->EndGameSession();
  }
}

void CardSelectUI::update(float dt) {
  EngineSingleton::GetInstance()->Update(dt);
}

cocos2d::MenuItemLabel *CardSelectUI::GetReadyButton() {
  return readyButton;
}

void CardSelectUI::StartGame(cocos2d::Ref *sender) {
  DeselectAllCards();
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  auto waitingScreen = CardSelectWaitUI::createScene();
  this->addChild(waitingScreen);
  std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::MENU_BUTTON_START_GAME);
  ControllerLocator::GetInputController()->ProcessInputEvent(gameEvent.get());
}

void CardSelectUI::DeselectAllCards(){
  for (auto card : *EngineSingleton::GetInstance()->GetCardCollection()->UnlockedCards()) {
    card->SetHighlighted(false);
  }
}

bool CardSelectUI::onTouchBegan(Touch* touch, Event* event) {
  return true;
}

void CardSelectUI::onTouchEnded(Touch* touch, Event* event) {
  cocos2d::Vec2 touchCoordinates = touch->getLocation();
  cocos2d::Point touchLocation = cardSelectionScrollView->getInnerContainer()->convertToNodeSpace(touchCoordinates);
  std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::TOUCH_INPUT_CARD_SELECT, touchLocation.x, touchLocation.y);
  ControllerLocator::GetInputController()->ProcessInputEvent(gameEvent.get());
}

void CardSelectUI::DisplayCardCollection() {
  CardCollectionInterface *cardCollection = EngineSingleton::GetInstance()->GetCardCollection();
  
  cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
  cocos2d::Vec2 origin = cardSelectionScrollView->getPosition();
  
  auto sprite = cocos2d::Sprite::create(kCardNeutralFilename);
  Size cardSize = sprite->getContentSize();
  float cardHeight = cardSize.height;
  
  // Calculate height based on number of cards in the collection
  float dynamicHeightCalc = ((cardCollection->UnlockedCards()->size() / kMaxNumberOfCardsInCollectionRow) * cardHeight) + cardHeight;
  
  // Set the container height to either the visible height or the dynamic height, whatever is larger
  float scrollViewContainerHeight = (dynamicHeightCalc < visibleSize.height) ? visibleSize.height : dynamicHeightCalc;
  
  cardSelectionScrollView->setInnerContainerSize(cocos2d::Size(visibleSize.width, scrollViewContainerHeight));
  cocos2d::Size scrollViewContainerSize = cardSelectionScrollView->getContentSize();
  
  float padding = 20.0f;
  float cardRowWidth = (cardSize.width * kMaxNumberOfCardsInCollectionRow) + (padding * 3.0f);
  float widthDifference = scrollViewContainerSize.width - cardRowWidth;
  float initialCardPositionX = (widthDifference / 2.0f) + (cardSize.width / 2.0f);
  float cardSpacing = cardSize.width + padding;
  
  cocos2d::Vec2 cardPosition(initialCardPositionX, scrollViewContainerHeight - (cardSize.height / 2.0f + padding + (widthDifference / 2.0f)));
  
  for (int card = 0; card < cardCollection->UnlockedCards()->size(); card++) {
    if (card != 0 && card % kMaxNumberOfCardsInCollectionRow == 0) {
      cardPosition.y -= cardHeight + padding;
      cardPosition.x = initialCardPositionX;
    }
    
    cocos2d::Sprite *cardSprite = cardCollection->UnlockedCards()->at(card)->GetSprite();
    cardSprite->setParent(nullptr);
    cardSelectionScrollView->addChild(cardSprite);
    cardCollection->UnlockedCards()->at(card)->GetSprite()->setPosition(cardPosition);
    cardCollection->UnlockedCards()->at(card)->SetHighlighted(false);
    cardPosition.x += cardSpacing;
  }
}

void CardSelectUI::OnNotify(GameEvent *event){
  switch (event->Type()) {
    case GameEvent::CARD_SELECTION_DECK_NOT_FULL:
      readyButton->setEnabled(true);
      break;
    case GameEvent::CARD_SELECTION_DECK_FULL:
      readyButton->setEnabled(false);
      break;
    default:
      break;
  }
  
}
