// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_CHEST_SLOT_UI_H_
#define UI_CHEST_SLOT_UI_H_

#include "cocos2d.h"
#include "Engine/Utils/Observer.h"

class Chest;
class ChestSlot;
class ChestDetailsPopUpUI;

class ChestSlotUI: public cocos2d::Layer, public Observer {
public:
  virtual ~ChestSlotUI();
  static cocos2d::Scene* createScene();
  virtual bool init() override;
  virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) override;
  virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event) override;
  void Update(float dt);
  virtual void OnNotify(GameEvent *event) override;
  CREATE_FUNC(ChestSlotUI);
  
private:
  void DisplayPlayerChestsInChestSlots();
  void AddChestSlots();
  void OpenChest(ChestSlot *chestSlot);
  void CreateChestDetailsPopUpAndInit(ChestSlot *chest);
  
  float chestSlotSpacing;
  std::vector<std::shared_ptr<ChestSlot>> chestSlots;
  cocos2d::Size visibleSize;
  cocos2d::Vec2 origin;
  ChestDetailsPopUpUI *chestDetailsPopUpUI;
};

#endif
