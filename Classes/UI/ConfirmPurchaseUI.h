// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_CONFIRM_PURCHASE_UI_H_
#define UI_CONFIRM_PURCHASE_UI_H_

#include "UI/PopUpUI.h"

class GameStoreProduct;

class ConfirmPurchaseUI : public PopUpUI {
public:
  virtual ~ConfirmPurchaseUI();
  virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) override;
  virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event) override;
  void SetProduct(GameStoreProduct *product);
  CREATE_FUNC(ConfirmPurchaseUI);
  
private:
  virtual void PopUpInit() override;
  void Confirm(cocos2d::Ref *sender);
  void Cancel(cocos2d::Ref *sender);
  void DisplayProductInformation();
  
  cocos2d::MenuItemImage *confirmButton;
  cocos2d::MenuItemImage *cancelButton;
  cocos2d::Label *cancelLabel;
  cocos2d::Label *productTitleLabel;
  cocos2d::Label *productDetailsLabel;
  cocos2d::Label *productCostLabel;
  GameStoreProduct *activeProduct;
};

#endif
