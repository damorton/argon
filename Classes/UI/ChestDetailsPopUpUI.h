// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef UI_CHEST_DETAILS_POP_UP_H_
#define UI_CHEST_DETAILS_POP_UP_H_

#include "UI/PopUpUI.h"

USING_NS_CC;

class Chest;
class ChestSlot;

class ChestDetailsPopUpUI : public PopUpUI {
public:
  virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) override;
  virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event) override;
  void SetSelectedChestSlot(ChestSlot *chestSlot);
  void Update();
  CREATE_FUNC(ChestDetailsPopUpUI);
  
private:
  void PopUpInit() override;
  void CloneChestImage();
  void OpenWithGems(cocos2d::Ref *sender);
  void StartUnlockTimer(cocos2d::Ref *sender);
  void UpdateOpenWithGemsButton();
  void DisplayRemainingTimeLabel();
  void DisplayStartUnlockTimerButton();

  cocos2d::Label *timeRemainingLabel;
  Chest *selectedChest;
  ChestSlot *selectedChestSlot;
  cocos2d::MenuItemImage *openWithGemsButton;
  cocos2d::MenuItemImage *startUnlockTimerButton;
  cocos2d::Label *openWithGemsCostLabel;
  cocos2d::Label *startUnlockTimeLabel;
  cocos2d::Label *startUnlockTimerButtonLabel;
};

#endif
