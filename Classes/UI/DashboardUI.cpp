// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/DashboardUI.h"
#include "UI/ShopUI.h"
#include "Engine/Singleton/EngineSingleton.h"
#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#endif
#include "UI/ResourceDisplayUI.h"
#include "UI/CardCollectionUI.h"

USING_NS_CC;

const std::string kChestSlotScreenName = "chestSlotScreen";

using namespace std;

Scene* DashboardUI::createScene() {
  // 'scene' is an autorelease object
  auto scene = Scene::create();
  
  // 'layer' is an autorelease object
  auto layer = DashboardUI::create();

  // add layer as a child to scene
  scene->addChild(layer);

  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool DashboardUI::init() {
  CCLOG("DashboardUI::init()");
  //////////////////////////////
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }
  
  if(!ControllerLocator::GetAudioController()->IsBackgroundMusicPlaying()){
    ControllerLocator::GetAudioController()->PlayBackgroundMusic(kDashboardBackgroundMusic);
  }
  
  visibleSize = Director::getInstance()->getWinSize();
  origin = Director::getInstance()->getVisibleOrigin();
  auto screenCentre = cocos2d::Vec2(visibleSize.width / 2, visibleSize.height * 0.5f);
  
  auto sprite = Sprite::create(kDashboardUIBackgroundImage);
  sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
  this->addChild(sprite, 0);
  
  messageLabel = Label::createWithTTF("Message", kFontType, kFontSizeSmall);
  messageLabel->setPosition(visibleSize.width / 2, visibleSize.height * 0.7f);
  messageLabel->setColor(cocos2d::Color3B::WHITE);
  messageLabel->setVisible(false);
  addChild(messageLabel);
  
  // Resource label container
  resourceLabelContainer = ResourceDisplayUI::create();
  resourceLabelContainer->setPosition(screenCentre.x, visibleSize.height - kPaddingLarge);
  this->addChild(resourceLabelContainer);

  // Play button
  auto itemPlayLabel = cocos2d::Label::createWithTTF("PLAY", kFontType, kFontSizeLarge);
  itemPlayLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  playButtonLabel = MenuItemLabel::create(itemPlayLabel, CC_CALLBACK_1(DashboardUI::Play, this));
  playButtonLabel->setColor(cocos2d::Color3B::WHITE);
  playButtonLabel->setEnabled(true);
  
  // Shop button
  auto shopButtonLabel = cocos2d::Label::createWithTTF("SHOP", kFontType, kFontSizeMedium);
  shopButtonLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  auto shopButton = MenuItemLabel::create(shopButtonLabel, CC_CALLBACK_1(DashboardUI::Shop, this));
  shopButton->setColor(cocos2d::Color3B::WHITE);
  shopButton->setEnabled(true);
  
  // Delete button
  auto itemDeleteLabel = cocos2d::Label::createWithTTF("DELETE GAME SAVE", kFontType, kFontSizeMedium);
  itemDeleteLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  deleteButtonLabel = MenuItemLabel::create(itemDeleteLabel, CC_CALLBACK_1(DashboardUI::Delete, this));
  deleteButtonLabel->setColor(cocos2d::Color3B::WHITE);
  deleteButtonLabel->setEnabled(true);
  
  // Quit button
  auto itemQuitLabel = cocos2d::Label::createWithTTF("QUIT", kFontType, kFontSizeMedium);
  itemQuitLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  quitButtonLabel = MenuItemLabel::create(itemQuitLabel, CC_CALLBACK_1(DashboardUI::Quit, this));
  quitButtonLabel->setColor(cocos2d::Color3B::WHITE);
  quitButtonLabel->setEnabled(true);
  
  auto viewCardCollectionLabel = cocos2d::Label::createWithTTF("Cards", kFontType, kFontSizeMedium);
  viewCardCollectionLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  viewCardCollectionButtonLabel = MenuItemLabel::create(viewCardCollectionLabel, CC_CALLBACK_1(DashboardUI::CardCollection, this));
  viewCardCollectionButtonLabel->setColor(cocos2d::Color3B::WHITE);
  viewCardCollectionButtonLabel->setEnabled(true);

  //Menu *menu = Menu::create(playButtonLabel, shopButton, deleteButtonLabel, quitButtonLabel, viewCardCollectionButtonLabel, nullptr);
  Menu *menu = Menu::create(playButtonLabel, shopButton, viewCardCollectionButtonLabel, quitButtonLabel, nullptr);
  menu->alignItemsVerticallyWithPadding(30);
  menu->setPosition(visibleSize.width / 2, visibleSize.height * 0.6f);
  addChild(menu);
  
  chestSlotUI = ChestSlotUI::create();
  chestSlotUI->setName(kChestSlotScreenName);
  this->addChild(chestSlotUI);
  
  sdkbox::PluginGoogleAnalytics::logScreen("Dashboard UI");
  //ServiceLocator::GooglePlayService()->PlayerStats();
  this->scheduleUpdate();
  return true;
}

void DashboardUI::menuCloseCallback(Ref* pSender) {
  //Close the cocos2d-x game scene and quit the application
  Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
  exit(0);
#endif

  /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

  //EventCustom customEndEvent("game_scene_close_event");
  //_eventDispatcher->dispatchEvent(&customEndEvent);
}

void DashboardUI::Shop(cocos2d::Ref *sender) {
  auto shop = ShopUI::createScene();
  cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.0f, shop, cocos2d::Color3B::BLACK));
}

void DashboardUI::Play(cocos2d::Ref *sender) {
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  DeactivatePlayButton();
  
  std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::MENU_BUTTON_QUICK_MATCH_START);
  ControllerLocator::GetInputController()->ProcessInputEvent(gameEvent.get());
}

void DashboardUI::Delete(cocos2d::Ref *sender) {
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  ControllerLocator::GetDaoController()->DeleteGameSave();
}

void DashboardUI::Quit(cocos2d::Ref *sender) {
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  ControllerLocator::GetAudioController()->StopBackgroundMusic(kDashboardBackgroundMusic);
  
  cocos2d::Director::getInstance()->end();
}

void DashboardUI::CardCollection(cocos2d::Ref *sender) {
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  auto cardCollectionUI = CardCollectionUI::createScene();
  cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.0f, cardCollectionUI, cocos2d::Color3B::BLACK));
}


void DashboardUI::ActivatePlayButton() {
  if(!playButtonLabel->isEnabled()){
    CCLOG("DashboardUI::ActivatePlayButton() play button activated");
    playButtonLabel->setEnabled(true);
    playButtonLabel->setString("PLAY");
  }
}

void DashboardUI::update(float dt) {
  EngineSingleton::GetInstance()->Update(dt);
}

void DashboardUI::DeactivatePlayButton() {
  if(playButtonLabel->isEnabled()){
    CCLOG("DashboardUI::DeactivatePlayButton() play button deactivated");
    playButtonLabel->setString("Loading...");
    playButtonLabel->setEnabled(false);
  }
}

void DashboardUI::onEnterTransitionDidFinish() {
}

void DashboardUI::OnNotify(GameEvent *event){
  switch (event->Type()) {
    case GameEvent::GPS_ERROR_WAIT_REPONSE:
      ActivatePlayButton();
      break;
    default:
      break;
  }
}
