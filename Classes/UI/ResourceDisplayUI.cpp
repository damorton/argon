// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/ResourceDisplayUI.h"
#include "Engine/Utils/Utils.h"
#include "Engine/Locator/UILocator.h"
#include "Engine/Locator/ControllerLocator.h"
#include "Engine/Utils/GameEvent.h"

ResourceDisplayUI::~ResourceDisplayUI() {
  
}

bool ResourceDisplayUI::init() {
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }
  
  visibleSize = Director::getInstance()->getVisibleSize();
  
  // Star label
  auto starValueBackground = cocos2d::Sprite::create(kPlayerResourceBackgroundSmallFilename);
  starValueBackground->setPositionX(-(visibleSize.width * 0.32f));
  this->addChild(starValueBackground);
  
  starSprite = cocos2d::Sprite::create(kStarFilename);
  starSprite->setScale(0.5f);
  starSprite->setPositionY(starValueBackground->getContentSize().height / 2);
  starValueBackground->addChild(starSprite);
  
  starLabel = cocos2d::Label::createWithTTF("0", kFontType, kFontSizeSmallPlus);
  starLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  //starLabel->setPosition(starValueBackground->getPosition());
  starValueBackground->addChild(starLabel);
  
  // Gold label
  cocos2d::Sprite *goldValueBackground = cocos2d::Sprite::create(kPlayerResourceBackgroundMediumFilename);
  goldValueBackground->setPositionX(-(visibleSize.width * 0.5f) * 0.05f);
  this->addChild(goldValueBackground);
  
  goldSprite = cocos2d::Sprite::create(kGoldCoinFilename);
  goldSprite->setScale(0.5f);
  goldSprite->setPosition(goldValueBackground->getContentSize().width, goldValueBackground->getContentSize().height / 2);
  goldValueBackground->addChild(goldSprite);
  
  goldLabel = cocos2d::Label::createWithTTF("0", kFontType, kFontSizeSmallPlus);
  goldLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  //goldLabel->setPosition(goldValueBackground->getPosition());
  goldValueBackground->addChild(goldLabel);
  
  cocos2d::Sprite *goldShopLinkImage = cocos2d::Sprite::create(kPlusSignFilename);
  auto goldShopLink = cocos2d::MenuItemSprite::create(goldShopLinkImage, goldShopLinkImage, CC_CALLBACK_1(ResourceDisplayUI::BuyGold, this));
  goldShopLink->setPositionX(-(visibleSize.width * 0.5f) * 0.28f);
  goldShopLink->setScale(0.5f);
  
  // Gem label
  cocos2d::Sprite *gemValueBackground = cocos2d::Sprite::create(kPlayerResourceBackgroundSmallFilename);
  gemValueBackground->setPositionX((visibleSize.width * 0.5f) * 0.65f);
  this->addChild(gemValueBackground);
  
  gemSprite = cocos2d::Sprite::create(kGemFilename);
  gemSprite->setScale(0.5f);
  gemSprite->setPosition(gemValueBackground->getContentSize().width, gemValueBackground->getContentSize().height * 0.5f);
  gemValueBackground->addChild(gemSprite);
  
  gemLabel = cocos2d::Label::createWithTTF("0", kFontType, kFontSizeSmallPlus);
  gemLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  //gemLabel->setPosition(gemValueBackground->getPosition());
  gemValueBackground->addChild(gemLabel);
  
  cocos2d::Sprite *gemShopLinkImage = cocos2d::Sprite::create(kPlusSignFilename);
  auto gemShopLink = cocos2d::MenuItemSprite::create(gemShopLinkImage, gemShopLinkImage, CC_CALLBACK_1(ResourceDisplayUI::BuyGems, this));
  gemShopLink->setPositionX((visibleSize.width * 0.5f) * 0.40f);
  gemShopLink->setScale(0.5f);
  
  auto shopMenu = cocos2d::Menu::create(gemShopLink, goldShopLink, NULL);
  shopMenu->setPosition(cocos2d::Vec2::ZERO);
  this->addChild(shopMenu);
  
  UpdateResourceLabels();
  
  return true;
}

void ResourceDisplayUI::UpdateResourceLabels() {
  starLabel->setString(Utils::to_str(ControllerLocator::GetConfigController()->ConfigurationValue(kStars)));
  cocos2d::Vec2 starLabelPosition = cocos2d::Vec2(starSprite->getPosition().x + (starSprite->getContentSize().width * 0.3f) + (starLabel->getContentSize().width / 2), starSprite->getPosition().y);
  starLabel->setPosition(starLabelPosition);
  
  goldLabel->setString(Utils::to_str(ControllerLocator::GetConfigController()->ConfigurationValue(kGold)));
  cocos2d::Vec2 goldLabelPosition = cocos2d::Vec2(goldSprite->getPosition().x - (goldSprite->getContentSize().width * 0.3f) - (goldLabel->getContentSize().width / 2), goldSprite->getPosition().y);
  goldLabel->setPosition(goldLabelPosition);
  
  gemLabel->setString(Utils::to_str(ControllerLocator::GetConfigController()->ConfigurationValue(kGems)));
  cocos2d::Vec2 gemPosition = cocos2d::Vec2(gemSprite->getPosition().x - (gemSprite->getContentSize().width * 0.3f) - (gemLabel->getContentSize().width / 2), gemSprite->getPosition().y);
  gemLabel->setPosition(gemPosition);
}

void ResourceDisplayUI::BuyGems(cocos2d::Ref *sender) {
  OpenShop();
}

void ResourceDisplayUI::BuyGold(cocos2d::Ref *sender) {
  OpenShop();
}

void ResourceDisplayUI::OpenShop(){
  if(cocos2d::Director::getInstance()->getRunningScene()->getName() != kShopUIName){
    ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
    auto shop = ShopUI::createScene();
    cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.0f, shop, cocos2d::Color3B::BLACK));
  }
}

void ResourceDisplayUI::OnNotify(GameEvent *event){
  if(event->Type() == GameEvent::EVENT_GAME_MODEL_UPDATED){
    UpdateResourceLabels();
  }
}


