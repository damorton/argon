// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/ResultUI.h"
#include "Engine/Singleton/EngineSingleton.h"
#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#endif
#include "Game/Factory/ChestFactory.h"

Scene* ResultUI::createScene() {
  // 'scene' is an autorelease object
  auto scene = Scene::create();

  // 'layer' is an autorelease object
  auto layer = ResultUI::create();

  // add layer as a child to scene
  scene->addChild(layer);

  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool ResultUI::init() {
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }

  auto visibleSize = Director::getInstance()->getVisibleSize();
  Vec2 origin = Director::getInstance()->getVisibleOrigin();
  cocos2d::Vec2 screenCentre(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);

  // Register for touch events
  auto touchListener = EventListenerTouchOneByOne::create();
  touchListener->setSwallowTouches(true);
  touchListener->onTouchBegan = CC_CALLBACK_2(ResultUI::onTouchBegan, this);
  touchListener->onTouchEnded = CC_CALLBACK_2(ResultUI::onTouchEnded, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
  
  // Background image
  auto sprite = Sprite::create(kGreyTransparentBackground);
  sprite->setPosition(screenCentre);
  this->addChild(sprite, 0);

  resultsLabel = Label::createWithTTF("Result", kFontType, kFontSizeLarge);
  resultsLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  this->addChild(resultsLabel);

  if (ControllerLocator::GetGameController()->HasLocalPlayerWon()) {
    resultsLabel->setString("You Win!");
    resultsLabel->setPosition(origin.x + visibleSize.width / 2, origin.y + (visibleSize.height * 0.80f));
    
    ControllerLocator::GetAudioController()->PlaySoundEffect(kOpenChestSoundEffect);
    
    unsigned long currentNumberOfChests = EngineSingleton::GetInstance()->Chests()->size();
    if(currentNumberOfChests < kMaxNumberOfChests) {
      rewardChest = ChestFactory::Create(Chest::CHEST_SILVER, EngineSingleton::GetInstance()->GetTimestamp());
      auto tempRewardChest = cocos2d::Sprite::createWithSpriteFrame(rewardChest->Sprite()->getSpriteFrame());
      tempRewardChest->setPosition(screenCentre);
      this->addChild(tempRewardChest);
      EngineSingleton::GetInstance()->AddChest(rewardChest);
      
      std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::EVENT_SAVE_GAME_CONFIG);
      EngineSingleton::GetInstance()->Notify(gameEvent.get());
    }
    
  } else {
    resultsLabel->setString("You Lose");
    resultsLabel->setPosition(origin.x + visibleSize.width / 2, origin.y + (visibleSize.height * 0.50f));
  }
  
  return true;
}

bool ResultUI::onTouchBegan(Touch* touch, Event* event) {
  return true;
}

void ResultUI::onTouchEnded(Touch* touch, Event* event) {
  CloseResult(this);
}

void ResultUI::CloseResult(cocos2d::Ref *sender) {
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  EngineSingleton::GetInstance()->EndGameSession();
  ControllerLocator::GetAudioController()->PlayBackgroundMusic(kDashboardBackgroundMusic);
}
