// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/LoadingUI.h"
#include "UI/LoadingSpinnerUI.h"
#include "Engine/Singleton/EngineSingleton.h"

Scene* LoadingUI::createScene() {
  // 'scene' is an autorelease object
  auto scene = Scene::create();
  
  // 'layer' is an autorelease object
  auto layer = LoadingUI::create();
  
  // add layer as a child to scene
  scene->addChild(layer);
  
  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool LoadingUI::init() {
  CCLOG("LoadingUI::init()");
  //////////////////////////////
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }
  
  UILocator::Register(this);
  
  auto visibleSize = Director::getInstance()->getVisibleSize();
  Vec2 origin = Director::getInstance()->getVisibleOrigin();
  Size size = Director::getInstance()->getWinSize();
  Vec2 screenCenter(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);

  // Background image
  auto sprite = Sprite::create(kLoadingUIBackgroundImage);
  sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
  this->addChild(sprite, 0);
  
  // Register for touch events
  auto touchListener = EventListenerTouchOneByOne::create();
  touchListener->onTouchBegan = CC_CALLBACK_2(LoadingUI::onTouchBegan, this);
  touchListener->setSwallowTouches(true); // Do not propogate touch event to lower layers
  _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

  // Labels
  loadingMessage = Label::createWithTTF("", kFontType, kFontSizeMedium);
  loadingMessage->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  loadingMessage->setPosition(screenCenter);
  addChild(loadingMessage);

  // Add the loading spinner directly instead of using the event system becuase the first running
  // scene will be this one and the spinner is added to the Directors running scene
  auto loadingSpinner = LoadingSpinnerUI::create();
  this->addChild(loadingSpinner);
  
  this->scheduleUpdate();
  return true;
}

bool LoadingUI::onTouchBegan(cocos2d::Touch*, cocos2d::Event*) {
  CCLOG("Loading screen touch event consumed");
  return true;
}

void LoadingUI::OnNotify(GameEvent *event){
  switch (event->Type()) {
    case GameEvent::EVENT_GAME_CONFIG_LOADED:
      if(ServiceLocator::GooglePlayService()->IsSignedIn()){
        auto dashboardUI = DashboardUI::createScene();
        cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.0f, dashboardUI, cocos2d::Color3B::BLACK));
      }
      break;
    default:
      break;
  }
}

void LoadingUI::update(float dt) {
  EngineSingleton::GetInstance()->Update(dt);
}
