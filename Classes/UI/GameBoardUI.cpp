// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "UI/GameBoardUI.h"
#include "Engine/Singleton/EngineSingleton.h"
#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#endif
#include "Engine/Factory/GameEventFactory.h"

USING_NS_CC;

Scene* GameBoardUI::createScene() {
  // 'scene' is an autorelease object
  auto scene = Scene::create();

  // 'layer' is an autorelease object
  auto layer = GameBoardUI::create();

  // add layer as a child to scene
  scene->addChild(layer);

  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool GameBoardUI::init() {
  CCLOG("GameBoardUI::init()");
  //////////////////////////////
  // 1. super init first
  if (!Layer::init()) {
    return false;
  }

  ControllerLocator::GetAudioController()->PlayBackgroundMusic(kGameworldBackgroundMusic);

  // Register for touch events
  auto touchListener = EventListenerTouchOneByOne::create();
  touchListener->onTouchBegan = CC_CALLBACK_2(GameBoardUI::onTouchBegan, this);
  touchListener->onTouchMoved = CC_CALLBACK_2(GameBoardUI::onTouchMoved, this);
  touchListener->onTouchEnded = CC_CALLBACK_2(GameBoardUI::onTouchEnded, this);
  _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

  auto visibleSize = Director::getInstance()->getVisibleSize();
  Vec2 origin = Director::getInstance()->getVisibleOrigin();
  cocos2d::Vec2 screenCentre(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);
  Size size = Director::getInstance()->getWinSize();

  // 'layer' is an autorelease object
  auto hudUI = HudUI::create();
  this->addChild(hudUI, 5);

  // Background image
  auto sprite = Sprite::create(kGameboardUIBackgroundImage);
  sprite->setPosition(screenCentre);
  this->addChild(sprite);
  
  cocos2d::ParticleSystemQuad *particleSystem = cocos2d::ParticleSystemQuad::create(kGameboardBackgroundParticleEffect);
  particleSystem->setPosition(screenCentre);
  this->addChild(particleSystem);

  auto quitLabel = cocos2d::Label::createWithTTF("Surrender", kFontType, kFontSizeMedium);
  quitLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  quitButtonLabel = cocos2d::MenuItemLabel::create(quitLabel, CC_CALLBACK_1(GameBoardUI::QuitGame, this));

  Menu *menu = Menu::create(quitButtonLabel, nullptr);
  menu->alignItemsVerticallyWithPadding(10);
  menu->setPosition(Vec2((visibleSize.width / 2.0f) + origin.x, (visibleSize.height * 0.9f) + origin.y));
  addChild(menu);

  this->scheduleUpdate();
  this->schedule(schedule_selector(GameBoardUI::ClockTimer), 1.0f);
  
  sdkbox::PluginGoogleAnalytics::logScreen("Gameboard UI");
  sdkbox::PluginGoogleAnalytics::logEvent("Session", "Start", "Game session started", 1);
  sdkbox::PluginGoogleAnalytics::logEvent("User", "Start", ServiceLocator::GooglePlayService()->GooglePlayUsername(), 1);
  return true;
}

void GameBoardUI::update(float dt) {
  EngineSingleton::GetInstance()->Update(dt);
}

void GameBoardUI::ClockTimer(float dt) {
  EngineSingleton::GetInstance()->ClockTimer();
}

void GameBoardUI::QuitGame(cocos2d::Ref *sender) {
  ControllerLocator::GetAudioController()->PlaySoundEffect(kMenuSelectSoundEffect);
  
  ControllerLocator::GetGameController()->LocalPlayerQuitGame();
  sdkbox::PluginGoogleAnalytics::logEvent("User", "Quit", ServiceLocator::GooglePlayService()->GooglePlayUsername(), 1);
}

bool GameBoardUI::onTouchBegan(Touch* touch, Event* event) {
  cocos2d::Vec2 touchCoordinates = touch->getLocation();
  cocos2d::Vec2 touchLocation = convertToNodeSpace(touchCoordinates);
  ControllerLocator::GetInputController()->SetTouchLocation(touchLocation.x, touchLocation.y);
  std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::TOUCH_INPUT_GAME_BOARD_BEGIN, touchLocation.x, touchLocation.y);
  ControllerLocator::GetInputController()->ProcessInputEvent(gameEvent.get());
  return true;
}

void GameBoardUI::onTouchMoved(Touch* touch, Event* event) {
  cocos2d::Vec2 touchCoordinates = touch->getLocation();
  cocos2d::Vec2 touchLocation = convertToNodeSpace(touchCoordinates);
  ControllerLocator::GetInputController()->SetTouchLocation(touchLocation.x, touchLocation.y);
}

void GameBoardUI::onTouchEnded(Touch* touch, Event* event) {
  cocos2d::Vec2 touchCoordinates = touch->getLocation();
  cocos2d::Vec2 touchLocation = convertToNodeSpace(touchCoordinates);
  std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::TOUCH_INPUT_GAME_BOARD_END, touchLocation.x, touchLocation.y);
  ControllerLocator::GetInputController()->ProcessInputEvent(gameEvent.get());
}
