// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/Audio/Audio.h"

class SimpleAudioEngineWrapper : public Audio {
public:
  SimpleAudioEngineWrapper();
  virtual ~SimpleAudioEngineWrapper();
  virtual int PlaySoundEffect(const std::string &soundEffect) override;
  virtual void StopSoundEffect(unsigned int soundEffectId) override;
  virtual void StopAllSoundEffects() override;
  virtual void PlayBackgroundMusic(const std::string &music) override;
  virtual void StopBackgroundMusic(const std::string &music) override;
  virtual bool IsBackgroundMusicPlaying() override;
};
