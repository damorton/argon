// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include <string>

class Audio {
public:
  virtual int PlaySoundEffect(const std::string &soundEffect) = 0;
  virtual void StopSoundEffect(unsigned int soundEffectId) = 0;
  virtual void StopAllSoundEffects() = 0;
  virtual void PlayBackgroundMusic(const std::string &music) = 0;
  virtual void StopBackgroundMusic(const std::string &music) = 0;
  virtual bool IsBackgroundMusicPlaying() = 0;
};
