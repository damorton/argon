// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "SimpleAudioEngine.h"
#include "Engine/Audio/SimpleAudioEngineWrapper.h"

#define AUDIO_ON

SimpleAudioEngineWrapper::SimpleAudioEngineWrapper() {
}

SimpleAudioEngineWrapper::~SimpleAudioEngineWrapper() {
}

int SimpleAudioEngineWrapper::PlaySoundEffect(const std::string &soundEffect) {
  int soundId = -1;
#ifdef AUDIO_ON
  soundId = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(soundEffect.c_str());
#endif
  return soundId;
}

void SimpleAudioEngineWrapper::StopSoundEffect(unsigned int soundEffectId) {
#ifdef AUDIO_ON
  CocosDenshion::SimpleAudioEngine::getInstance()->stopEffect(soundEffectId);
#endif
}

void SimpleAudioEngineWrapper::PlayBackgroundMusic(const std::string &music) {
#ifdef AUDIO_ON
  CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(music.c_str(), true);
#endif
}

void SimpleAudioEngineWrapper::StopBackgroundMusic(const std::string &music) {
#ifdef AUDIO_ON
  CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
#endif
}

void SimpleAudioEngineWrapper::StopAllSoundEffects() {
#ifdef AUDIO_ON
  CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
#endif
}

bool SimpleAudioEngineWrapper::IsBackgroundMusicPlaying() {
#ifdef AUDIO_ON
  return CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying();
#endif
  return false;
}
