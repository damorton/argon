// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/Locator/UILocator.h"

RewardUI *UILocator::m_pRewardUI = nullptr;
LoadingUI *UILocator::m_pLoadingUI = nullptr;

void UILocator::Register(RewardUI *rewardUI) {
  m_pRewardUI = rewardUI;
}

void UILocator::Register(LoadingUI *loadingUI) {
  m_pLoadingUI = loadingUI;
}

RewardUI *UILocator::GetRewardUI() {
  return m_pRewardUI;
}

LoadingUI *UILocator::GetLoadingUI() {
  return m_pLoadingUI;
}
