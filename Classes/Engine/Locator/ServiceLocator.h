/// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ServiceLocator_hpp
#define ServiceLocator_hpp

#include "../Service/GameService/GameServiceInterface.h"
#include "Engine/Service/Shop/GameStore.h"

class ServiceLocator {
public:
  virtual ~ServiceLocator();
  static void Register(std::unique_ptr<GameServiceInterface> networkController);
  static void Register(std::unique_ptr<ShopInterface> gameStore);
  static GameServiceInterface *GooglePlayService();
  static ShopInterface *GameStore();
private:
  static std::unique_ptr<GameServiceInterface> googlePlayService;
  static std::unique_ptr<ShopInterface> gameStoreService;


};

#endif
