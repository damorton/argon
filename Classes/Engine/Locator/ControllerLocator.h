// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ENGINE_LOCATOR_CONTROLLERLOCATOR_H_
#define ENGINE_LOCATOR_CONTROLLERLOCATOR_H_

#include <memory>

#include "Engine/Audio/SimpleAudioEngineWrapper.h"
#include "Engine/Controller/GameController.h"
#include "Engine/Controller/InputController.h"
#include "Engine/Controller/ConfigController.h"
#include "Engine/Controller/DaoController.h"

class ControllerLocator {
public:
  virtual ~ControllerLocator();
  
  // Registers the game controllers
  static void Register(std::unique_ptr<GameController> gameController);
  static void Register(std::unique_ptr<InputController> inputController);
  static void Register(std::unique_ptr<ConfigController> configController);
  static void Register(std::unique_ptr<DaoController> configController);
  static void Register(std::unique_ptr<Audio> audioController);

  // Returns the game controllers
  static ConfigController *GetConfigController();
  static InputController *GetInputController();
  static GameController *GetGameController();
  static DaoController *GetDaoController();
  static Audio *GetAudioController();
  
  // Resets the game controller instance
  static void ResetGameControllerService();
private:
  static std::unique_ptr<GameController> gameControllerService;
  static std::unique_ptr<InputController> inputControllerService;
  static std::unique_ptr<ConfigController> configControllerService;
  static std::unique_ptr<DaoController> daoControllerService;
  static std::unique_ptr<Audio> audioControllerService;
};

#endif
