// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/Locator/ControllerLocator.h"

std::unique_ptr<GameController> ControllerLocator::gameControllerService(nullptr);
std::unique_ptr<InputController> ControllerLocator::inputControllerService(nullptr);
std::unique_ptr<ConfigController> ControllerLocator::configControllerService(nullptr);
std::unique_ptr<DaoController> ControllerLocator::daoControllerService(nullptr);
std::unique_ptr<Audio> ControllerLocator::audioControllerService(nullptr);

ControllerLocator::~ControllerLocator() {
  gameControllerService.reset();
  inputControllerService.reset();
  configControllerService.reset();
  daoControllerService.reset();
  audioControllerService.reset();
}

// Game controller service
GameController *ControllerLocator::GetGameController() {
  return gameControllerService.get();
}

void ControllerLocator::Register(std::unique_ptr<GameController> gameController) {
  gameControllerService = std::move(gameController);
}

void ControllerLocator::ResetGameControllerService() {
  gameControllerService.reset();
}

// Input controller
InputController *ControllerLocator::GetInputController() {
  return inputControllerService.get();
}

void ControllerLocator::Register(std::unique_ptr<InputController> inputController) {
  inputControllerService = std::move(inputController);
}

// Config controller
ConfigController *ControllerLocator::GetConfigController() {
  return configControllerService.get();
}

void ControllerLocator::Register(std::unique_ptr<ConfigController> configController) {
  configControllerService = std::move(configController);
}

// Dao controller
DaoController *ControllerLocator::GetDaoController() {
  return daoControllerService.get();
}

void ControllerLocator::Register(std::unique_ptr<DaoController> daoController) {
  daoControllerService = std::move(daoController);
}

Audio *ControllerLocator::GetAudioController() {
  return audioControllerService.get();
}

void ControllerLocator::Register(std::unique_ptr<Audio> audioController) {
  audioControllerService = std::move(audioController);
}
