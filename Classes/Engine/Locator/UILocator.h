// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef LOCATOR_UI_LOCATOR_H_
#define LOCATOR_UI_LOCATOR_H_

#include "UI/DashboardUI.h"
#include "UI/CardSelectUI.h"
#include "UI/ShopUI.h"
#include "UI/GameBoardUI.h"
#include "UI/ResultUI.h"
#include "UI/RewardUI.h"
#include "UI/HudUI.h"
#include "UI/LoadingUI.h"
#include "UI/CardSelectWaitUI.h"

class UILocator {
public:
  static void Register(RewardUI *rewardUI);
  static void Register(LoadingUI *loadingUI);

  static RewardUI *GetRewardUI();
  static LoadingUI *GetLoadingUI();
  
private:
  static RewardUI *m_pRewardUI;
  static LoadingUI *m_pLoadingUI;
};

#endif
