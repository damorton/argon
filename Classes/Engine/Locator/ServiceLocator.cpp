// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/Locator/ServiceLocator.h"

std::unique_ptr<GameServiceInterface> ServiceLocator::googlePlayService(nullptr);
std::unique_ptr<ShopInterface> ServiceLocator::gameStoreService(nullptr);


ServiceLocator::~ServiceLocator() {
  googlePlayService.reset();
}

void ServiceLocator::Register(std::unique_ptr<GameServiceInterface> networkController) {
  googlePlayService = std::move(networkController);
}

void ServiceLocator::Register(std::unique_ptr<ShopInterface> gameStore) {
  gameStoreService = std::move(gameStore);
}

GameServiceInterface *ServiceLocator::GooglePlayService() {
  return googlePlayService.get();
}

ShopInterface *ServiceLocator::GameStore() {
  return gameStoreService.get();
}
