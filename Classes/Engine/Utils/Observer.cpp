#include "Engine/Utils/Observer.h"
#include "Engine/Singleton/EngineSingleton.h"

Observer::Observer(){
  RegisterWithSubject();
}

void Observer::RegisterWithSubject() {
  EngineSingleton::GetInstance()->AddObserver(this);
}

Observer::~Observer(){
  EngineSingleton::GetInstance()->RemoveObserver(this);
}
