// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef BASE_UTILS_H
#define BASE_UTILS_H

#include <sstream>
#include <vector>

class Utils {
public:
  
  // Converts data to string
  template<typename T>
  static std::string to_str(T Number) {
    std::ostringstream ss;
    ss << Number;
    return ss.str();
  }
  
  template<typename T>
  static int to_int(T string) {
    std::istringstream ss(string);
    int num;
    ss >> num;
    return num;
  }

  // Converts vector of bytes to string
  static std::string vec_to_string(const std::vector<uint8_t>& v) {
    return std::string(v.begin(), v.end());
  }

  // Converts string to a vector of bytes
  static std::vector<uint8_t> str_to_vector(const std::string& str) {
    return std::vector < uint8_t > (str.begin(), str.end());
  }
  
  static int milli_to_sec(unsigned long long milliseconds) {
    unsigned long long seconds = 0;
    seconds = milliseconds / 1000;
    return (int)seconds;
  }
  
  static int seconds_to_hours(int seconds) {
    return seconds_to_minutes(seconds) / 60;
  }
  
  static int seconds_to_minutes(int seconds) {
    return seconds / 60;
  }
  
  static std::string seconds_to_time_format(int seconds) {
    std::string timeFormatted = "";
    int hours = Utils::seconds_to_hours(seconds);
    int minutes = Utils::seconds_to_minutes(seconds) % 60;
    int secs = seconds % 60;
    bool skipSeconds = false;
    if (hours > 0) {
      timeFormatted = Utils::to_str(hours) + "h ";
      skipSeconds = true;
    }
    if(minutes > 0) {
      timeFormatted += Utils::to_str(minutes) + "min ";
    }
    if(secs > 0 && !skipSeconds) {
      timeFormatted += Utils::to_str(secs) + "secs";
    }
    return timeFormatted;
  }
};
#endif
