// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ENGINE_UTILS_GAME_EVENT_H_
#define ENGINE_UTILS_GAME_EVENT_H_

class GameEvent {
public:
  enum EventType {
    END_GAME_RESOLVE_CONFLICTS,
    TOUCH_INPUT_GAME_BOARD_BEGIN,
    TOUCH_INPUT_GAME_BOARD_END,
    TOUCH_INPUT_CARD_SELECT,
    TOUCH_INPUT_SHOP_SELECT,
    MENU_BUTTON_QUICK_MATCH_START,
    MENU_BUTTON_START_GAME,
    CLIENT_PLAYER_READY_TO_START,
    CLIENT_PLAYER_QUIT_GAME,
    HOST_GAME_START,
    HOST_SYNC_GAME_MODEL,
    EVENT_GAME_MODEL_UPDATED,
    EVENT_GAME_CONFIG_LOADED,
    EVENT_SAVE_GAME_CONFIG,
    EVENT_SAVE_GAME_COMPLETE,
    GPS_ERROR_WAIT_REPONSE,
    CARD_SELECTION_DECK_NOT_FULL,
    CARD_SELECTION_DECK_FULL,
    GAME_SESSION_END
  };
  
  GameEvent(EventType eventType, float positionX, float positionY);
  virtual ~GameEvent();
  void Init();
  EventType Type();
  float PositionX();
  float PositionY();
  
private:
  EventType type;
  float posX;
  float posY;
};

#endif
