// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/Utils/GameEvent.h"

GameEvent::GameEvent(EventType eventType, float positionX, float positionY) {
  type = eventType;
  posX = positionX;
  posY = positionY;
  Init();
}

GameEvent::~GameEvent() {

}

void GameEvent::Init(){
  
}

GameEvent::EventType GameEvent::Type(){
  return type;
}

float GameEvent::PositionX(){
  return posX;
}

float GameEvent::PositionY(){
  return posY;
}
