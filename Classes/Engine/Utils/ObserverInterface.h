// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ENGINE_UTILS_OBSERVER_INTERFACE_H_
#define ENGINE_UTILS_OBSERVER_INTERFACE_H_

#include <memory>

class GameEvent;

class ObserverInterface {
public:
  virtual void OnNotify(GameEvent *event) = 0;

protected:
  virtual ~ObserverInterface(){}
};

#endif
