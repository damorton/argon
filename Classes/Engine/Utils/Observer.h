// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ENGINE_UTILS_OBSERVER_H_
#define ENGINE_UTILS_OBSERVER_H_

#include "Engine/Utils/ObserverInterface.h"

class Observer : public ObserverInterface {
public:
  Observer();
  virtual ~Observer();
  virtual void RegisterWithSubject();
};

#endif
