// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ENGINE_UTILS_SUBJECT_H_
#define ENGINE_UTILS_SUBJECT_H_

#include <vector>

#include "Engine/Utils/Observer.h"

class Subject {
public:
  void Notify(GameEvent *event) {
    for (size_t i = 0; i < observers.size(); i++) {
      observers[i]->OnNotify(event);
    }
  }
  
  void AddObserver(Observer* observer) {
    observers.push_back(observer);
  }
  
  void RemoveObserver(Observer* observer) {
    for (size_t i = 0; i < observers.size(); i++) {
      if (observers[i] == observer) {
        observers.erase(observers.begin() + i);
      }
    }
  }
  
private:
  std::vector<ObserverInterface*> observers;
};

#endif
