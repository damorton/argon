// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/Controller/DaoController.h"
#include "Engine/Singleton/EngineSingleton.h"

DaoController::DaoController() {
}

void DaoController::SaveGame(rapidjson::Document &gameSaveData) {
  ServiceLocator::GooglePlayService()->SaveGame(kSaveGameName, kSaveGameDescription, JsonParser::JsonToString(gameSaveData), gpg::SnapshotConflictPolicy::MOST_RECENTLY_MODIFIED);
}

void DaoController::SaveToLocalFile(rapidjson::Document &gameSaveData) {
  std::string writablePath = cocos2d::FileUtils::getInstance()->getWritablePath();
  writablePath += kGameConfigurationFile;
  CCLOG("Saving to local file");
  JsonParser::JsonToFile(gameSaveData, writablePath);
}

void DaoController::ListGameSaves() {
  ServiceLocator::GooglePlayService()->ListGameSaves();
}

void DaoController::DeleteGameSave() {
  ServiceLocator::GooglePlayService()->DeleteGameSave();
}

void DaoController::LoadGame(const std::string &filename) {
  ServiceLocator::GooglePlayService()->LoadGame(kSaveGameName);
}

void DaoController::OnNotify(GameEvent *event){
  
}
