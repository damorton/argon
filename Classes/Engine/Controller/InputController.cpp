// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/Controller/InputController.h"
#include "Engine/Singleton/EngineSingleton.h"

void InputController::ProcessInputEvent(GameEvent *event) {
  EngineSingleton::GetInstance()->ProcessEvent(event);
}

void InputController::SetTouchLocation(float x, float y) {
  touchLocationX = x;
  touchLocationY = y;
}

cocos2d::Vec2 InputController::GetTouchLocation() {
  return cocos2d::Vec2(touchLocationX, touchLocationY);
}

void InputController::OnNotify(GameEvent *event){
  
}
