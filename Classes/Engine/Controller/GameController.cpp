// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/Controller/GameController.h"
#include "Engine/Locator/UILocator.h"
#include "Engine/JsonParser/JsonParser.h"
#include "Game/GameSession.h"
#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#endif
#include "Game/Factory/CardFactory.h"
#include "Engine/Singleton/EngineSingleton.h"

GameController::~GameController() {
  CCLOG("GameController dtor");
  gameBoard.reset();
  playerCardDeck.clear();
}

GameController::GameController() {
  std::unique_ptr<GameSession> gameSession(new GameSession());
  cardSelected = false;
  selectedCard = nullptr;
  localPlayerQuit = false;
  clientPlayerQuit = false;
  gameSessionEnded = false;
  numberOfPlayersReady = 0;
  conflictResolutionAttempts = 0;
  gameTimeSeconds = kGameSessionTimeSeconds;
  stopClockTimer = false;
  gameOver = false;
  winningCardColor = CardColor::CARD_COLOR_BLANK;
  selectedCardIndex = -1; // no card selected
  cardCollection = EngineSingleton::GetInstance()->GetCardCollection();
  SetGameState(GameState::GAME_INITIALISED);
}

void GameController::ClockTimer() {
  if(GetGameState() == GameState::GAME_IN_PROGRESS) {
    gameTimeSeconds--;
    if(gameTimeSeconds <= 0) {
      std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::GAME_SESSION_END);
      EngineSingleton::GetInstance()->Notify(gameEvent.get());
      gameTimeSeconds = 0;
    }
  }
}

int GameController::GetGameTimeSeconds() {
  return gameTimeSeconds;
}

void GameController::StopGameClockTimer() {
  stopClockTimer = true;
}

bool GameController::IsGameTimerStopped(){
  return stopClockTimer;
}

void GameController::ProcessCardSelectTouchInput(float touchInputX, float touchInputY) {
  for (auto card : *cardCollection->UnlockedCards()) {
    if (card->GetSprite()->getBoundingBox().containsPoint(cocos2d::Vec2(touchInputX, touchInputY))) {
      if (!card->IsSelected()) {
        if (playerCardDeck.size() < kMaxNumberOfCardsInDeck) {
          playerCardDeck.push_back(card->GetCardData());
          card->SetHighlighted(true);
          CCLOG("Card added to deck");
          ControllerLocator::GetAudioController()->PlaySoundEffect(kCardSelectSoundEffect);
          break;
        } else {
          CCLOG("Maximum number of cards selected for deck");
          break;
        }
      } else {
        playerCardDeck.erase(std::remove_if(playerCardDeck.begin(),
                                            playerCardDeck.end(),
                                            [&](Card::CardData const &cardDataRef){
                                              return cardDataRef.cardName == card->GetCardName();
                                            }), playerCardDeck.end());
        card->SetHighlighted(false);
        CCLOG("Card removed from deck");
        ControllerLocator::GetAudioController()->PlaySoundEffect(kCardSelectSoundEffect);
        break;
      }
    }
  }
  
  if (playerCardDeck.size() == kMaxNumberOfCardsInDeck) {
    std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::CARD_SELECTION_DECK_NOT_FULL);
    EngineSingleton::GetInstance()->Notify(gameEvent.get());
  } else {
    std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::CARD_SELECTION_DECK_FULL);
    EngineSingleton::GetInstance()->Notify(gameEvent.get());
  }
}

void GameController::ProcessGameBoardTouchInputBegin(float touchInputX, float touchInputY) {
  if (selectedCardIndex == -1) {
    for (int cardIndex = 0; cardIndex < kMaxNumberOfCardsInDeck; cardIndex++) {
      if (GetCardInDeckAt(cardIndex)->GetSprite()->getBoundingBox().containsPoint(cocos2d::Vec2(touchInputX, touchInputY)) && GetCardInDeckAt(cardIndex)->GetSprite()->isVisible()) {
        SelectCardInDeck(cardIndex);
        ControllerLocator::GetAudioController()->PlaySoundEffect(kCardSelectSoundEffect);
        return;
      }
    }
  }
}

void GameController::ProcessGameBoardTouchInputEnd(float touchInputX, float touchInputY) {
  int selectedCellIndex = CheckTouchInputOnCells(touchInputX, touchInputY);
  
  if (selectedCellIndex < 0 && selectedCardIndex != -1) {
    GetCardInDeckAt(selectedCardIndex)->ResetPositionToDeck();
    GetCardInDeckAt(selectedCardIndex)->SetSelected(false);
    selectedCardIndex = -1;
    selectedCard = nullptr;
  }
  
  if (selectedCellIndex >= 0 && selectedCardIndex >= 0) {
    rapidjson::Document jsonObject = JsonParser::CreateEmptyJsonDocument();
    AddJsonObjectHeader(jsonObject);
    jsonObject.AddMember("event", rapidjson::Value(GameEvent::TOUCH_INPUT_GAME_BOARD_END), jsonObject.GetAllocator());
    UpdateJsonObjectCellIndex(selectedCellIndex, jsonObject);
    UpdateJsonObjectCardSelect(selectedCardIndex, jsonObject);
    
    UpdateGameModelTouchInput(jsonObject);
    
    ControllerLocator::GetAudioController()->PlaySoundEffect(kCardSelectSoundEffect);
    
    if(!EngineSingleton::GetInstance()->IsHost()){
      std::string message = JsonParser::JsonToString(jsonObject);
      SendMessageToHost(message);
    }
  }
}

void GameController::AddJsonObjectHeader(rapidjson::Document &jsonObject) {
  googlePlayServicesUsername = ServiceLocator::GooglePlayService()->GooglePlayUsername();
  rapidjson::Value usernameValue(googlePlayServicesUsername.c_str(), jsonObject.GetAllocator());
  jsonObject.AddMember("username", usernameValue, jsonObject.GetAllocator());
  unsigned long long eventTimestampDelta = EngineSingleton::GetInstance()->GetTimestampDelta();
  jsonObject.AddMember("timestamp_delta", rapidjson::Value(eventTimestampDelta), jsonObject.GetAllocator());
}

void GameController::UpdateJsonObjectCellIndex(int cellIndex, rapidjson::Document &jsonObject) {
  jsonObject.AddMember("cell_index", rapidjson::Value(cellIndex), jsonObject.GetAllocator());
}

void GameController::UpdateJsonObjectCardSelect(int deckIndex, rapidjson::Document &jsonObject) {
  std::string cardName = GetCardInDeckAt(deckIndex)->GetCardName();
  rapidjson::Value cardNameValue(cardName.c_str(), jsonObject.GetAllocator());
  jsonObject.AddMember("card_name", cardNameValue, jsonObject.GetAllocator());
  
  std::string cardFilename = GetCardInDeckAt(deckIndex)->GetFilename();
  rapidjson::Value cardFilenameValue(cardFilename.c_str(), jsonObject.GetAllocator());
  jsonObject.AddMember("card_filename", cardFilenameValue, jsonObject.GetAllocator());
  
  CardColor cardColor = GetCardInDeckAt(deckIndex)->GetCardColor();
  jsonObject.AddMember("card_color", rapidjson::Value(cardColor), jsonObject.GetAllocator());
  
  int attributeTop = gameBoard->GetCardInDeckAt(deckIndex)->GetAttribute(Card::TOP);
  jsonObject.AddMember("card_attribute_top", rapidjson::Value(attributeTop), jsonObject.GetAllocator());
  
  int attributeRight = gameBoard->GetCardInDeckAt(deckIndex)->GetAttribute(Card::RIGHT);
  jsonObject.AddMember("card_attribute_right", rapidjson::Value(attributeRight), jsonObject.GetAllocator());
  
  int attributeBottom = gameBoard->GetCardInDeckAt(deckIndex)->GetAttribute(Card::BOTTOM);
  jsonObject.AddMember("card_attribute_bottom", rapidjson::Value(attributeBottom), jsonObject.GetAllocator());
  
  int attributeLeft = gameBoard->GetCardInDeckAt(deckIndex)->GetAttribute(Card::LEFT);
  jsonObject.AddMember("card_attribute_left", rapidjson::Value(attributeLeft), jsonObject.GetAllocator());
}

void GameController::SendMessageToHost(std::string &message) {
  if(EngineSingleton::GetInstance()->HostParticipant().IsConnectedToRoom()){
    ServiceLocator::GooglePlayService()->SendMessageToParticipant(message, EngineSingleton::GetInstance()->HostParticipant());
  }
}

void GameController::Update() {
  
  // Update the selected cards location to follow the players touch input
  if (selectedCard != nullptr && selectedCardIndex != -1 && GetGameState() == GameState::GAME_IN_PROGRESS) {
    selectedCard->setPosition(ControllerLocator::GetInputController()->GetTouchLocation());
  }
  
  // Syncing game sessions
  if (GetGameState() == GameState::GAME_SESSION_SYNC && EngineSingleton::GetInstance()->IsHost()) {
    SyncEndGameWithParticipants();
  }
}

void GameController::BroadcastUpdatedGameModel() {
  CCLOG("Broadcasting game model update to clients");
  std::string jsonGameModelUpdate = GetSerializedGameState(GameEvent::HOST_SYNC_GAME_MODEL);
  ServiceLocator::GooglePlayService()->SendMessageToParticipants(jsonGameModelUpdate);
}

GameBoard::Cell *GameController::GetCellAt(int index) {
  return gameBoard->GetCellAt(index);
}

void GameController::ProcessIncomingJsonObject(rapidjson::Document &jsonObject) {
  if (jsonObject.HasMember("event")) {
    GameEvent::EventType eventType = (GameEvent::EventType) jsonObject["event"].GetInt();
    
    // CLIENT -> HOST
    if (EngineSingleton::GetInstance()->IsHost()) {
      switch (eventType) {
        case GameEvent::CLIENT_PLAYER_QUIT_GAME:
          clientPlayerQuit = true;
          FinaliseGameSession();
          BroadcastUpdatedGameModel();
          return;
        case GameEvent::CLIENT_PLAYER_READY_TO_START:
          ConfirmPlayerReadyToStart(jsonObject);
          return;
        case GameEvent::TOUCH_INPUT_GAME_BOARD_END:
          UpdateGameModelTouchInput(jsonObject);
          return;
        default:
          break;
      }
    }
    
    // HOST -> CLIENT
    if(GetGameState() != GameState::GAME_SESSION_END) {
      switch (eventType) {
        case GameEvent::HOST_GAME_START:
          StartGameSetup();
          return;
        case GameEvent::HOST_SYNC_GAME_MODEL:
          SyncGameModelWithHost(jsonObject);
          return;
        default:
          break;
      }
    
    
      // CONFLICT RESOLUTION
      switch (eventType) {
        case GameEvent::END_GAME_RESOLVE_CONFLICTS:
          if (EngineSingleton::GetInstance()->IsHost()) {
            ResolvePendingConflicts(jsonObject);
          } else {
            SyncGameModelWithHost(jsonObject);
            ReplyToHostConflictResolution();
          }
          return;
        default:
          break;
      }
    }
    return;
    CCLOG("GameController::ProcessIncomingJsonObject Invalid event type");
  } else {
    CCLOG("GameController::ProcessGameModelUpdate() Error no event member");
  }
}

void GameController::ConfirmPlayerReadyToStart(rapidjson::Document &jsonObject) {
  CCLOG("Player confirmed ready to start game");
  numberOfPlayersReady++;
  
  if (numberOfPlayersReady >= 2) {
    SendGameStartNotificationToClients();
    StartGameSetup();
  }
}

void GameController::SendGameStartNotificationToClients() {
  rapidjson::Document jsonObject = JsonParser::CreateEmptyJsonDocument();
  AddJsonObjectHeader(jsonObject);
  jsonObject.AddMember("event", rapidjson::Value(GameEvent::HOST_GAME_START), jsonObject.GetAllocator());
  std::string jsonObjectString = JsonParser::JsonToString(jsonObject);
  ServiceLocator::GooglePlayService()->SendMessageToParticipants(jsonObjectString);
}

void GameController::SendQuitGameNotificationToHost() {
  rapidjson::Document jsonObject = JsonParser::CreateEmptyJsonDocument();
  AddJsonObjectHeader(jsonObject);
  jsonObject.AddMember("event", rapidjson::Value(GameEvent::CLIENT_PLAYER_QUIT_GAME), jsonObject.GetAllocator());
  std::string message = JsonParser::JsonToString(jsonObject);
  SendMessageToHost(message);
}

void GameController::ResolvePendingConflicts(rapidjson::Document &jsonObject) {
  bool conflictsResolved = CompareParticipantGameModel(jsonObject);
  if (conflictsResolved) {
    CCLOG("\n\nALL CONFLICTS RESOLVED\n\n");
    FinaliseGameSession();
    BroadcastUpdatedGameModel();
  }
}

void GameController::FinaliseGameSession() {
  CalculateWinningColor();
  std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::GAME_SESSION_END);
  EngineSingleton::GetInstance()->Notify(gameEvent.get());
}

void GameController::LocalPlayerQuitGame() {
  localPlayerQuit = true;
  if (EngineSingleton::GetInstance()->IsHost()) {
    // Host has quit the game, end the session
    FinaliseGameSession();
    BroadcastUpdatedGameModel();
  } else {
    // Client has quit the game, send a quit notification, end the session
    SendQuitGameNotificationToHost();
    FinaliseGameSession();
  }
}

bool GameController::HasLocalPlayerWon() {
  if (localPlayerQuit) {
    return false;
  } else {
    if (winningCardColor == gameBoard->GetLocalPlayer()->Color()) {
      return true;
    } else {
      return false;
    }
  }
}

bool GameController::CompareParticipantGameModel(rapidjson::Document &jsonObject) {
  CCLOG("\n\nCOMPARING GAME MODEL FROM CLIENT TO HOST\n\n");
  assert(jsonObject.HasMember("board"));
  for (int i = 0; i < (kMaxBoardRows * kMaxBoardColumns); i++) {
    int cellIndex = jsonObject["board"][i]["cell"].GetInt();
    std::string opponentCardName = jsonObject["board"][i]["card_name"].GetString();
    CardColor opponentCardColor = (CardColor) jsonObject["board"][i]["card_color"].GetInt();
    Card *currentCard = gameBoard->GetCellAt(cellIndex)->m_pCard.get();
    std::string hostCardName = currentCard->GetCardName();
    CardColor hostCardColor = currentCard->GetCardColor();
    if (opponentCardName != hostCardName && opponentCardColor != hostCardColor) {
      return false;
    }
  }
  return true;
}

void GameController::ReplyToHostConflictResolution() {
  std::string message = GetSerializedGameState(GameEvent::END_GAME_RESOLVE_CONFLICTS);
  SendMessageToHost(message);
}

void GameController::UpdateGameModelTouchInput(rapidjson::Document &jsonObject) {
  if (!jsonObject.HasMember("cell_index")) {
    return;
  }
  
  int cellIndex = jsonObject["cell_index"].GetInt();
  std::string cardOwner = jsonObject["username"].GetString();
  std::string cardName = jsonObject["card_name"].GetString();
  CardColor cardColor = (CardColor) jsonObject["card_color"].GetInt();
  unsigned long long eventTimestampDelta = jsonObject["timestamp_delta"].GetUint64();
  
  // If the cell is not occupied update the board
  if (!gameBoard->GetCellAt(cellIndex)->IsOccupied) {
    gameBoard->GetCellAt(cellIndex)->m_pCard.reset();
    Card *catalogueCard = ControllerLocator::GetConfigController()->GetCardInCardCatalogueByName(cardName);
    Card::CardData data = {cardOwner, catalogueCard->GetCardName(), catalogueCard->GetFilename(), cardColor, catalogueCard->GetAttribute(Card::Attribute::TOP), catalogueCard->GetAttribute(Card::Attribute::RIGHT), catalogueCard->GetAttribute(Card::Attribute::BOTTOM), catalogueCard->GetAttribute(Card::Attribute::LEFT), 1, catalogueCard->Level()};
    std::shared_ptr<Card> card = CardFactory::CreateCard(data, eventTimestampDelta);
    gameBoard->PlaceCardOnBoard(card, gameBoard->GetCellAt(cellIndex));
    
    // If this is a local message, remove the card from the players hand
    if (jsonObject["username"].GetString() == googlePlayServicesUsername) {
      // Disable card in deck after placing it
      gameBoard->GetCardInDeckAt(selectedCardIndex)->SetEnabled(false);
      selectedCardIndex = -1;
      selectedCard = nullptr;
    }
  } else {
    if (EngineSingleton::GetInstance()->IsHost()) {
      CCLOG("------CONFLICT-----");
      
      // Host has recieved an incoming message that specifies
      // an already occupied cell. This can only happen when both host
      // and opponent place a card in the same cell at the same time.
      
      // Resolution involves:
      // Flip a coin
      // Place the winning card
      int result = EngineSingleton::GetInstance()->FlipCoin();
      
      if (result) {
        Card *currentCard = gameBoard->GetCellAt(cellIndex)->m_pCard.get();
        
        if (currentCard->GetCardColor() == gameBoard->GetLocalPlayer()->Color()) {
          // Put the card that was placed on the board, back in the players deck
          std::string cardName = currentCard->GetCardName();
          gameBoard->GetCardInDeckByName(cardName)->SetEnabled(true);
          gameBoard->GetCardInDeckByName(cardName)->ResetPositionToDeck();
        }
        
        gameBoard->GetCellAt(cellIndex)->m_pCard.reset();
        Card *catalogueCard = ControllerLocator::GetConfigController()->GetCardInCardCatalogueByName(cardName);
        Card::CardData data = {cardOwner, catalogueCard->GetCardName(), catalogueCard->GetFilename(), cardColor, catalogueCard->GetAttribute(Card::Attribute::TOP), catalogueCard->GetAttribute(Card::Attribute::RIGHT), catalogueCard->GetAttribute(Card::Attribute::BOTTOM), catalogueCard->GetAttribute(Card::Attribute::LEFT), 1, catalogueCard->Level()};
        std::shared_ptr<Card> card = CardFactory::CreateCard(data, eventTimestampDelta);
        gameBoard->PlaceCardOnBoard(card, gameBoard->GetCellAt(cellIndex));
      } else {
        // model is up to date
      }
      CCLOG("------CONFLICT END-----");
    }
  }
  
  RunCardBattleSimulationForCardAt(cellIndex);
  
  if(gameBoard->IsFull()) {
    StopGameClockTimer();
  }
  
  if (EngineSingleton::GetInstance()->IsHost()) {
    
    // Broadcast the updated game model to all participants
    BroadcastUpdatedGameModel();
    
    // If the game board is full initiate the game session sync
    if (gameBoard->IsFull()) {
      CCLOG("Game board is full.");
      SyncGameSession();
    }
  }
}

void GameController::RunCardBattleSimulationForCardAt(int cellIndex) {
  CCLOG("\n\nCard battle simulation running...\n\n");
  int cardRow = (int) (cellIndex / kMaxBoardRows);
  int cardCol = cellIndex % kMaxBoardRows;
  
  int top = cardRow + 1;
  int right = cardCol + 1;
  int bottom = cardRow - 1;
  int left = cardCol - 1;
  
  // This cards TOP against opponents BOTTOM
  if (CheckIndexLimits(top)) {
    Card *attackingCard = gameBoard->GetCardInBoardAt(cardRow, cardCol);
    Card *defendingCard = gameBoard->GetCardInBoardAt(top, cardCol);
    int attackingCardValue = attackingCard->GetAttribute(Card::TOP);
    int defendingCardValue = defendingCard->GetAttribute(Card::BOTTOM);
    CardColor attackingCardColor = attackingCard->GetCardColor();
    CardColor defendingCardColor = defendingCard->GetCardColor();
    
    if (attackingCardColor != defendingCardColor && defendingCardColor != CardColor::CARD_COLOR_BLANK && attackingCardValue > defendingCardValue) {
      if (CheckCardTimestampDifferenceWithinLimits(attackingCard, defendingCard)) {
        gameBoard->FlipCard(top, cardCol);
      }
    }
  }
  
  // This cards RIGHT against opponent cards LEFT
  if (CheckIndexLimits(right)) {
    Card *attackingCard = gameBoard->GetCardInBoardAt(cardRow, cardCol);
    Card *defendingCard = gameBoard->GetCardInBoardAt(cardRow, right);
    int attackingCardValue = attackingCard->GetAttribute(Card::RIGHT);
    int defendingCardValue = defendingCard->GetAttribute(Card::LEFT);
    CardColor attackingCardColor = attackingCard->GetCardColor();
    CardColor defendingCardColor = defendingCard->GetCardColor();
    
    if (attackingCardColor != defendingCardColor && defendingCardColor != CardColor::CARD_COLOR_BLANK && attackingCardValue > defendingCardValue) {
      if (CheckCardTimestampDifferenceWithinLimits(attackingCard, defendingCard)) {
        gameBoard->FlipCard(cardRow, right);
      }
    }
  }
  
  // This cards BOTTOM against opponents TOP
  if (CheckIndexLimits(bottom)) {
    Card *attackingCard = gameBoard->GetCardInBoardAt(cardRow, cardCol);
    Card *defendingCard = gameBoard->GetCardInBoardAt(bottom, cardCol);
    int attackingCardValue = attackingCard->GetAttribute(Card::BOTTOM);
    int defendingCardValue = defendingCard->GetAttribute(Card::TOP);
    CardColor attackingCardColor = attackingCard->GetCardColor();
    CardColor defendingCardColor = defendingCard->GetCardColor();
    
    if (attackingCardColor != defendingCardColor && defendingCardColor != CardColor::CARD_COLOR_BLANK && attackingCardValue > defendingCardValue) {
      if (CheckCardTimestampDifferenceWithinLimits(attackingCard, defendingCard)) {
        gameBoard->FlipCard(bottom, cardCol);
      }
    }
  }
  
  // This cards LEFT against opponent cards RIGHT
  if (CheckIndexLimits(left)) {
    Card *attackingCard = gameBoard->GetCardInBoardAt(cardRow, cardCol);
    Card *defendingCard = gameBoard->GetCardInBoardAt(cardRow, left);
    int attackingCardValue = attackingCard->GetAttribute(Card::LEFT);
    int defendingCardValue = defendingCard->GetAttribute(Card::RIGHT);
    CardColor attackingCardColor = attackingCard->GetCardColor();
    CardColor defendingCardColor = defendingCard->GetCardColor();
    
    if (attackingCardColor != defendingCardColor && defendingCardColor != CardColor::CARD_COLOR_BLANK && attackingCardValue > defendingCardValue) {
      if (CheckCardTimestampDifferenceWithinLimits(attackingCard, defendingCard)) {
        gameBoard->FlipCard(cardRow, left);
      }
    }
  }
}

bool GameController::CheckIndexLimits(int index) {
  if (index < 0 || index >= kMaxBoardRows) {
    return false;
  } else {
    return true;
  }
}

bool GameController::CheckCardTimestampDifferenceWithinLimits(Card *attackingCard, Card *defendingCard) {
  int attackingCardTimestamp = (int) attackingCard->GetTimestamp();
  int defendingCardTimestamp = (int) defendingCard->GetTimestamp();
  int timestampDiff = attackingCardTimestamp - defendingCardTimestamp;
  timestampDiff = abs(timestampDiff);
  if (timestampDiff > kTimestampLimit) {
    return true;
  } else {
    return false;
  }
}

void GameController::SyncGameModelWithHost(rapidjson::Document &jsonObject) {
  // Process the json object data and
  // update the game model
  CCLOG("----------GameController::SyncGameModelWithHost() Participant JSON Game Model Update----------");
  CCLOG("Comparing current game model to Host update and applying Deltas...");
  
  assert(jsonObject.HasMember("board"));
  
  for (int i = 0; i < (kMaxBoardRows * kMaxBoardColumns); i++) {
    int cellIndex = jsonObject["board"][i]["cell"].GetInt();
    std::string hostCardOwner = jsonObject["board"][i]["username"].GetString();
    std::string hostCardName = jsonObject["board"][i]["card_name"].GetString();
    CardColor hostCardColor = (CardColor) jsonObject["board"][i]["card_color"].GetInt();
    unsigned long long eventTimestampDelta = jsonObject["board"][i]["timestamp_delta"].GetUint64();
    
    Card *currentCard = gameBoard->GetCellAt(cellIndex)->m_pCard.get();
    std::string currentCardName = currentCard->GetCardName();
    CardColor currentCardColor = currentCard->GetCardColor();
    
    if (hostCardColor == currentCardColor) {
      // No update needed
    } else if (hostCardColor == CardColor::CARD_COLOR_BLANK && currentCardColor != CardColor::CARD_COLOR_BLANK) {
      // The local player has placed a card before the next host update,
      // wait for a new host update
    } else if (hostCardColor != currentCardColor) {
      gameBoard->GetCellAt(cellIndex)->m_pCard.reset();
      Card *catalogueCard = ControllerLocator::GetConfigController()->GetCardInCardCatalogueByName(hostCardName);
      Card::CardData data = {hostCardOwner, catalogueCard->GetCardName(), catalogueCard->GetFilename(), hostCardColor, catalogueCard->GetAttribute(Card::Attribute::TOP), catalogueCard->GetAttribute(Card::Attribute::RIGHT), catalogueCard->GetAttribute(Card::Attribute::BOTTOM), catalogueCard->GetAttribute(Card::Attribute::LEFT), 1, catalogueCard->Level()};
      std::shared_ptr<Card> card = CardFactory::CreateCard(data, eventTimestampDelta);
      gameBoard->PlaceCardOnBoard(card, gameBoard->GetCellAt(cellIndex));
    }
  }
  
  // Once the update has been applied check the board and return all cards that
  // are not present to the players deck.
  // TODO improve this by storing all cards on the board in a map
  ReturnUnusedCardsToDeck();
  
  GameState gameState = (GameState)jsonObject["gamestate"].GetInt();
  SetGameState(gameState);
  
  // Update the winning color and end the game session
  if (gameState == GameState::GAME_SESSION_END) {
    assert(jsonObject.HasMember("winning_color"));
    winningCardColor = (CardColor) jsonObject["winning_color"].GetInt();
    EndGameSession();
  }
  
  CCLOG("----------GameController::SyncGameModelWithHost() Participant JSON Game Model Update End----------\n");
}

void GameController::EndGameSession() {
  if(!gameSessionEnded){
    auto resultUI = ResultUI::create();
    gameboardUI->addChild(resultUI, 10);
    gameSessionEnded = true;
    CCLOG("\n\nGAME SESSION IS OVER\n\n");
  }
}

void GameController::ReturnUnusedCardsToDeck() {
  for (int cardInDeckIndex = 0; cardInDeckIndex < kMaxNumberOfCardsInDeck; cardInDeckIndex++) {
    
    bool foundUsedCard = false;
    
    for (int cardOnBoardIndex = 0; cardOnBoardIndex < (kMaxBoardRows * kMaxBoardColumns); cardOnBoardIndex++) {
      if (gameBoard->GetCardInDeckAt(cardInDeckIndex)->GetCardName() == gameBoard->GetCardInBoardAt(cardOnBoardIndex)->GetCardName() && gameBoard->GetCardInDeckAt(cardInDeckIndex)->Owner() == gameBoard->GetCardInBoardAt(cardOnBoardIndex)->Owner()) {
        
        // Card in deck is on the board
        gameBoard->GetCardInDeckAt(cardInDeckIndex)->SetEnabled(false);
        foundUsedCard = true;
        break;
      }
    }
    
    if (!foundUsedCard) {
      gameBoard->GetCardInDeckAt(cardInDeckIndex)->SetEnabled(true);
      gameBoard->GetCardInDeckAt(cardInDeckIndex)->ResetPositionToDeck();
    }
  }
}

void GameController::SyncGameSession() {
  SetGameState(GameState::GAME_SESSION_SYNC);
}

void GameController::SyncEndGameWithParticipants() {
  conflictResolutionAttempts++;
  if (conflictResolutionAttempts < kMaxConflictResolutionAttempts) {
    std::string serializedGameModel = GetSerializedGameState(GameEvent::END_GAME_RESOLVE_CONFLICTS);
    ServiceLocator::GooglePlayService()->SendMessageToParticipants(serializedGameModel);
  } else {
    FinaliseGameSession();
    BroadcastUpdatedGameModel();
  }
}

std::string GameController::GetSerializedGameState(GameEvent::EventType gameEvent) {
  rapidjson::Document jsonDocument = JsonParser::CreateEmptyJsonDocument();
  jsonDocument.AddMember("event", rapidjson::Value(gameEvent), jsonDocument.GetAllocator());
  
  if (gameBoard) {
    
    // Game state
    jsonDocument.AddMember("gamestate", rapidjson::Value(GetGameState()), jsonDocument.GetAllocator());
    
    CardColor winningColor = winningCardColor;
    jsonDocument.AddMember("winning_color", rapidjson::Value(winningColor), jsonDocument.GetAllocator());
    
    // Create array
    rapidjson::Value boardCellGrid(rapidjson::kArrayType);
    
    for (int cellIndex = 0; cellIndex < (kMaxBoardRows * kMaxBoardColumns); cellIndex++) {
      // Create cell object
      rapidjson::Value cellObject(rapidjson::kObjectType);
      
      cellObject.AddMember("cell", rapidjson::Value(cellIndex), jsonDocument.GetAllocator());
      
      bool cellIsOccupied = gameBoard->IsOccupied(cellIndex);
      cellObject.AddMember("occupied", rapidjson::Value(cellIsOccupied), jsonDocument.GetAllocator());
      
      std::string cardOwner = gameBoard->GetCellAt(cellIndex)->m_pCard->Owner();
      rapidjson::Value cardOwnerValue(cardOwner.c_str(), jsonDocument.GetAllocator());
      cellObject.AddMember("username", cardOwnerValue, jsonDocument.GetAllocator());
      
      std::string cardName = gameBoard->GetCellAt(cellIndex)->m_pCard->GetCardName();
      rapidjson::Value cardNameValue(cardName.c_str(), jsonDocument.GetAllocator());
      cellObject.AddMember("card_name", cardNameValue, jsonDocument.GetAllocator());
      
      CardColor cardColor = gameBoard->GetCellAt(cellIndex)->m_pCard->GetCardColor();
      cellObject.AddMember("card_color", rapidjson::Value(cardColor), jsonDocument.GetAllocator());
      
      unsigned long long eventTimestampDelta = gameBoard->GetCellAt(cellIndex)->m_pCard->GetTimestamp();
      cellObject.AddMember("timestamp_delta", rapidjson::Value(eventTimestampDelta), jsonDocument.GetAllocator());
      
      // Add cell object to array
      boardCellGrid.PushBack(cellObject, jsonDocument.GetAllocator());
    }
    
    // Add array to json object
    jsonDocument.AddMember("board", boardCellGrid, jsonDocument.GetAllocator());
  }
  
  return JsonParser::JsonToString(jsonDocument);
}

void GameController::SetGameState(GameState gameState){
  state = gameState;
}

GameState GameController::GetGameState() {
  return state;
}

void GameController::StartGameSetup() {
  gameboardUI = GameBoardUI::createScene();
  cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.0f, gameboardUI, cocos2d::Color3B::BLACK));
  
  std::unique_ptr<GameBoard> gameBoardInstance(new GameBoard(gameboardUI, playerCardDeck));
  gameBoard = std::move(gameBoardInstance);
  
  EngineSingleton::GetInstance()->SetGameSessionStartTime();
  SetGameState(GameState::GAME_IN_PROGRESS);
}

void GameController::PlayerReadyToStart() {
  CCLOG("Sending notification to host. Player ready to start game.");
  // Send message to host telling it that this player is ready to start the game
  rapidjson::Document jsonObject = JsonParser::CreateEmptyJsonDocument();
  
  AddJsonObjectHeader(jsonObject);
  
  jsonObject.AddMember("event", rapidjson::Value(GameEvent::CLIENT_PLAYER_READY_TO_START), jsonObject.GetAllocator());
  
  if (EngineSingleton::GetInstance()->IsHost()) {
    ConfirmPlayerReadyToStart(jsonObject);
  } else {
    std::string message = JsonParser::JsonToString(jsonObject);
    SendMessageToHost(message);
  }
}

int GameController::CheckTouchInputOnCells(float touchInputX, float touchInputY) {
  // Check if the touch input is on the cell grid
  for (int index = 0; index < (kMaxBoardRows * kMaxBoardColumns); index++) {
    if (GetCellAt(index)->m_pCard != nullptr) {
      if (GetCellAt(index)->m_pCard->GetSprite()->getBoundingBox().containsPoint(cocos2d::Vec2(touchInputX, touchInputY))) {
        // If the player has clicked on an occupied cell
        // cancel the input command
        if (GetCellAt(index)->IsOccupied) {
          return -1;
        }
        
        // Touched cell not occupied
        return index;
      }
    }
  }
  
  return -1;
}

void GameController::SelectCardInDeck(int cardIndex) {
  if (GetCardInDeckAt(cardIndex)->IsSelected()) {
    GetCardInDeckAt(cardIndex)->SetSelected(false);
    selectedCardIndex = -1;
    selectedCard = nullptr;
  } else {
    // If another card is selected, deselect current card
    if (selectedCardIndex >= 0) {
      GetCardInDeckAt(selectedCardIndex)->SetSelected(false);
      selectedCardIndex = -1;
      selectedCard = nullptr;
    }
    
    // Select card
    GetCardInDeckAt(cardIndex)->SetSelected(true);
    selectedCardIndex = cardIndex;
    selectedCard = GetCardInDeckAt(cardIndex)->GetSprite();
  }
}

Card *GameController::GetCardInDeckAt(int cardIndex) {
  return gameBoard->GetCardInDeckAt(cardIndex);
}

void GameController::CalculateWinningColor() {
  // Check who quit the game and award the win to the other player
  if(localPlayerQuit){
    if(gameBoard->GetLocalPlayer()->Color() == CardColor::CARD_COLOR_BLUE) {
      winningCardColor = CardColor::CARD_COLOR_RED;
    } else {
      winningCardColor = CardColor::CARD_COLOR_BLUE;
    }
    return;
  } else if (clientPlayerQuit) {
    // Client quit the match, host wins
    if(gameBoard->GetLocalPlayer()->Color() == CardColor::CARD_COLOR_BLUE) {
      winningCardColor = CardColor::CARD_COLOR_BLUE;
    }
    return;
  }
  
  // Nobody quit the game, count the colors and award the win.
  int blue = 0;
  int red = 0;
  for (int index = 0; index < (kMaxBoardRows * kMaxBoardColumns); index++) {
    if (GetCellAt(index)->m_pCard->GetCardColor() == CardColor::CARD_COLOR_BLUE) {
      blue++;
    } else if (GetCellAt(index)->m_pCard->GetCardColor() == CARD_COLOR_RED) {
      red++;
    }
  }
  
  if (blue > red) {
    winningCardColor = CardColor::CARD_COLOR_BLUE;
  } else if (red > blue) {
    winningCardColor = CardColor::CARD_COLOR_RED;
  } else {
    // Its' a draw .... you can never have a draw in this game, something went wrong, probably.. :|
  }
}

void GameController::OnNotify(GameEvent *event){
  switch (event->Type()) {
    case GameEvent::GAME_SESSION_END:
      if(GetGameState() != GameState::GAME_SESSION_END){
        SetGameState(GameState::GAME_SESSION_END);
        EndGameSession();
      }
      break;
      
    default:
      break;
  }
}
