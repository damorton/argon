// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef CONTROLLER_GAME_CONTROLLER_H_
#define CONTROLLER_GAME_CONTROLLER_H_

#include "Game/GameBoard.h"
#include "external/json/document.h"

#include "../Service/GameService/GameServiceInterface.h"
#include "Engine/Utils/Observer.h"
#include "Game/CardCollection.h"
#include "Engine/Utils/GameEvent.h"

class EngineSingleton;
class Card;
class CardSelectUI;
class ConfigController;
class GameEvent;

enum GameState {
  GAME_INITIALISED,
  GAME_IN_PROGRESS,
  GAME_SESSION_SYNC,
  GAME_SESSION_END
};

class GameController : public Observer {
public:
  GameController();
  virtual ~GameController();
  void Update();
  void ProcessCardSelectTouchInput(float touchInputX, float touchInputY);
  void ProcessGameBoardTouchInputBegin(float touchInputX, float touchInputY);
  void ProcessGameBoardTouchInputEnd(float touchInputX, float touchInputY);
  void ProcessIncomingJsonObject(rapidjson::Document &jsonObject);
  void PlayerReadyToStart();
  void LocalPlayerQuitGame();
  bool HasLocalPlayerWon();
  void SetGameState(GameState gameState);
  GameState GetGameState();
  void ClockTimer();
  int GetGameTimeSeconds();
  bool IsGameTimerStopped();
  virtual void OnNotify(GameEvent *event) override;

private:
  void StartGameSetup();
  void FinaliseGameSession();
  void EndGameSession();
  void SyncGameSession();
  void StopGameClockTimer();
  void CalculateWinningColor();
  std::string GetSerializedGameState(GameEvent::EventType gameEvent);
  void UpdateGameModelTouchInput(rapidjson::Document &jsonObject);
  void SyncGameModelWithHost(rapidjson::Document &jsonObject);
  void AddJsonObjectHeader(rapidjson::Document &jsonObject);
  void UpdateJsonObjectCellIndex(int cellIndex, rapidjson::Document &jsonObject);
  void UpdateJsonObjectCardSelect(int deckIndex, rapidjson::Document &jsonObject);
  GameBoard::Cell *GetCellAt(int index);
  Card *GetCardInDeckAt(int cardIndex);
  void SendMessageToHost(std::string &message);
  int CheckTouchInputOnCells(float touchInputX, float touchInputY);
  void BroadcastUpdatedGameModel();
  void SelectCardInDeck(int cardIndex);
  void SyncEndGameWithParticipants();
  void ResolvePendingConflicts(rapidjson::Document &jsonObject);
  bool CompareParticipantGameModel(rapidjson::Document &jsonObject);
  void ReplyToHostConflictResolution();
  void ConfirmPlayerReadyToStart(rapidjson::Document &jsonObject);
  void SendGameStartNotificationToClients();
  void SendQuitGameNotificationToHost();
  void RunCardBattleSimulationForCardAt(int cellIndex);
  bool CheckIndexLimits(int index);
  bool CheckCardTimestampDifferenceWithinLimits(Card *attackingCard, Card *defendingCard);
  void ReturnUnusedCardsToDeck();

  std::unique_ptr<GameBoard> gameBoard;
  std::vector<Card::CardData> playerCardDeck;
  int selectedCardIndex;
  int numberOfPlayersReady;
  int conflictResolutionAttempts;
  int gameTimeSeconds;
  bool playerWon;
  bool localPlayerQuit;
  bool clientPlayerQuit;
  bool cardSelected;
  bool gameOver;
  bool stopClockTimer;
  std::string googlePlayServicesUsername;
  CardColor winningCardColor;
  cocos2d::Sprite *selectedCard;
  gpg::MultiplayerParticipant hostParticipant;
  CardCollectionInterface *cardCollection;
  GameState state;
  cocos2d::Scene *gameboardUI;
  bool gameSessionEnded;
};

#endif
