// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/Controller/ConfigController.h"
#include "Engine/Singleton/EngineSingleton.h"
#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#endif
#include "Game/Factory/CardFactory.h"
#include "Game/Factory/ChestFactory.h"
#include "GameDefines.h"
#include "UI/LoadingSpinnerUI.h"

const std::string kLoadingSpinnerName = "loadingSpinner";

ConfigController::ConfigController() {
}

ConfigController::~ConfigController() {
  cardCatalogue.clear();
  gameConfigurationMap.clear();
}

void ConfigController::LoadDefaultGameConfiguration() {
  std::string defaultGameConfigurationFromFile = cocos2d::FileUtils::getInstance()->fullPathForFilename(kGameConfigurationFile);
  rapidjson::Document defaultGameConfiguration = JsonParser::FileToJson(defaultGameConfigurationFromFile);
    
  // Set the username value in the game config array
  // Array = kGameConfigurationJsonObject
  // Username = index 0
  // Username value = index 1
  defaultGameConfiguration[kGameConfigurationJsonObject.c_str()][1][1].SetString(ServiceLocator::GooglePlayService()->GooglePlayUsername().c_str(), defaultGameConfiguration.GetAllocator());
  LoadGameConfiguration(defaultGameConfiguration);
}

void ConfigController::LoadGameConfiguration(rapidjson::Document &gameSaveData) {
  rapidjson::Document gameConfigJsonObject;
  gameConfigJsonObject.CopyFrom(gameSaveData, gameSaveData.GetAllocator());
  InitGameConfig(gameConfigJsonObject);
  InitCardCollection(gameConfigJsonObject);
  InitChestCollection(gameConfigJsonObject);
  
  // Refactor this to mediator
  EngineSingleton::GetInstance()->CreateGameStore();
 
  std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::EVENT_GAME_MODEL_UPDATED);
  EngineSingleton::GetInstance()->Notify(gameEvent.get());
  
  gameEvent = GameEventFactory::Create(GameEvent::EVENT_GAME_CONFIG_LOADED);
  EngineSingleton::GetInstance()->Notify(gameEvent.get());
}

void ConfigController::InitGameConfig(rapidjson::Document &gameConfigJsonObject) {
  assert(gameConfigJsonObject.HasMember(kGameConfigurationJsonObject.c_str()));
  rapidjson::Value &configArray = gameConfigJsonObject[kGameConfigurationJsonObject.c_str()];
  assert(configArray.IsArray());
  
  const int kKey = 0;
  const int kValue = 1;
  
  for(int arrayIndex = 0; arrayIndex < configArray.Size(); arrayIndex++) {
    std::string key = configArray[arrayIndex][kKey].GetString();
    std::string value = configArray[arrayIndex][kValue].GetString();
    gameConfigurationMap[key] = value;
  }
}

void ConfigController::InitChestCollection(rapidjson::Document &gameConfigJsonObject) {
  assert(gameConfigJsonObject.HasMember(kChestDataJsonObject.c_str()));
  rapidjson::Value &chestContainerArray = gameConfigJsonObject[kChestDataJsonObject.c_str()];
  assert(chestContainerArray.IsArray());
  
  for(int chest = 0; chest < chestContainerArray.Size(); chest++) {
    Chest::ChestType chestType = (Chest::ChestType)chestContainerArray[chest]["chest_type"].GetInt();
    unsigned long long timeCreated = chestContainerArray[chest]["time_created"].GetUint64();
    unsigned long long timeUnlocked = chestContainerArray[chest]["time_unlocked"].GetUint64();
    Chest::State chestState = (Chest::State)chestContainerArray[chest]["chest_state"].GetInt();
    
    std::shared_ptr<Chest> chestTemp = ChestFactory::Create(chestType, timeCreated, timeUnlocked, chestState);
    EngineSingleton::GetInstance()->AddChest(chestTemp);
  }
}

void ConfigController::InitCardCollection(rapidjson::Document &gameConfigJsonObject) {
  assert(gameConfigJsonObject.HasMember("card_collection"));
  rapidjson::Value &cardCollectionArray = gameConfigJsonObject["card_collection"];
  assert(cardCollectionArray.IsArray());
  
  for (rapidjson::SizeType cardIndex = 0; cardIndex < cardCollectionArray.Size(); cardIndex++) {
    std::string cardOwner = ServiceLocator::GooglePlayService()->GooglePlayUsername();
    std::string cardName = cardCollectionArray[cardIndex]["card_name"].GetString();
    Card::CardLevel cardLevel = (Card::CardLevel)cardCollectionArray[cardIndex]["card_level"].GetInt();
    std::string filename = cardCollectionArray[cardIndex]["card_filename"].GetString();
    int attributeTop = cardCollectionArray[cardIndex]["card_attribute_top"].GetInt();
    int attributeRight = cardCollectionArray[cardIndex]["card_attribute_right"].GetInt();
    int attributeBottom = cardCollectionArray[cardIndex]["card_attribute_bottom"].GetInt();
    int attributeLeft = cardCollectionArray[cardIndex]["card_attribute_left"].GetInt();
    int cardQuantity = cardCollectionArray[cardIndex]["quantity"].GetInt();
    Card::CardData cardData = {
      cardOwner,
      cardName,
      filename,
      CardColor::CARD_COLOR_NEUTRAL,
      attributeTop,
      attributeRight,
      attributeBottom,
      attributeLeft,
      cardQuantity,
      cardLevel};
    std::shared_ptr<Card> card = CardFactory::CreateCard(cardData);
    cardCatalogue.push_back(card);
    bool unlocked = cardCollectionArray[cardIndex]["unlocked"].GetBool();
    if (unlocked) {
      card->Unlock();
      EngineSingleton::GetInstance()->GetCardCollection()->AddCard(card);
    }
  }
}

void ConfigController::VaildateGameSaveData(rapidjson::Document &gameSaveData) {  
  std::string defaultGameConfigurationFromFile = cocos2d::FileUtils::getInstance()->fullPathForFilename(kGameConfigurationFile);
  rapidjson::Document defaultGameConfiguration = JsonParser::FileToJson(defaultGameConfigurationFromFile);
  if(ConfigSerialNumbersEqual(gameSaveData, defaultGameConfiguration)){
    LoadGameConfiguration(gameSaveData);
  } else {
    // TODO start game data migration from old save to new save
    ControllerLocator::GetDaoController()->DeleteGameSave();
  }
}

bool ConfigController::ConfigSerialNumbersEqual(rapidjson::Document &cloudSaveData, rapidjson::Document &defaultGameConfiguration){
  assert(cloudSaveData.HasMember(kGameConfigurationJsonObject.c_str()));
  assert(defaultGameConfiguration.HasMember(kGameConfigurationJsonObject.c_str()));
  
  rapidjson::Value &cloudSaveConfigArray = cloudSaveData[kGameConfigurationJsonObject.c_str()];
  assert(cloudSaveConfigArray.IsArray());
  
  rapidjson::Value &defaultGameSaveConfigArray = defaultGameConfiguration[kGameConfigurationJsonObject.c_str()];
  assert(defaultGameSaveConfigArray.IsArray());
  
  if(cloudSaveConfigArray.Size() != defaultGameSaveConfigArray.Size()) {
    return false;
  }
  
  const int kKey = 0;
  const int kValue = 1;
  std::string cloudSaveConfigSerialNumber = "0";
  std::string defaultGameConfigurationSerialNumber = "0";
  std::string key;
  std::string value;
  for(int arrayIndex = 0; arrayIndex < cloudSaveConfigArray.Size(); arrayIndex++) {
    
    // Cloud save
    key = cloudSaveConfigArray[arrayIndex][kKey].GetString();
    value = cloudSaveConfigArray[arrayIndex][kValue].GetString();
    if(key == kGameConfigSerialNumberKey) {
      cloudSaveConfigSerialNumber = value;
    }
    
    // Default config
    key = defaultGameSaveConfigArray[arrayIndex][kKey].GetString();
    value = defaultGameSaveConfigArray[arrayIndex][kValue].GetString();
    if(key == kGameConfigSerialNumberKey) {
      defaultGameConfigurationSerialNumber = value;
    }
  }
  
  if(cloudSaveConfigSerialNumber == defaultGameConfigurationSerialNumber) {
    return true;
  } else {
    return false;
  }
}

std::vector<std::shared_ptr<Card>> *ConfigController::GetCardCatalogue() {
  return &cardCatalogue;
}

Card *ConfigController::GetCardInCardCatalogueByName(std::string &cardName) {
  for(auto card : cardCatalogue){
    if(card->GetCardName() == cardName) {
      return card.get();
    }
  }
  return nullptr;
}


void ConfigController::SaveGameConfiguration() {
  CCLOG("Saving game configuration...");
  std::string defaultGameConfigurationFromFile = cocos2d::FileUtils::getInstance()->fullPathForFilename(kGameConfigurationFile);
  rapidjson::Document gameConfiguration = JsonParser::FileToJson(defaultGameConfigurationFromFile);
  gameConfiguration[kGameConfigurationJsonObject.c_str()][1][1].SetString(ServiceLocator::GooglePlayService()->GooglePlayUsername().c_str(), gameConfiguration.GetAllocator());
  
  UpdateJsonObjectWithGameConfig(gameConfiguration);
  UpdateJsonObjectWithUnlockedCards(gameConfiguration);
  UpdateJsonObjectWithChests(gameConfiguration);
  ControllerLocator::GetDaoController()->SaveGame(gameConfiguration);
}

void ConfigController::UpdateJsonObjectWithGameConfig(rapidjson::Document &gameConfigJsonObject) {
  const int kKey = 0;
  const int kValue = 1;
  assert(gameConfigJsonObject.HasMember(kGameConfigurationJsonObject.c_str()));
  rapidjson::Value &configArray = gameConfigJsonObject[kGameConfigurationJsonObject.c_str()];
  assert(configArray.IsArray());
  for(int arrayIndex = 0; arrayIndex < configArray.Size(); arrayIndex++) {
    std::string value = gameConfigurationMap[configArray[arrayIndex][kKey].GetString()];
    configArray[arrayIndex][kValue].SetString(value.c_str(), gameConfigJsonObject.GetAllocator());
  }
}

void ConfigController::UpdateJsonObjectWithUnlockedCards(rapidjson::Document &gameConfigJsonObject) {
  assert(gameConfigJsonObject.HasMember("card_collection"));
  rapidjson::Value &cardCollectionArray = gameConfigJsonObject["card_collection"];
  assert(cardCollectionArray.IsArray());
  
  for (rapidjson::SizeType cardIndex = 0; cardIndex < cardCollectionArray.Size(); cardIndex++) {
    std::string cardInJsonObjectName = cardCollectionArray[cardIndex]["card_name"].GetString();
    for(auto card : *EngineSingleton::GetInstance()->GetCardCollection()->UnlockedCards()) {
      std::string unlockedCardName = card->GetCardName();
      if(unlockedCardName == cardInJsonObjectName) {
        bool unlocked = cardCollectionArray[cardIndex]["unlocked"].GetBool();
        if(!unlocked) {
          cardCollectionArray[cardIndex]["unlocked"].SetBool(true);
        }
      }
    }
  }
}

void ConfigController::UpdateJsonObjectWithChests(rapidjson::Document &gameConfigJsonObject) {
  assert(gameConfigJsonObject.HasMember(kChestDataJsonObject.c_str()));
  gameConfigJsonObject.RemoveMember(kChestDataJsonObject.c_str());
  rapidjson::Value chestContainerArray(rapidjson::kArrayType);
  for(auto chest : *EngineSingleton::GetInstance()->Chests()) {
    rapidjson::Value chestObject(rapidjson::kObjectType);
    chestObject.AddMember("chest_type", rapidjson::Value(chest->GetType()), gameConfigJsonObject.GetAllocator());
    chestObject.AddMember("time_created", rapidjson::Value(chest->GetTimeStamp()), gameConfigJsonObject.GetAllocator());
    chestObject.AddMember("time_unlocked", rapidjson::Value(chest->TimeUnlocked()), gameConfigJsonObject.GetAllocator());
    chestObject.AddMember("chest_state", rapidjson::Value(chest->GetState()), gameConfigJsonObject.GetAllocator());
    chestContainerArray.PushBack(chestObject, gameConfigJsonObject.GetAllocator());
  }
  gameConfigJsonObject.AddMember("chests", chestContainerArray, gameConfigJsonObject.GetAllocator());
}

std::string ConfigController::ConfigurationValue(std::string key) {
  if(gameConfigurationMap.find(key) != gameConfigurationMap.end()) {
    return gameConfigurationMap[key];
  } else {
    return "";
  }
}

void ConfigController::StoreConfigSetting(std::string key, std::string value) {
  gameConfigurationMap[key] = value;
}

void ConfigController::ApplyRewardsToPlayerData(Chest *rewardChest) {
  UpdatePlayerData(rewardChest);
}

void ConfigController::UpdatePlayerData(Chest *rewardChest) {
  EngineSingleton::GetInstance()->GetCardCollection()->AddCards(rewardChest->Cards());
  UpdatePlayerGold(rewardChest->Gold());
  UpdatePlayerGems(rewardChest->Gems());
  UpdatePlayerStars(rewardChest->Stars());
  
  sdkbox::PluginGoogleAnalytics::logEvent("Chest", "Opened", "Cards in chest", static_cast<int>(rewardChest->Cards()->size()));
}

void ConfigController::UpdatePlayerGold(int gold) {
  std::string currentGoldStr = ConfigurationValue(kGold);
  int currentGold = Utils::to_int(currentGoldStr);
  currentGold += gold;
  StoreConfigSetting(kGold, Utils::to_str(currentGold));
}

void ConfigController::UpdatePlayerGems(int gems) {
  std::string currentGemsStr = ConfigurationValue(kGems);
  int currentGems = Utils::to_int(currentGemsStr);
  currentGems += gems;
  StoreConfigSetting(kGems, Utils::to_str(currentGems));
}

void ConfigController::UpdatePlayerStars(int stars) {
  std::string currentStarsStr = ConfigurationValue(kStars);
  int currentStars = Utils::to_int(currentStarsStr);
  currentStars += stars;
  StoreConfigSetting(kStars, Utils::to_str(currentStars));
  sdkbox::PluginGoogleAnalytics::logEvent("Chest", "Opened", "Stars awarded", stars);
}

bool ConfigController::CheckGemAmount(int gems) {
  std::string currentGemsStr = ConfigurationValue(kGems);
  int currentGems = Utils::to_int(currentGemsStr);
  if(gems < currentGems) {
    return true;
  } else {
    return false;
  }
}

bool ConfigController::CheckGoldAmount(int gold) {
  std::string currentGoldStr = ConfigurationValue(kGold);
  int currentGold = Utils::to_int(currentGoldStr);
  if(gold < currentGold) {
    return true;
  } else {
    return false;
  }
}

void ConfigController::OnNotify(GameEvent *event){
  if(event->Type() == GameEvent::EVENT_SAVE_GAME_CONFIG){
    SaveGameConfiguration();
    DisplayLoadingSpinner();
  } else if(event->Type() == GameEvent::EVENT_SAVE_GAME_COMPLETE){
    RemoveLoadingSpinner();
  }
}

void ConfigController::DisplayLoadingSpinner(){
  auto loadingSpinner = LoadingSpinnerUI::create();
  loadingSpinner->setName(kLoadingSpinnerName);
  cocos2d::Director::getInstance()->getRunningScene()->addChild(loadingSpinner, 10);
}

void ConfigController::RemoveLoadingSpinner(){
  cocos2d::Director::getInstance()->getRunningScene()->removeChildByName(kLoadingSpinnerName);
}
