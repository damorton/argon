// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef CONTROLLER_CONFIG_CONTROLLER_H_
#define CONTROLLER_CONFIG_CONTROLLER_H_

#include "Engine/JsonParser/JsonParser.h"
#include <unordered_map>
#include <vector>
#include <memory>
#include "Engine/Utils/Observer.h"

class Chest;
class Card;

class ConfigController : public Observer {
public:
  ConfigController();
  virtual ~ConfigController();
  std::string ConfigurationValue(std::string key);
  void StoreConfigSetting(std::string key, std::string value);
  void LoadGameConfiguration(rapidjson::Document &gameSaveData);
  void LoadDefaultGameConfiguration();
  void VaildateGameSaveData(rapidjson::Document &gameSaveData);
  virtual void OnNotify(GameEvent *event) override;
  
  // TODO create class to handle card speific functions
  std::vector<std::shared_ptr<Card>> *GetCardCatalogue();
  Card *GetCardInCardCatalogueByName(std::string &cardName);
  
  // TODO create class to handle player rewards
  void ApplyRewardsToPlayerData(Chest *rewardChest);
  void UpdatePlayerGold(int gold);
  void UpdatePlayerGems(int gems);
  void UpdatePlayerStars(int stars);
  bool CheckGemAmount(int gems);
  bool CheckGoldAmount(int gold);

private:
  void DisplayLoadingSpinner();
  void RemoveLoadingSpinner();
  void SaveGameConfiguration();
  void InitGameConfig(rapidjson::Document &gameConfigJsonObject);
  void InitChestCollection(rapidjson::Document &gameConfigJsonObject);
  void InitCardCollection(rapidjson::Document &gameConfigJsonObject);
  void UpdateJsonObjectWithGameConfig(rapidjson::Document &gameConfigJsonObject);
  void UpdateJsonObjectWithUnlockedCards(rapidjson::Document &gameConfigJsonObject);
  void UpdateJsonObjectWithChests(rapidjson::Document &gameConfigJsonObject);
  void UpdatePlayerData(Chest *rewardChest);
  bool ConfigSerialNumbersEqual(rapidjson::Document &cloudSaveData, rapidjson::Document &defaultGameConfiguration);
  
  std::vector<std::shared_ptr<Card>> cardCatalogue;
  std::unordered_map<std::string, std::string> gameConfigurationMap;
};

#endif
