// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef CONTROLLER_DAO_CONTROLLER_H_
#define CONTROLLER_DAO_CONTROLLER_H_

#ifdef SDKBOX_ENABLED
#include "PluginGPG/PluginGPG.h"
#endif

#include "Engine/Utils/Observer.h"
#include "Engine/JsonParser/JsonParser.h"

class DaoController : public Observer {
public:
  DaoController();
  void ListGameSaves();
  void DeleteGameSave();
  void LoadGame(const std::string& filename);
  void SaveGame(rapidjson::Document &gameSaveData);
  void SaveToLocalFile(rapidjson::Document &gameSaveData);
  virtual void OnNotify(GameEvent *event) override;

private:
  bool gameConfigLoadedFromGooglePlay;
};
#endif
