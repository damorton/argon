// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef CONTROLLER_INPUT_CONTROLLER_H_
#define CONTROLLER_INPUT_CONTROLLER_H_

#include "cocos2d.h"
#include "external/json/document.h"
#include "Engine/Utils/Observer.h"

class GameEvent;

class InputController : public Observer {
public:
  void ProcessInputEvent(GameEvent *event);
  void SetTouchLocation(float x, float y);
  cocos2d::Vec2 GetTouchLocation();
  virtual void OnNotify(GameEvent *event) override;

private:
  int touchLocationX;
  int touchLocationY;
};
#endif 
