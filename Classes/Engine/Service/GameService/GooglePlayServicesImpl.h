// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ENGINE_GOOGLE_PLAY_SERVICES_GOOGLE_PLAY_SERVICES_IMPL_H_
#define ENGINE_GOOGLE_PLAY_SERVICES_GOOGLE_PLAY_SERVICES_IMPL_H_

#include "../GameService/GameServiceInterface.h"
#include "Engine/JsonParser/JsonParser.h"

class GooglePlayServicesImpl : public GameServiceInterface, public gpg::IRealTimeEventListener {
public:
  GooglePlayServicesImpl();
protected:
  virtual bool IsAuthorized() override;
  virtual void ListGameSaves() override;
  virtual void DeleteGameSave() override;
  virtual void LoadGame(const std::string &filename) override;
  virtual void SaveGame(const std::string& filename, const std::string& description, const std::string& content, gpg::SnapshotConflictPolicy conflict_policy) override;
  virtual std::string GooglePlayUsername() override;
  virtual void SendMessageToParticipants(std::string &message) override;
  virtual void SendMessageToParticipant(std::string &message, gpg::MultiplayerParticipant participant) override;
  virtual gpg::RealTimeRoom *Gameroom() override;
  virtual void CreateRealtimeGameroom(const int minimumPlayersToStart, const int maxParticipants) override;
  virtual void LeaveRealtimeGameroom() override;
  virtual void PlayerStats() override;
  virtual bool IsSignedIn() override;
  virtual bool IsGameRoomValid() override;
  virtual bool HasReceievedData() override;
  virtual std::string Buffer() override;
  virtual std::vector<gpg::MultiplayerParticipant> GameRoomParticipants() override;

private:
  // Initialise Google Play Services and load game configuration data from Google servers
  void InitGpg();
  void OnConnect();
  void SetRoom(gpg::RealTimeRoom const &room);
  void ProcessIncomingData();
  
  //IRealTimeEventListener
  void OnRoomStatusChanged(gpg::RealTimeRoom const &room) override;
  void OnConnectedSetChanged(gpg::RealTimeRoom const &room) override;
  void OnP2PConnected(gpg::RealTimeRoom const &room, gpg::MultiplayerParticipant const &participant) override;
  void OnP2PDisconnected(gpg::RealTimeRoom const &room, gpg::MultiplayerParticipant const &participant) override;
  void OnParticipantStatusChanged(gpg::RealTimeRoom const &room, gpg::MultiplayerParticipant const &participant) override;
  void OnDataReceived(gpg::RealTimeRoom const &room, gpg::MultiplayerParticipant const &from_participant, std::vector<uint8_t> data, bool is_reliable)
      override;

  gpg::GameServices *gameServices;
  gpg::RealTimeRoom realTimeRoom;
  std::string buffer;
  bool dataReceived;
  int maxParticipantsInGameroom;
  rapidjson::Document gameConfigurationJsonObject;
};
#endif
