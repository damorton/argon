// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "../GameService/GooglePlayServicesImpl.h"

#include "../GameService/StateManager.h"
#include "Engine/Singleton/EngineSingleton.h"

using namespace std;

GooglePlayServicesImpl::GooglePlayServicesImpl() {
  gameConfigurationJsonObject = JsonParser::CreateEmptyJsonDocument();
  maxParticipantsInGameroom = 0;
  dataReceived = false;
  InitGpg();
}

//
// IRealtimeEventListener
//
void GooglePlayServicesImpl::OnRoomStatusChanged(gpg::RealTimeRoom const &room) {
  printf("Room Status Change %d\n", room.Status());
}

void GooglePlayServicesImpl::OnConnectedSetChanged(gpg::RealTimeRoom const &room) {
  printf("Connection Change %d\n", room.Status());
}

void GooglePlayServicesImpl::OnP2PConnected(gpg::RealTimeRoom const &room, gpg::MultiplayerParticipant const &participant) {
  printf("P2P Connected %s : %s\n", room.Id().c_str(), participant.DisplayName().c_str());
}

void GooglePlayServicesImpl::OnP2PDisconnected(gpg::RealTimeRoom const &room, gpg::MultiplayerParticipant const &participant) {
  printf("P2P Disconnected %s : %s\n", room.Id().c_str(), participant.DisplayName().c_str());
}

void GooglePlayServicesImpl::OnParticipantStatusChanged(gpg::RealTimeRoom const &room, gpg::MultiplayerParticipant const &participant) {
  printf("Participant Status Change %s : %s\n", room.Id().c_str(), participant.DisplayName().c_str());
  if(participant.DisplayName() == GooglePlayUsername()) {
    if(participant.Status() == gpg::ParticipantStatus::LEFT) {
      // This player has disconnected from the game
    }
  } else {
    // Opponent has left
  }
  
}

void GooglePlayServicesImpl::OnDataReceived(gpg::RealTimeRoom const &room, gpg::MultiplayerParticipant const &from_participant, std::vector<uint8_t> data, bool is_reliable) {
  std::string receivedData = Utils::vec_to_string(data);
  buffer = receivedData;
  dataReceived = true;
}

void GooglePlayServicesImpl::LeaveRealtimeGameroom() {
  gameServices->RealTimeMultiplayer().LeaveRoom(realTimeRoom, [this](gpg::ResponseStatus const &response) {
    printf("Game Over. Leaving room.\n");
  });
  
  printf("GooglePlayServicesImpl::LeaveRealtimeGameroom() realtime gameroom not available\n");
}

void GooglePlayServicesImpl::SendMessageToParticipant(std::string &message, gpg::MultiplayerParticipant participant) {
  CCLOG("\n\nGooglePlayServicesImpl::SendMessageToParticipant \n\n%s\n\n", message.c_str());
  gameServices->RealTimeMultiplayer().SendReliableMessage(realTimeRoom, participant, Utils::str_to_vector(message), [=](gpg::MultiplayerStatus const & status) {
    if (gpg::MultiplayerStatus::VALID == status){
      //printf("GooglePlayServicesImpl::SendMessageToParticipant Send reliable msg success\n");
    } else {
      //printf("GooglePlayServicesImpl::SendMessageToParticipant Send reliable msg fail\n");
    }
  });
}

void GooglePlayServicesImpl::CreateRealtimeGameroom(const int minimumPlayersToStart, const int maxParticipants) {
  maxParticipantsInGameroom = maxParticipants;
  
  gpg::RealTimeRoomConfig config = gpg::RealTimeRoomConfig::Builder().SetMinimumAutomatchingPlayers(
                                                                                                    minimumPlayersToStart).Create();
  
  gameServices->RealTimeMultiplayer().CreateRealTimeRoom(config, this, [this](gpg::RealTimeMultiplayerManager::RealTimeRoomResponse const &response) {
    if (gpg::MultiplayerStatus::VALID == response.status) {
      printf("create room success: %s\n", response.room.Id().c_str());
      SetRoom(response.room);
      
      // Open the waiting room UI
      gameServices->RealTimeMultiplayer()
      .ShowWaitingRoomUI(realTimeRoom,
                         maxParticipantsInGameroom,
                         [this](gpg::RealTimeMultiplayerManager::WaitingRoomUIResponse const &wait_response) {
                           
                           if (gpg::IsSuccess(wait_response.status)) {
                             SetRoom(wait_response.room);
                             
                             // Start quick match when realtime game room is available.
                             EngineSingleton::GetInstance()->StartQuickMatch();
                             
                             printf("GooglePlayServicesImpl::CreateRealtimeGameroom() realtime gameroom available\n");
                           } else if (gpg::IsError(wait_response.status)) {
                             printf("GooglePlayServicesImpl::CreateRealtimeGameroom() Error in wait_response\n");
                             
                             std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::GPS_ERROR_WAIT_REPONSE);
                             EngineSingleton::GetInstance()->Notify(gameEvent.get());
                             
                             gameServices->RealTimeMultiplayer().LeaveRoom(realTimeRoom, [this](gpg::ResponseStatus const &response) {
                               printf("GooglePlayServicesImpl::CreateRealtimeGameroom() Game Over. Leaving room.\n");
                               printf("GooglePlayServicesImpl::CreateRealtimeGameroom() realtime gameroom not available\n");
                             });
                           } else {
                             printf("GooglePlayServicesImpl::CreateRealtimeGameroom() Unknown error in wait_response\n");
                           }
                         });
    } else {
      printf("GooglePlayServicesImpl::CreateRealtimeGameroom() Create room fail: %d\n", response.status);
    }
  });
}

void GooglePlayServicesImpl::SetRoom(gpg::RealTimeRoom const &room) {
  realTimeRoom = room;
}

gpg::RealTimeRoom *GooglePlayServicesImpl::Gameroom() {
  return &realTimeRoom;
}

std::vector<gpg::MultiplayerParticipant> GooglePlayServicesImpl::GameRoomParticipants() {
  return realTimeRoom.Participants();
}

void GooglePlayServicesImpl::SendMessageToParticipants(std::string &message) {
  CCLOG("\n\nGooglePlayServicesImpl::SendMessageToParticipants \n\n%s\n\n", message.c_str());
  std::string reliableMessageData = message;
  for (auto part : realTimeRoom.Participants()) {
    if (part.DisplayName() != StateManager::PlayerName) {
      gameServices->RealTimeMultiplayer().SendReliableMessage(realTimeRoom, part, Utils::str_to_vector(reliableMessageData), [=](gpg::MultiplayerStatus const & status) {
        if (gpg::MultiplayerStatus::VALID == status) {
          //printf("GooglePlayServicesImpl::SendMessageToParticipants Send reliable msg success\n");
        } else {
          //printf("GooglePlayServicesImpl::SendMessageToParticipants Send reliable msg fail\n");
        }
      });
    }
  }
}

void GooglePlayServicesImpl::OnConnect() {
  if (!gameServices) {
    return;
  }
  
  if (gameServices->IsAuthorized()) {
    gameServices->SignOut();
  } else {
    gameServices->StartAuthorizationUI();
  }
}

std::string GooglePlayServicesImpl::GooglePlayUsername() {
  return StateManager::PlayerName;
}

void GooglePlayServicesImpl::InitGpg() {
#ifdef SDKBOX_ENABLED
  sdkbox::PluginGPG::init();
#endif
  printf("GooglePlayServicesImpl::InitGPG()");
  StateManager::setCallback([this](gpg::AuthOperation op, gpg::AuthStatus st) {
    if(st == gpg::AuthStatus::ERROR_NOT_AUTHORIZED) {
      OnConnect();
    }
  });
  StateManager::Init(*CreatePlatformConfiguration().get());
  gameServices = StateManager::GetGameServices();
  if (!StateManager::IsSignedIn()) {
    StateManager::SignIn();
  } else {
    // Display message to user the that sign in failed
  }
}

void GooglePlayServicesImpl::PlayerStats() {
  gameServices->Stats().FetchForPlayer(gpg::DataSource::CACHE_OR_NETWORK, [this](const gpg::StatsManager::FetchForPlayerResponse& response) {
    if (IsSuccess(response.status)) {
      //printf("Days since last played: %ld.", (long)response.data.DaysSinceLastPlayed());
    } else {
      printf("Fetch for player error. code %d.", (int)response.status);
    }
  });
}

bool GooglePlayServicesImpl::IsSignedIn() {
  return StateManager::IsSignedIn();
}

bool GooglePlayServicesImpl::IsGameRoomValid() {
  return realTimeRoom.Valid();
}

bool GooglePlayServicesImpl::HasReceievedData() {
  return dataReceived;
}

std::string GooglePlayServicesImpl::Buffer() {
  dataReceived = false;
  return buffer;
}

void GooglePlayServicesImpl::ListGameSaves(){
  if(gameServices) {
    gameServices->Snapshots().ShowSelectUIOperation(true,
                                                    true,
                                                    kMaxSaves,
                                                    "Saved Games",
                                                    [this](gpg::SnapshotManager::SnapshotSelectUIResponse const & response) {
                                                      if (IsSuccess(response.status)) {
                                                        if (response.data.Valid()) {
                                                          // load a game.
                                                          // SnapshotMetadata
                                                          gpg::SnapshotMetadata snapshot = response.data;
                                                          std::string filename = snapshot.FileName();
                                                          CCLOGINFO("Load game: %s with description: %s", filename.c_str(), snapshot.Description().c_str());
                                                          LoadGame(filename);
                                                        } else {
                                                          // create game
                                                          SaveGame(kSaveGameName, kSaveGameDescription, JsonParser::JsonToString(gameConfigurationJsonObject), gpg::SnapshotConflictPolicy::MOST_RECENTLY_MODIFIED);
                                                        }
                                                      } else {
                                                        // error receiving info
                                                        CCLOGINFO("Error in snapshot select ui: %d", (int)response.status);
                                                      }
                                                    });
    
  }
}

void GooglePlayServicesImpl::DeleteGameSave(){
  if(gameServices){
    gpg::SnapshotConflictPolicy conflict_policy = gpg::SnapshotConflictPolicy::HIGHEST_PROGRESS;
    gameServices->Snapshots().Open(kSaveGameName,
                                   conflict_policy,
                                   [this](gpg::SnapshotManager::OpenResponse const &response) {
                                     if (IsSuccess(response.status)) {
                                       gameServices->Snapshots().Delete( response.data );
                                       CCLOG("Save deleted");
                                       EngineSingleton::GetInstance()->EndSession();
                                     } else {
                                       CCLOG( "Error deleting save");
                                       CCLOG("Delete error, code: %d", (int)response.status);
                                     }
                                   });
  }
}


void GooglePlayServicesImpl::SaveGame(const std::string &filename, const std::string &description, const std::string &content, gpg::SnapshotConflictPolicy conflict_policy) {
  if(gameServices) {
    std::vector<uint8_t> data = Utils::str_to_vector(content);
    gameServices->Snapshots().Open(filename,
                                   conflict_policy,
                                   [this, filename, description, data](gpg::SnapshotManager::OpenResponse const &response) {
                                     if (IsSuccess(response.status)) {
                                       gpg::SnapshotMetadata metadata = response.data;
                                       gpg::SnapshotMetadataChange::Builder builder;
                                       gpg::SnapshotMetadataChange metadata_change = builder.SetDescription(description).Create();
                                       gameServices->Snapshots().Commit(metadata,
                                                                        metadata_change,
                                                                        data,
                                                                        [this,filename](const gpg::SnapshotManager::CommitResponse &commit_response) {
                                                                          std::string message("Create game " + filename + " success.");
                                                                          std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::EVENT_SAVE_GAME_COMPLETE);
                                                                          EngineSingleton::GetInstance()->Notify(gameEvent.get());
                                                                        });
                                     } else {
                                       CCLOG("Error creating game save");
                                     }
                                   });
  }
}

void GooglePlayServicesImpl::LoadGame(const std::string &filename){
  if(gameServices) {
    gameServices->Snapshots().Open(gpg::DataSource::CACHE_OR_NETWORK,
                                   filename,
                                   gpg::SnapshotConflictPolicy::MOST_RECENTLY_MODIFIED,
                                   [this](gpg::SnapshotManager::OpenResponse const & response) {
                                     gpg::SnapshotManager::ReadResponse responseRead = gameServices->Snapshots().ReadBlocking(response.data);
                                     if ( responseRead.status == gpg::ResponseStatus::VALID ) {
                                       // Successful network response
                                       std::string responseData = Utils::vec_to_string(responseRead.data);
                                       gameConfigurationJsonObject = JsonParser::StringToJson(responseData);
                                       if(gameConfigurationJsonObject.IsObject()) {
                                         Director::getInstance()->getScheduler()->performFunctionInCocosThread([this]() {
                                           ControllerLocator::GetConfigController()->VaildateGameSaveData(gameConfigurationJsonObject);
                                         });
                                         CCLOG("Game loaded");
                                       } else {
                                         Director::getInstance()->getScheduler()->performFunctionInCocosThread([this]() {
                                           ControllerLocator::GetConfigController()->LoadDefaultGameConfiguration();
                                         });
                                         CCLOG("Default game loaded");
                                       }
                                       
                                     } else {
                                       CCLOG("Error loading game contents.");
                                     }
                                     
                                   });
  }
}

bool GooglePlayServicesImpl::IsAuthorized() {
  if(gameServices){
    return gameServices->IsAuthorized();
  }
  return false;
}
