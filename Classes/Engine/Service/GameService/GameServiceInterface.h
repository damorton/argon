// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ENGINE_SERVICE_GOOGLE_PLAY_SERVICE_GAME_SERVICE_INTERFACE_H_
#define ENGINE_SERVICE_GOOGLE_PLAY_SERVICE_GAME_SERVICE_INTERFACE_H_

#ifdef SDKBOX_ENABLED
#include "PluginGPG/PluginGPG.h"
#endif

class GameServiceInterface {
public:
  virtual ~GameServiceInterface(){}
  virtual bool IsAuthorized() = 0;
  virtual void ListGameSaves() = 0;
  virtual void DeleteGameSave() = 0;
  virtual void LoadGame(const std::string &filename) = 0;
  virtual void SaveGame(const std::string& filename, const std::string& description, const std::string& content, gpg::SnapshotConflictPolicy conflict_policy) = 0;
  virtual std::string GooglePlayUsername() = 0;
  virtual void SendMessageToParticipants(std::string &message) = 0;
  virtual void SendMessageToParticipant(std::string &message, gpg::MultiplayerParticipant participant) = 0;
  virtual gpg::RealTimeRoom *Gameroom() = 0;
  virtual void CreateRealtimeGameroom(const int minimumPlayersToStart, const int maxParticipants) = 0;
  virtual void LeaveRealtimeGameroom() = 0;
  virtual void PlayerStats() = 0;
  virtual bool IsSignedIn() = 0;
  virtual bool IsGameRoomValid() = 0;
  virtual bool HasReceievedData() = 0;
  virtual std::string Buffer() = 0;
  virtual std::vector<gpg::MultiplayerParticipant> GameRoomParticipants() = 0;
};

#endif
