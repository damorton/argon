// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ENGINE_SHOP_GAME_STORE_H_
#define ENGINE_SHOP_GAME_STORE_H_

#include <unordered_map>
#include "Game/Factory/CardFactory.h"
#include "Engine/Service/Shop/ShopInterface.h"

class GameStore : public ShopInterface {
public:
  GameStore(std::vector<std::shared_ptr<Card>> *cardCatalogue);
  virtual ~GameStore();
  virtual void Purchase(const std::string &productName) override;
  virtual std::vector<std::shared_ptr<GameStoreProduct>> *Products() override;
  virtual void RequestProducts() override;
  
private:
  void InitGameStore(std::vector<std::shared_ptr<Card>> *cardCatalogue);
  void UpdatePlayerGems(int purchasedGems);
  void UpdatePlayerGold(int purchasedGold);
  void PurchaseCard(GameStoreProduct *product);
  void PurchaseChest(GameStoreProduct *product);
  void PurchaseGold(GameStoreProduct *product);
  bool CardExists(std::shared_ptr<Card> card);
  std::shared_ptr<Card> ChooseRandomCardAt(Card::CardLevel level, std::vector<std::shared_ptr<Card>> *cardCatalogue);
  void SaveAndUpdateGame();
  
  // sdkbox::IAPListener
  virtual void onInitialized(bool ok) override;
  virtual void onSuccess(sdkbox::Product const& p) override;
  virtual void onFailure(sdkbox::Product const& p, const std::string &msg) override;
  virtual void onCanceled(sdkbox::Product const& p) override;
  virtual void onRestored(sdkbox::Product const& p) override;
  virtual void onProductRequestSuccess(std::vector<sdkbox::Product> const &products) override;
  virtual void onProductRequestFailure(const std::string &msg) override;
  virtual void onRestoreComplete(bool ok, const std::string &msg) override;
  
  std::vector<std::shared_ptr<GameStoreProduct>> storeProducts;
};

#endif
