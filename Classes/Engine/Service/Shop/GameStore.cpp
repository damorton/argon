// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/Service/Shop/GameStore.h"
#include "Engine/Service/Shop/GameStoreProduct.h"
#include "Engine/Singleton/EngineSingleton.h"
#include "Game/Factory/ChestFactory.h"

GameStore::GameStore(std::vector<std::shared_ptr<Card>> *cardCatalogue) {
#ifdef SDKBOX_ENABLED
  sdkbox::IAP::setDebug(true);
  sdkbox::IAP::setListener(this);
  sdkbox::IAP::init();
#endif  
  
  InitGameStore(cardCatalogue);
  CCLOG("Game store created");
}

GameStore::~GameStore() {
  storeProducts.clear();
}

void GameStore::InitGameStore(std::vector<std::shared_ptr<Card>> *cardCatalogue) {
  std::string gameStoreConfigFile = cocos2d::FileUtils::getInstance()->fullPathForFilename(kGameConfigurationFile);
  rapidjson::Document gameStoreProductConfig = JsonParser::FileToJson(gameStoreConfigFile);
  
  assert(gameStoreProductConfig.HasMember(kGameStoreProductsConfig.c_str()));
  rapidjson::Value &productArray = gameStoreProductConfig[kGameStoreProductsConfig.c_str()];
  assert(productArray.IsArray());
  
  for(int product = 0; product < productArray.Size(); product++) {
    std::string productTitle = productArray[product]["product_title"].GetString();
    std::string productName = productArray[product]["product_name"].GetString();
    std::string productId = productArray[product]["product_id"].GetString();
    std::string productImage = productArray[product]["product_image"].GetString();
    int productGemCost = productArray[product]["product_gem_cost"].GetInt();
    int productGoldCost = productArray[product]["product_gold_cost"].GetInt();
    int productValue = productArray[product]["product_value"].GetInt();
    
    std::shared_ptr<GameStoreProduct> gameStoreProduct = std::make_shared<GameStoreProduct>(productImage, productTitle, productName, productId, productGemCost, productGoldCost, productValue);
    
    // Create card product
    std::shared_ptr<Card> cardProduct;
    if(productName == "common_card"){
      cardProduct = ChooseRandomCardAt(Card::COMMON_CARD, cardCatalogue);
    } else if(productName == "rare_card") {
      cardProduct = ChooseRandomCardAt(Card::RARE_CARD, cardCatalogue);
    } else if(productName == "epic_card") {
      cardProduct = ChooseRandomCardAt(Card::EPIC_CARD, cardCatalogue);
    } else {
      cardProduct = nullptr;
    }
    
    if(cardProduct){
      gameStoreProduct->SetCard(cardProduct);
      std::string cardName = cardProduct->GetCardName();
      gameStoreProduct->SetTitle(cardName);
    }
    
    storeProducts.push_back(gameStoreProduct);
  }
  
  CCLOG("Product config loading from default file %s", gameStoreConfigFile.c_str());
}

std::shared_ptr<Card> GameStore::ChooseRandomCardAt(Card::CardLevel level, std::vector<std::shared_ptr<Card>> *cardCatalogue){
  std::shared_ptr<Card> card;
  do{
    card = CardFactory::CreateRandomCardAtLevel(level, cardCatalogue);
  }while(CardExists(card));
  return card;
}

bool GameStore::CardExists(std::shared_ptr<Card> card){
  Card *cardRefTemp = card.get();
  if(std::find_if(storeProducts.begin(), storeProducts.end(), [cardRefTemp](std::shared_ptr<GameStoreProduct> cardRef){
    return (cardRefTemp->GetCardName() == cardRef->StoredCard()->get()->GetCardName());
  }) != storeProducts.end()){
    return true;
  } else {
    return false;
  }
}

void GameStore::Purchase(const std::string &productName) {
  CCLOG("Purchasing %s", productName.c_str());
  GameStoreProduct *product;
  for(auto storeProduct : storeProducts) {
    if(productName == storeProduct->Name()) {
      product = storeProduct.get();
    }
  }
  
  if(product->GetId() == "card"){
    PurchaseCard(product);
    SaveAndUpdateGame();
  } else if(product->GetId() == "chest"){
    PurchaseChest(product);
    SaveAndUpdateGame();
  } else if (product->GetId() == "gold") {
    PurchaseGold(product);
    SaveAndUpdateGame();
  } else {
#ifdef SDKBOX_ENABLED
  sdkbox::IAP::purchase(productName);
#endif
  }
}

void GameStore::PurchaseCard(GameStoreProduct *product) {
  if(ControllerLocator::GetConfigController()->CheckGoldAmount(product->GoldCost())) {
    std::vector<std::shared_ptr<Card>> *cardCatalogue = ControllerLocator::GetConfigController()->GetCardCatalogue();
    for(auto card : *cardCatalogue){
      if(card->GetCardName() == product->Name()) {
        Card::CardData cardData = card->GetCardData();
        std::shared_ptr<Card> purchasedCard = CardFactory::CreateCard(cardData);
        EngineSingleton::GetInstance()->GetCardCollection()->AddCard(purchasedCard);
        UpdatePlayerGold(-product->GoldCost());
        break;
      }
    }
  } else {
    CCLOG("Purchase card failed, not enough gold");
  }
}
  
void GameStore::PurchaseChest(GameStoreProduct *product) {
  if(ControllerLocator::GetConfigController()->CheckGemAmount(product->GemCost())) {
    unsigned long currentNumberOfChests = EngineSingleton::GetInstance()->Chests()->size();
    if(currentNumberOfChests < kMaxNumberOfChests) {
      if(product->Title() == "Silver Chest") {
        std::shared_ptr<Chest> chest = ChestFactory::Create(Chest::CHEST_SILVER, EngineSingleton::GetInstance()->GetTimestamp());
        EngineSingleton::GetInstance()->AddChest(chest);
        UpdatePlayerGems(-product->GemCost());
      } else if(product->Title() == "Giant Chest") {
        
      } else if(product->Title() == "Magical Chest") {
        
      }
    } else {
      CCLOG("Purchase chest failed, no chest slots available");
    }
  } else {
    CCLOG("Purchase chest failed, not enough gems");
  }
}

void GameStore::PurchaseGold(GameStoreProduct *product) {
  if(ControllerLocator::GetConfigController()->CheckGemAmount(product->GemCost())) {
    UpdatePlayerGold(product->Value());
    UpdatePlayerGems(-product->GemCost());
  } else {
    CCLOG("Purchase gold failed, not enough gems");
  }
}

void GameStore::RequestProducts() {
#ifdef SDKBOX_ENABLED
  sdkbox::IAP::refresh();
#endif
}

void GameStore::onInitialized(bool ok) {
  CCLOG("GameStore::onInitialized");
}

void GameStore::onSuccess(sdkbox::Product const& p) {
  CCLOG("GameStore::onSuccess. Product: %s", p.name.c_str());
  for(auto storeProduct : storeProducts) {
    if(p.id == storeProduct->GetId()) {
      UpdatePlayerGems(storeProduct->Value());
    }
  }
  
  SaveAndUpdateGame();
}

void GameStore::SaveAndUpdateGame(){
  std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::EVENT_GAME_MODEL_UPDATED);
  EngineSingleton::GetInstance()->Notify(gameEvent.get());
  
  gameEvent = GameEventFactory::Create(GameEvent::EVENT_SAVE_GAME_CONFIG);
  EngineSingleton::GetInstance()->Notify(gameEvent.get());
}

void GameStore::UpdatePlayerGems(int purchasedGems) {
  ControllerLocator::GetConfigController()->UpdatePlayerGems(purchasedGems);
}

void GameStore::UpdatePlayerGold(int purchasedGold) {
  ControllerLocator::GetConfigController()->UpdatePlayerGold(purchasedGold);
}

std::vector<std::shared_ptr<GameStoreProduct>> *GameStore::Products() {
  return &storeProducts;
}

void GameStore::onFailure(sdkbox::Product const& p, const std::string &msg) {
  CCLOG("GameStore::onFailure. Product: %s", p.name.c_str());
}

void GameStore::onCanceled(sdkbox::Product const& p) {
  CCLOG("GameStore::onCanceled");
}

void GameStore::onRestored(sdkbox::Product const& p) {
  CCLOG("GameStore::onRestored");
}

void GameStore::onProductRequestSuccess(std::vector<sdkbox::Product> const &products) {
  CCLOG("GameStore::onProductRequestSuccess");
  for(auto product : products){
    for(auto storeProduct : storeProducts) {
      if(product.id == storeProduct->GetId()) {
        storeProduct->SetPrice(product.price);
      }
    }
  }
}

void GameStore::onProductRequestFailure(const std::string &msg) {
  CCLOG("GameStore::onProductRequestFailure");
}

void GameStore::onRestoreComplete(bool ok, const std::string &msg) {
  CCLOG("GameStore::onRestoreComplete");
}
