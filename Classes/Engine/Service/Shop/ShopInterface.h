// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ENGINE_SHOP_SHOP_INTERFACE_H_
#define ENGINE_SHOP_SHOP_INTERFACE_H_

#ifdef SDKBOX_ENABLED
#include "PluginIAP/PluginIAP.h"
#endif
#include "cocos2d.h"
#include "Engine/Service/Shop/GameStoreProduct.h"

class ShopInterface : public sdkbox::IAPListener {
public:
  virtual void Purchase(const std::string &productId) = 0;
  virtual std::vector<std::shared_ptr<GameStoreProduct>> *Products() = 0;
  virtual void RequestProducts() = 0;
};
#endif
