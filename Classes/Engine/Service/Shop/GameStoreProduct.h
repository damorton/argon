// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ENGINE_SERVICE_SHOP_GAME_STORE_PRODUCT_H_
#define ENGINE_SERVICE_SHOP_GAME_STORE_PRODUCT_H_

#include <string>
#include "cocos2d.h"

class Card;

class GameStoreProduct {
public:
  GameStoreProduct(std::string &productImage, std::string &productTitle, std::string &productName, std::string &productId, int productGemCost, int productGoldCost, int productValue);
  virtual ~GameStoreProduct();
  std::string GetId();
  int GemCost();
  int GoldCost();
  int Value();
  std::string Title();
  void SetTitle(std::string &cardTitle);
  
  /**
   * Get the name of the product e.g. product_name.
   *
   * @return name of the game store product.
   */
  std::string Name();
  
  /**
   * Get the game store products sprite image.
   * 
   * @return game store product sprite, or nullptr.
   */
  cocos2d::Sprite *Sprite();
  void SetPrice(std::string &productPrice);
  std::string Price();
  void SetCard(std::shared_ptr<Card> productCard);
  std::shared_ptr<Card> *StoredCard();
  cocos2d::Sprite *CurrencySprite();
  void SetCurrencySprite(cocos2d::Sprite *currencySprite);
  
private:
  
  std::string title;
  std::string name;
  std::string identifier;
  int gemCost;
  int goldCost;
  int value;
  std::string price;
  cocos2d::Sprite *sprite;
  cocos2d::Sprite *currencySpriteImage;
  std::shared_ptr<Card> card;
};

#endif
