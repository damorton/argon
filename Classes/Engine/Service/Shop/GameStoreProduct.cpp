// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/Service/Shop/GameStoreProduct.h"
#include "Game/Card.h"
#include "Engine/Utils/Utils.h"

GameStoreProduct::GameStoreProduct(std::string &productImage, std::string &productTitle, std::string &productName, std::string &productId, int productGemCost, int productGoldCost, int productValue) {
  sprite = cocos2d::Sprite::create(productImage);
  sprite->retain();
  price = "";
  identifier = productId;
  gemCost = productGemCost;
  if(gemCost > 0) {
    price = Utils::to_str(gemCost);
  }
  goldCost = productGoldCost;
  if(goldCost > 0) {
    price = Utils::to_str(goldCost);
  }
  value = productValue;
  title = productTitle;
  name = productName;
  currencySpriteImage = nullptr;
}

GameStoreProduct::~GameStoreProduct() {
  printf("GameStore dtor %s", title.c_str());
  sprite->release();
}

std::string GameStoreProduct::GetId() {
  return identifier;
}

int GameStoreProduct::GemCost() {
  return gemCost;
}

int GameStoreProduct::GoldCost() {
  return goldCost;
}

int GameStoreProduct::Value() {
  return value;
}

std::string GameStoreProduct::Title() {
  return title;
}

void GameStoreProduct::SetTitle(std::string &cardTitle){
  title = cardTitle;
}

std::string GameStoreProduct::Name() {
  return name;
}

void GameStoreProduct::SetCard(std::shared_ptr<Card> productCard) {
  card = productCard;
  name = productCard->GetCardName();
}

std::shared_ptr<Card> *GameStoreProduct::StoredCard() {
  return &card;
}

void GameStoreProduct::SetPrice(std::string &productPrice) {
  price = productPrice;
}

std::string GameStoreProduct::Price() {
  return price;
}

cocos2d::Sprite *GameStoreProduct::Sprite() {
  return sprite;
}

cocos2d::Sprite *GameStoreProduct::CurrencySprite(){
  return currencySpriteImage;
}

void GameStoreProduct::SetCurrencySprite(cocos2d::Sprite *currencySprite){
  currencySpriteImage = currencySprite;
}
