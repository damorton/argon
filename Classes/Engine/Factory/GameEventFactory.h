// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef GAME_FACTORY_GAME_EVENT_FACTORY_H_
#define GAME_FACTORY_GAME_EVENT_FACTORY_H_

#include "Engine/Utils/GameEvent.h"
#include <memory>

class GameEventFactory {
public:
  virtual ~GameEventFactory(){};
  static std::shared_ptr<GameEvent> Create(GameEvent::EventType eventType, float positionX = 0, float positionY = 0);
private:
  GameEventFactory(){};
  GameEventFactory(const GameEventFactory &){}
  GameEventFactory &operator=(const GameEventFactory &){return *this;}
};

#endif
