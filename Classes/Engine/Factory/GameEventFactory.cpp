// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "GameEventFactory.h"

std::shared_ptr<GameEvent> GameEventFactory::Create(GameEvent::EventType eventType, float positionX, float positionY) {
  return std::make_shared<GameEvent>(eventType, positionX, positionY);
}
