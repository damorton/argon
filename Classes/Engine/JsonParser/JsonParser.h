// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef BASE_JSON_PARSER_H_
#define BASE_JSON_PARSER_H_

#include <string>
#include "external/json/document.h"

class JsonParser {
public:
  /**
   * Converts json structured string to rapid json document.
   *
   * @param std::string& json object string.
   * @return rapid json document.
   */
  static rapidjson::Document StringToJson(std::string &jsonObjectString);
  
  
  /**
   * Converts json object to string.
   * 
   * @param rapidjson::Document& rapid json document.
   * @return string representation of rapid json document.
   */
  static std::string JsonToString(rapidjson::Document &document);
  
  /**
   * Creates an empty json object.
   * 
   * @return empty rapid json document.
   */
  static rapidjson::Document CreateEmptyJsonDocument();
  
  /**
   * Reads a json structured file and converts it to a rapid json document.
   *
   * @param std::string& full path to json file.
   * @return rapid json document with json file contents.
   */
  static rapidjson::Document FileToJson(const std::string &fullPath);
  
  /**
   * Writes json object data to file.
   * 
   * @param rapidjson::Document& rapid json document.
   * @param std::string& full writable path to file system.
   */
  static void JsonToFile(rapidjson::Document &jsonObject, const std::string &fullPath);

private:
  JsonParser() {}
  JsonParser(const JsonParser &){}
  JsonParser &operator=(const JsonParser &){return *this;}
};
#endif
