// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/JsonParser/JsonParser.h"
#include <fstream>
#include "cocos/cocos2d.h"
#include "external/json/stringbuffer.h"
#include "external/json/writer.h"

std::string JsonParser::JsonToString(rapidjson::Document &jsonObject) {
  rapidjson::StringBuffer buffer;
  rapidjson::Writer<rapidjson::StringBuffer> jsonWriter(buffer);
  jsonObject.Accept(jsonWriter);
  return buffer.GetString();
}

rapidjson::Document JsonParser::StringToJson(std::string &jsonObjectString) {
  rapidjson::Document document = CreateEmptyJsonDocument();
  document.Parse(jsonObjectString.c_str());
  return document;
}

rapidjson::Document JsonParser::CreateEmptyJsonDocument() {
  rapidjson::Document jsonDocument;
  jsonDocument.SetObject();
  return jsonDocument;
}

rapidjson::Document JsonParser::FileToJson(const std::string &fullPath) {
  std::string sbuf;
  cocos2d::FileUtils::getInstance()->getContents(fullPath, &sbuf);
  return StringToJson(sbuf);
}

void JsonParser::JsonToFile(rapidjson::Document &jsonObject, const std::string &fullPath) {
  std::ofstream outputFile;
  outputFile.open(fullPath);
  if (outputFile.is_open()) {
    std::string jsonObjectData = JsonToString(jsonObject);
    outputFile << jsonObjectData;
  }
  outputFile.close();
}
