// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ENGINE_SINGLETON_ENGINE_SINGLETON_H_
#define ENGINE_SINGLETON_ENGINE_SINGLETON_H_

#include <string>
#include <ctime>

#include "cocos/cocos2d.h"

#include "../Service/GameService/GooglePlayServicesImpl.h"
#include "Engine/Utils/Utils.h"
#include "Engine/Utils/Subject.h"
#include "Engine/Locator/ControllerLocator.h"
#include "Engine/Locator/UILocator.h"
#include "Engine/Locator/ServiceLocator.h"
#include "Engine/Factory/GameEventFactory.h"
#include "Game/CardCollection.h"
#include "GameDefines.h"

class EngineSingleton : public Subject {
public:
  /**
   * Get the instance of the game engine singleton. Instantiation occurs on first call.
   *
   * @return EngineSingleton* instance of the engine singleton.
   */
  static EngineSingleton* GetInstance();
  
  /**
   * Initializes the game specific objects and services.
   * This must be called before attempting to use the EngineSingleton.
   */
  void Init();

  void Update(float dt);
  void EndGameSession();
  bool IsHost();
  gpg::MultiplayerParticipant HostParticipant();
  void ProcessIncomingNetworkData(std::string &data);
  void ProcessEvent(GameEvent *event);
  static unsigned long long GetTimestamp();
  unsigned long long GetTimestampDelta();
  void SetGameSessionStartTime();
  void StartQuickMatch();
  std::string GetHostUsername();
  int FlipCoin();
  int GetRandomInt(int min, int max);
  void ClockTimer();
  void AddChest(std::shared_ptr<Chest> &chest);
  void RemoveChest(Chest *chest);
  std::vector<std::shared_ptr<Chest>> *Chests();
  void EndSession();
  void CreateGameStore();
  CardCollectionInterface *GetCardCollection();
  
private:
  EngineSingleton();
  virtual ~EngineSingleton();
  void SeedRandomNumberGenerator();
  void CreateCardCollection();
  void CreateControllers();
  void CreateServices();
  void CreateRealtimeGameRoom();
  void InitNewGame();
  void PickHost();
  std::string FindParticipantWithHighestId();
  void SetHost(const std::string &hostUsername);
  void PlayerReadyToStart();
  void BroadcastUpdatedGameModel();
  void CleanUpGameInstance();
  
  static EngineSingleton *m_Instance;
  unsigned long long m_llGameSessionStart;
  std::string m_HostUsername;
  std::string m_Username;
  std::vector<std::shared_ptr<Chest>> m_vChests;
  bool startQuickMatch;
  bool gameConfigLoadRequestSent;
  bool m_bIsHost;
  gpg::MultiplayerParticipant hostParticipant;
  std::unique_ptr<CardCollectionInterface> cardCollection;
};

#endif
