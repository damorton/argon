// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Engine/Singleton/EngineSingleton.h"
#include "Game/Card.h"
#include "Game/Player.h"
#include "Engine/JsonParser/JsonParser.h"
#include "Engine/Service/Shop/GameStore.h"
#include "Engine/Utils/GameEvent.h"

using namespace std;

EngineSingleton* EngineSingleton::m_Instance = nullptr;

EngineSingleton* EngineSingleton::GetInstance() {
  if (m_Instance == nullptr)
    m_Instance = new EngineSingleton();
  return m_Instance;
}

EngineSingleton::~EngineSingleton() {
  CleanUpGameInstance();
  m_vChests.clear();
  cardCollection.reset();
}

void EngineSingleton::CleanUpGameInstance() {
  auto dashboardUI = DashboardUI::createScene();
  cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.0f, dashboardUI, cocos2d::Color3B::BLACK));
  
  ServiceLocator::GooglePlayService()->LeaveRealtimeGameroom();
  ControllerLocator::ResetGameControllerService();
  
  m_bIsHost = false;
  startQuickMatch = false;
}

EngineSingleton::EngineSingleton() {
}

void EngineSingleton::Init() {
  SeedRandomNumberGenerator();
  
  m_bIsHost = false;
  startQuickMatch = false;
  gameConfigLoadRequestSent = false;
  
  CreateCardCollection();
  CreateControllers();
  CreateServices();
}

void EngineSingleton::SeedRandomNumberGenerator() {
  std::srand((unsigned int)std::time(NULL));
}

void EngineSingleton::CreateCardCollection() {
  std::unique_ptr<CardCollectionInterface> collection(new CardCollection());
  cardCollection = std::move(collection);
}

void EngineSingleton::CreateControllers() {
  ControllerLocator::Register(std::unique_ptr < InputController > (new InputController()));
  ControllerLocator::Register(std::unique_ptr < DaoController > (new DaoController()));
  ControllerLocator::Register(std::unique_ptr < ConfigController > (new ConfigController()));
  ControllerLocator::Register(std::unique_ptr < SimpleAudioEngineWrapper > (new SimpleAudioEngineWrapper()));
}

void EngineSingleton::CreateServices() {
  ServiceLocator::Register(std::unique_ptr<GameServiceInterface> (new GooglePlayServicesImpl()));
}

void EngineSingleton::CreateGameStore() {
  std::vector<std::shared_ptr<Card>> *cardCatalogue = ControllerLocator::GetConfigController()->GetCardCatalogue();
  ServiceLocator::Register(std::unique_ptr<ShopInterface> (new GameStore(cardCatalogue)));
}

void EngineSingleton::ClockTimer() {
  if(ControllerLocator::GetGameController()) {
    ControllerLocator::GetGameController()->ClockTimer();
  }
}

bool EngineSingleton::IsHost() {
  return m_bIsHost;
}

gpg::MultiplayerParticipant EngineSingleton::HostParticipant() {
  return hostParticipant;
}

void EngineSingleton::EndSession() {
  cocos2d::Director::getInstance()->end();
}

void EngineSingleton::Update(float dt) {
  
  if(startQuickMatch) {
    InitNewGame();
    startQuickMatch = false;
  }
  
  if(!gameConfigLoadRequestSent && ServiceLocator::GooglePlayService()->IsAuthorized()) {
    ControllerLocator::GetDaoController()->LoadGame(kSaveGameName);
    gameConfigLoadRequestSent = true;
  }
  
  if(ControllerLocator::GetGameController()){
    ControllerLocator::GetGameController()->Update();
  }
  
  if(ServiceLocator::GooglePlayService()->HasReceievedData()) {
    std::string networkData = ServiceLocator::GooglePlayService()->Buffer();
    ProcessIncomingNetworkData(networkData);
  }
}

void EngineSingleton::InitNewGame() {
  auto cardSelectUI = CardSelectUI::createScene();
  cocos2d::Director::getInstance()->replaceScene(cocos2d::TransitionFade::create(1.0f, cardSelectUI, cocos2d::Color3B::BLACK));
  PickHost();
  ControllerLocator::Register(std::unique_ptr<GameController>(new GameController()));
}

void EngineSingleton::PickHost() {
  if (ServiceLocator::GooglePlayService()->IsGameRoomValid()) {
    std::string participantWithHighestId = FindParticipantWithHighestId();
    SetHost(participantWithHighestId);
  }
}

std::string EngineSingleton::FindParticipantWithHighestId() {
  std::string participantId;
  std::string participantUsername;
  std::vector<gpg::MultiplayerParticipant> gameroomParticipants = ServiceLocator::GooglePlayService()->GameRoomParticipants();
  for (auto part : gameroomParticipants) {
    if (part.Id() > participantId) {
      participantId = part.Id();
      participantUsername = part.DisplayName();
      hostParticipant = part;
    }
  }
  return participantUsername;
}

void EngineSingleton::SetHost(const std::string &hostUsername) {
  m_HostUsername = hostUsername;
  std::string localPlayersUsername = ServiceLocator::GooglePlayService()->GooglePlayUsername();
  if (localPlayersUsername == m_HostUsername) {
    m_bIsHost = true;
  } else {
    m_bIsHost = false;
  }
}

void EngineSingleton::PlayerReadyToStart() {
  ControllerLocator::GetGameController()->PlayerReadyToStart();
}

void EngineSingleton::CreateRealtimeGameRoom() {
  ServiceLocator::GooglePlayService()->CreateRealtimeGameroom(kMinimumParticipantsInGameroomToStart, kParticipantsAllowedInGameroom);
}

void EngineSingleton::EndGameSession() {
  CleanUpGameInstance();
}

void EngineSingleton::ProcessEvent(GameEvent *event) {
  switch (event->Type()) {
    case GameEvent::MENU_BUTTON_QUICK_MATCH_START:
      CreateRealtimeGameRoom();
      break;
    case GameEvent::MENU_BUTTON_START_GAME:
      PlayerReadyToStart();
      break;
    case GameEvent::TOUCH_INPUT_CARD_SELECT:
      ControllerLocator::GetGameController()->ProcessCardSelectTouchInput(event->PositionX(), event->PositionY());
      break;
    case GameEvent::TOUCH_INPUT_GAME_BOARD_BEGIN:
      if (ControllerLocator::GetGameController()->GetGameState() == GameState::GAME_IN_PROGRESS) {
        ControllerLocator::GetGameController()->ProcessGameBoardTouchInputBegin(event->PositionX(), event->PositionY());
      }
      break;
    case GameEvent::TOUCH_INPUT_GAME_BOARD_END:
      if (ControllerLocator::GetGameController()->GetGameState() == GameState::GAME_IN_PROGRESS) {
        ControllerLocator::GetGameController()->ProcessGameBoardTouchInputEnd(event->PositionX(), event->PositionY());
      }
      break;
    default:
      break;
  }
}

void EngineSingleton::ProcessIncomingNetworkData(std::string &data) {
  rapidjson::Document jsonObject = JsonParser::StringToJson(data);
  if (jsonObject.IsObject()) {
    ControllerLocator::GetGameController()->ProcessIncomingJsonObject(jsonObject);
  }
}

unsigned long long EngineSingleton::GetTimestamp() {
  auto duration = std::chrono::system_clock::now().time_since_epoch();
  unsigned long long millis = std::chrono::duration_cast < std::chrono::milliseconds > (duration).count();
  return millis;
}

unsigned long long EngineSingleton::GetTimestampDelta() {
  return GetTimestamp() - m_llGameSessionStart;
}

void EngineSingleton::SetGameSessionStartTime() {
  m_llGameSessionStart = GetTimestamp();
}

void EngineSingleton::StartQuickMatch() {
  startQuickMatch = true;
}

std::string EngineSingleton::GetHostUsername() {
  return m_HostUsername;
}

int EngineSingleton::FlipCoin() {
  return std::rand() % 2;
}

int EngineSingleton::GetRandomInt(int min, int max) {
  return std::rand() % (max-min + 1) + min;
}

void EngineSingleton::AddChest(std::shared_ptr<Chest> &chest) {
  m_vChests.push_back(chest);
}

std::vector<std::shared_ptr<Chest>> *EngineSingleton::Chests() {
  return &m_vChests;
}

void EngineSingleton::RemoveChest(Chest *chest) {
  m_vChests.erase(std::remove_if(m_vChests.begin(), m_vChests.end(),
                                 [chest](std::shared_ptr<Chest> chestRef)
                                 {
                                   return chestRef->Id() == chest->Id();
                                 }), m_vChests.end());
}

CardCollectionInterface *EngineSingleton::GetCardCollection() {
  return cardCollection.get();
}
