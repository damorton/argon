// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef GAME_DEFINES_H_
#define GAME_DEFINES_H_

#include "cocos2d.h"

// Music
const std::string kDashboardBackgroundMusic = "music/dashboard_background_music.wav";
const std::string kMenuSelectSoundEffect = "music/menu_select_soundeffect.wav";
const std::string kCardSelectSoundEffect = "music/menu_select_soundeffect.wav";
const std::string kPlaceCardSuccessfulSoundEffect = "music/place_card_successful.wav";
const std::string kOpenChestSoundEffect = "music/place_card_successful.wav";
const std::string kShopBackgroundMusic = "music/gameworld_background_music.wav";
const std::string kGameworldBackgroundMusic = "music/gameworld_background_music.wav";

const int kMaxSaves = 8;
const std::string kSaveGameName = "ArgonGameSave";
const std::string kSaveGameDescription = "A game save of cocos2d-x game";

const int kMinimumParticipantsInGameroomToStart = 1;
const int kParticipantsAllowedInGameroom = 1;
const int kMaxBoardRows = 3;
const int kMaxBoardColumns = 3;
const int kMaxNumberOfChests = 4;

const int kMaxNumberOfCardsInDeck = 5;
const int kMaxNumberOfCardsInCollectionRow = 4;
const int kMaxNumberOfProductsInCollectionRow = 3;
const int kGameSessionTimeSeconds = 60;
const int kCardSelectTimeout = 30;

const int kPaddingSmall = 25;
const int kPaddingMedium = 40;
const int kPaddingLarge = 60;

const std::string kCardBlankOwner = "null";
const std::string kCardBlankName = "card_blank";

// Particle effect
const std::string kCardPlacedParicleEffect = "particle_effect/card_particle_placed.plist";
const std::string kCardBlueParicleEffect = "particle_effect/card_particle_blue.plist";
const std::string kCardRedParicleEffect = "particle_effect/card_particle_red.plist";
const std::string kGameboardBackgroundParticleEffect = "particle_effect/gameboard_particle_background.plist";
const std::string kLoadingSpinnerParicleEffect = "particle_effect/loading_spinner.plist";
// Background
const std::string kDashboardUIBackgroundImage = "background/dashboard_background.png";
const std::string kLoadingUIBackgroundImage = "background/loading_ui_background.png";
const std::string kRewardUIBackgroundImage = "background/reward_ui_background.png";
const std::string kGameboardUIBackgroundImage = "background/gameboard_ui_background.png";
const std::string kCardCollectionUIBackgroundImage = "background/card_collection_ui_background.png";
const std::string kShopUIBackgroundImage = "background/shop_ui_background.png";
const std::string kWidgetContentBackgroundImage = "background/widget_content_background.png";
const std::string kGreyTransparentBackground = "background/grey_transparent_background.png";
const std::string kAvatarBackgroundBlue = "background/avatar_background_blue.png";
const std::string kAvatarBackgroundRed = "background/avatar_background_red.png";
const std::string kChestSlotFilename = "background/chest_slot_background.png";
const std::string kChestSlotLabelBackgroundFilename = "background/chest_slot_label_background.png";
const std::string kPlayerResourceBackgroundSmallFilename = "background/player_resource_background_small.png";
const std::string kPlayerResourceBackgroundMediumFilename = "background/player_resource_background_medium.png";

// Card
const std::string kCardBlankFilename = "card/card_blank.png";
const std::string kCardNeutralFilename = "card/card_neutral.png";
const std::string kBlueBackgroundFilename = "card/BlueCardBackground.png";
const std::string kRedBackgroundFilename = "card/RedCardBackground.png";
const std::string kCommonCardBackgroundImage = "card/card_common_background.png";
const std::string kRareCardBackgroundImage = "card/card_rare_background.png";
const std::string kEpicCardBackgroundImage = "card/card_epic_background.png";
const std::string kCardSelectedImageFilename = "card/card_selected.png";
// Chest
const std::string kFreeChestFilename = "chest/chest_free.png";
const std::string kSilverChestFilename = "chest/chest_silver.png";

// Buttons
const std::string kOpenWithGemsButtonBackgroundImage = "button/open_with_gems_button.png";
const std::string kStartUnlockTimerButtonBackgroundImage = "button/start_unlock_timer_button.png";
const std::string kConfirmButtonBackgroundImage = "button/confirm_button.png";
const std::string kCancelButtonBackgroundImage = "button/cancel_button.png";

// Resource labels
const std::string kPlusSignFilename = "resource_labels/plus_sign.png";
const std::string kStarFilename = "resource_labels/star.png";
const std::string kGoldCoinFilename = "resource_labels/gold_coin.png";
const std::string kGemFilename = "resource_labels/gem.png";

// Game configuration files
const std::string kGameConfigurationFile = "config/game_config.json";

// Json
const std::string kGameConfigurationJsonObject = "game_configuration";
const std::string kGameStoreProductsConfig = "game_store_products";
const std::string kGameConfigSerialNumberKey = "game_data_serial_number";
const std::string kChestDataJsonObject = "chests";
const std::string kGametime = "gametime";
const std::string kGamesession = "gamesession";
const std::string kUsername = "username";
const std::string kStars = "stars";
const std::string kGold = "gold";
const std::string kGems = "gems";

// UI Names
const std::string kShopUIName = "shop";

// Shop
const std::string kProductBackgroundFilename = "shop/product_background.png";
const std::string kGemShopImageFilename = "shop/gem_shop_image.png";
const std::string kGoldShopImageFilename = "shop/gold_shop_image.png";

// Milliseconds between timestamps
// When comparing event timestamps the maximum delta allow is
// set by the kTimestampLimit
const int kTimestampLimit = 300;

// Number of conflict resolution attempts before failure
const int kMaxConflictResolutionAttempts = 10;

const std::string kFontType = "font/Roboto-Regular.ttf";

// Font sizes
const int kFontSizeSmall = 18;
const int kFontSizeSmallPlus = 24;
const int kFontSizeMedium = 36;
const int kFontSizeMediumLarge = 54;
const int kFontSizeLarge = 72;
const int kTextOutlineSize = 2;

enum CardColor {
  CARD_COLOR_BLANK,
  CARD_COLOR_NEUTRAL,
  CARD_COLOR_RED,
  CARD_COLOR_BLUE
};

#endif
