// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef GAME_PLAYER_H_
#define GAME_PLAYER_H_

#include "GameDefines.h"

enum PlayerNumber{
  PLAYER_ONE,
  PLAYER_TWO
};

class Player {
public:
  Player(std::string &playerName, CardColor cardColor, PlayerNumber playerNumber);
  ~Player();
  std::string Name();
  CardColor Color();
  PlayerNumber Number();
  
  // Sets the image buffer for the players avatar
  void SetAvatarImageBuffer(std::vector<char> &imageBuffer);
  
  // Creates the avatar image using the image buffer
  void CreatePlayerAvatar();
  
  // Returns the players avatar image sprite
  cocos2d::Sprite *AvatarImage();

private:
  CardColor color;
  std::string name;
  cocos2d::Sprite *avatar;
  PlayerNumber number;
  std::vector<char> avatarImageBuffer;
};

#endif
