// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef GAME_GAME_SESSION_H_
#define GAME_GAME_SESSION_H_

#include <string>
#include "Engine/JsonParser/JsonParser.h"

class GameSession {
public:
  void SaveGold(int goldAmount);
  void SaveGems(int gemsAmount);
  void SaveGameTimeMilliseconds(unsigned long long ms);
  std::string SerializeGameSession();
  unsigned int Gold();
  unsigned int Gems();
  unsigned int GameTimeSeconds();
  unsigned long long GameTimeMilliseconds();
private:
  void CreateGamesessionJsonObjectAndInfoArray();
  void AddValueToInfoArray(const std::string &valueName, int value, rapidjson::Document::AllocatorType &allocator);
  void AddInfoArrayToGamesessionObject(const std::string &gamesessionObjectName, rapidjson::Document::AllocatorType &allocator);
  
  unsigned long long gameTimeMilliseconds = 0;
  unsigned int gametimeSeconds = 0;
  unsigned int gold = 0;
  unsigned int gems = 0;
  rapidjson::Document serializedGameSessionJsonObject;
  rapidjson::Value jsonInformationArray;
};
#endif
