// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Game/GameSession.h"
#include "Engine/Utils/Utils.h"
#include "GameDefines.h"

void GameSession::SaveGold(int goldAmount) {
  if(goldAmount > 0){
    gold = goldAmount;
  }
}

void GameSession::SaveGems(int gemsAmount) {
  if(gemsAmount > 0) {
    gems = gemsAmount;
  }
}

void GameSession::SaveGameTimeMilliseconds(unsigned long long ms) {
  gameTimeMilliseconds = ms;
  gametimeSeconds = Utils::milli_to_sec(gameTimeMilliseconds);
}

std::string GameSession::SerializeGameSession() {
  CreateGamesessionJsonObjectAndInfoArray();
  rapidjson::Document::AllocatorType &allocator = serializedGameSessionJsonObject.GetAllocator();
  AddValueToInfoArray(kGold, gold, allocator);
  AddValueToInfoArray(kGems, gems, allocator);
  AddValueToInfoArray(kGametime, gametimeSeconds, allocator);
  AddInfoArrayToGamesessionObject(kGamesession, allocator);
  return JsonParser::JsonToString(serializedGameSessionJsonObject);
}

void GameSession::CreateGamesessionJsonObjectAndInfoArray() {
  serializedGameSessionJsonObject = JsonParser::CreateEmptyJsonDocument();
  jsonInformationArray = rapidjson::Value(rapidjson::kArrayType);
}

void GameSession::AddValueToInfoArray(const std::string &valueName, int value, rapidjson::Document::AllocatorType &allocator) {
  rapidjson::Value jsonObject(rapidjson::kObjectType);
  jsonObject.AddMember(rapidjson::Value(valueName.c_str(), allocator), rapidjson::Value(value), allocator);
  jsonInformationArray.PushBack(jsonObject, allocator);
}

void GameSession::AddInfoArrayToGamesessionObject(const std::string &gamesessionObjectName, rapidjson::Document::AllocatorType &allocator) {
  serializedGameSessionJsonObject.AddMember(rapidjson::Value(gamesessionObjectName.c_str(), allocator), jsonInformationArray, allocator);
}

unsigned int GameSession::Gold() {
  return gold;
}

unsigned int GameSession::Gems() {
  return gems;
}

unsigned int GameSession::GameTimeSeconds() {
  return gametimeSeconds;
}

unsigned long long GameSession::GameTimeMilliseconds() {
  return gameTimeMilliseconds;
}
