// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Game/GameBoard.h"
#include "Game/Factory/CardFactory.h"
#include "Game/Player.h"
#include "Engine/Singleton/EngineSingleton.h"
#ifdef SDKBOX_ENABLED
#include "PluginGPG/PluginGPG.h"
#endif


GameBoard::GameBoard(cocos2d::Scene *contextScene,std::vector<Card::CardData> selectedPlayerDeck) {
  selectedCardList = selectedPlayerDeck;
  context = contextScene;
  CreatePlayers();
  StartGame();
}

GameBoard::~GameBoard() {
  localPlayer.reset();
  opponentPlayer.reset();
  playerDeck.clear();
  selectedCardList.clear();
  CCLOG("GameBoard dtor");
}

void GameBoard::StartGame() {
  InitAndDisplayCellGrid();
  InitAndDisplayPlayerDeck();
}

void GameBoard::DisplayPlayerAvatars() {
  localPlayer->CreatePlayerAvatar();
  opponentPlayer->CreatePlayerAvatar();

  // Add avatars to game board
  cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
  cocos2d::Vec2 origin = cocos2d::Director::getInstance()->getVisibleOrigin();
  cocos2d::Size targetSize = cocos2d::Size(128, 128);
  std::string localPlayerAvatarBackgroundFilename;
  std::string opponentPlayerAvatarBackgroundFilename;

  if (localPlayer->Color() == CardColor::CARD_COLOR_BLUE) {
    localPlayerAvatarBackgroundFilename = kAvatarBackgroundBlue;
    opponentPlayerAvatarBackgroundFilename = kAvatarBackgroundRed;
  } else {
    localPlayerAvatarBackgroundFilename = kAvatarBackgroundRed;
    opponentPlayerAvatarBackgroundFilename = kAvatarBackgroundBlue;
  }

  cocos2d::Sprite *localPlayerAvatar = localPlayer->AvatarImage();
  localPlayerAvatar->setScale(targetSize.width / localPlayerAvatar->getContentSize().width, targetSize.height / localPlayerAvatar->getContentSize().height);
  cocos2d::Sprite *localPlayerAvatarBackground = cocos2d::Sprite::create(localPlayerAvatarBackgroundFilename);
  cocos2d::Vec2 localPlayerAvatarBackgroundPosition = cocos2d::Vec2(origin.x + localPlayerAvatarBackground->getContentSize().width / 2, origin.y + visibleSize.height - localPlayerAvatarBackground->getContentSize().height / 2);
  localPlayerAvatarBackground->setPosition(localPlayerAvatarBackgroundPosition);
  localPlayerAvatarBackground->addChild(localPlayerAvatar);
  localPlayerAvatar->setPosition(localPlayerAvatarBackground->getContentSize() / 2);
  context->addChild(localPlayerAvatarBackground);
  
  cocos2d::Label *localPlayerUsernameLabel = cocos2d::Label::createWithTTF(localPlayer->Name(), kFontType, kFontSizeSmall);
  localPlayerUsernameLabel->setString(localPlayer->Name());
  localPlayerUsernameLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  localPlayerUsernameLabel->setPosition(cocos2d::Vec2(origin.x + (localPlayerUsernameLabel->getContentSize().width / 2), localPlayerAvatarBackground->getPosition().y - (localPlayerAvatarBackground->getContentSize().height / 2) - (localPlayerUsernameLabel->getContentSize().height / 2)));
  context->addChild(localPlayerUsernameLabel);
  

  cocos2d::Sprite *opponentPlayerAvatar = opponentPlayer->AvatarImage();
  opponentPlayerAvatar->setScale(targetSize.width / opponentPlayerAvatar->getContentSize().width, targetSize.height / opponentPlayerAvatar->getContentSize().height);
  cocos2d::Sprite *opponentPlayerAvatarBackground = cocos2d::Sprite::create(opponentPlayerAvatarBackgroundFilename);
  cocos2d::Vec2 opponentPlayerAvatarBackgroundPosition = cocos2d::Vec2(origin.x + visibleSize.width - opponentPlayerAvatarBackground->getContentSize().width / 2, origin.y + visibleSize.height - opponentPlayerAvatarBackground->getContentSize().height / 2);
  opponentPlayerAvatarBackground->setPosition(opponentPlayerAvatarBackgroundPosition);
  opponentPlayerAvatarBackground->addChild(opponentPlayerAvatar);
  opponentPlayerAvatar->setPosition(opponentPlayerAvatarBackground->getContentSize() / 2);
  context->addChild(opponentPlayerAvatarBackground);
  
  cocos2d::Label *opponentPlayerUsernameLabel = cocos2d::Label::createWithTTF(localPlayer->Name(), kFontType, kFontSizeSmall);
  opponentPlayerUsernameLabel->setString(opponentPlayer->Name());
  opponentPlayerUsernameLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  opponentPlayerUsernameLabel->setPosition(cocos2d::Vec2(origin.x + visibleSize.width - (opponentPlayerUsernameLabel->getContentSize().width / 2), opponentPlayerAvatarBackground->getPosition().y - (opponentPlayerAvatarBackground->getContentSize().height / 2) - (opponentPlayerUsernameLabel->getContentSize().height / 2)));
  context->addChild(opponentPlayerUsernameLabel);

}

void GameBoard::CreatePlayers() {
  // Players
  CardColor thisPlayerColor;
  CardColor opponentPlayerColor;

  // Get usernames of game room participants
  playerUsername = ServiceLocator::GooglePlayService()->GooglePlayUsername();

  for (auto participant : ServiceLocator::GooglePlayService()->Gameroom()->Participants()) {
    if (participant.DisplayName() != playerUsername) {
      opponentUsername = participant.DisplayName();
    }
  }

  // Set the color of the players based on the host status
  // Host = Blue
  if (EngineSingleton::GetInstance()->IsHost()) {
    thisPlayerColor = CardColor::CARD_COLOR_BLUE;
    opponentPlayerColor = CardColor::CARD_COLOR_RED;
  } else {
    thisPlayerColor = CardColor::CARD_COLOR_RED;
    opponentPlayerColor = CardColor::CARD_COLOR_BLUE;
  }

  std::unique_ptr<Player> localPlayerInstance(new Player(playerUsername, thisPlayerColor, PLAYER_ONE));
  localPlayer = std::move(localPlayerInstance);

  std::unique_ptr<Player> opponentPlayerInstance(new Player(opponentUsername, opponentPlayerColor, PLAYER_TWO));
  opponentPlayer = std::move(opponentPlayerInstance);
  
  LoadPlayerAvatars();
}

void GameBoard::LoadPlayerAvatars() {
  for (auto player : ServiceLocator::GooglePlayService()->Gameroom()->Participants()) {
    std::string playerName = player.DisplayName();
    if (playerName == localPlayer->Name()) {
      std::string avatarUrl = player.AvatarUrl(gpg::ImageResolution::HI_RES);
      LoadAvatarImages(avatarUrl, localPlayer->Number());
    } else if (playerName == opponentPlayer->Name()) {
      std::string avatarUrl = player.AvatarUrl(gpg::ImageResolution::HI_RES);
      LoadAvatarImages(avatarUrl, opponentPlayer->Number());
    }
  }
}

void GameBoard::LoadAvatarImages(std::string &avatarUrl, PlayerNumber playerNumber) {
  CCLOG("onHttpRequestCompleted In the request");
  cocos2d::network::HttpRequest* request = new (std::nothrow) cocos2d::network::HttpRequest();
  request->setUrl(avatarUrl);
  request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
  
  if (playerNumber == PLAYER_ONE) {
    request->setResponseCallback(CC_CALLBACK_2(GameBoard::onRequestLocalPlayerImgCompleted, this));
  } else if (playerNumber == PLAYER_TWO) {
    request->setResponseCallback(CC_CALLBACK_2(GameBoard::onRequestOpponentPlayerImgCompleted, this));
  }
  
  cocos2d::network::HttpClient::getInstance()->send(request);
  request->release();
}

void GameBoard::onRequestLocalPlayerImgCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response) {
  if (!response) {
    log("onHttpRequestCompleted - No Response");
    return;
  }
  
  CCLOG("onHttpRequestCompleted - Response code: %lu", response->getResponseCode());
  
  if (!response->isSucceed()) {
    log("onHttpRequestCompleted - Response failed");
    log("onHttpRequestCompleted - Error buffer: %s", response->getErrorBuffer());
    return;
  }
  CCLOG("onHttpRequestCompleted - Response code: %s", response->getResponseDataString());
  
  std::vector<char> *buffer = response->getResponseData();
  SetAvatarImageBuffer(*buffer, PLAYER_ONE);
}

void GameBoard::onRequestOpponentPlayerImgCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response) {
  if (!response) {
    log("onHttpRequestCompleted - No Response");
    return;
  }
  
  CCLOG("onHttpRequestCompleted - Response code: %lu", response->getResponseCode());
  
  if (!response->isSucceed()) {
    log("onHttpRequestCompleted - Response failed");
    log("onHttpRequestCompleted - Error buffer: %s", response->getErrorBuffer());
    return;
  }
  CCLOG("onHttpRequestCompleted - Response code: %s", response->getResponseDataString());
  
  std::vector<char> *buffer = response->getResponseData();
  SetAvatarImageBuffer(*buffer, PLAYER_TWO);
}

void GameBoard::SetAvatarImageBuffer(std::vector<char> &imageBuffer, PlayerNumber playerNumber) {
  if(playerNumber == PLAYER_ONE) {
    localPlayer->SetAvatarImageBuffer(imageBuffer);
    playerAvatarsLoaded++;
  } else if(playerNumber == PLAYER_TWO) {
    opponentPlayer->SetAvatarImageBuffer(imageBuffer);
    playerAvatarsLoaded++;
  }
  
  if(playerAvatarsLoaded >= 2){
    //DisplayPlayerAvatars();
  }
}

void GameBoard::InitAndDisplayCellGrid() {
  cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
  cocos2d::Vec2 origin = cocos2d::Director::getInstance()->getVisibleOrigin();

  cocos2d::Vec2 center = origin + (visibleSize / 2);
  cocos2d::Vec2 gameboardCenter = cocos2d::Vec2(center.x, center.y * 0.9f);
  //cocos2d::Size gameboardSize(visibleSize.width, visibleSize.width);

  // First cell coordinates
  auto sprite = cocos2d::Sprite::create(kCardBlankFilename);
  float cardWidth = sprite->getContentSize().width;
  float cardHeight = sprite->getContentSize().height;

  // Bottom left cell
  cocos2d::Vec2 cellPosition(gameboardCenter.x - cardWidth, gameboardCenter.y - cardWidth);

  // Set all cells to empty
  for (int row = 0; row < kMaxBoardRows; row++) {
    for (int col = 0; col < kMaxBoardColumns; col++) {
      gameboardCells[row][col].m_pCard = nullptr;
      gameboardCells[row][col].IsOccupied = false;
      gameboardCells[row][col].posX = cellPosition.x;
      gameboardCells[row][col].posY = cellPosition.y;

      std::shared_ptr<Card> blankCard = CardFactory::CreateGameboardCellCard();
      PlaceCard(blankCard, &gameboardCells[row][col]);

      cellPosition.x += cardWidth;
    }

    cellPosition.x = gameboardCenter.x - cardWidth;
    cellPosition.y += cardHeight;
  }
}

void GameBoard::InitAndDisplayPlayerDeck() {
  cocos2d::Size visibleSize = cocos2d::Director::getInstance()->getVisibleSize();
  cocos2d::Vec2 origin = cocos2d::Director::getInstance()->getVisibleOrigin();

  cocos2d::Vec2 center = origin + (visibleSize / 2);

  // First cell coordinates
  float cardSpacing = visibleSize.width / (kMaxNumberOfCardsInDeck + 1);
  float horizontalCardPosition = cardSpacing;
  float cardHeight;
  float verticalCardPosition;

  for (int selectedCard = 0; selectedCard < kMaxNumberOfCardsInDeck; selectedCard++) {

    Card::CardData data = selectedCardList.at(selectedCard);
    std::shared_ptr<Card> card = CardFactory::CreateCard(data);
    card->GetSprite()->setScale(0.7f);
    card->SetColor(localPlayer->Color());

    playerDeck.push_back(card);

    // Calculate position
    cardHeight = card->GetSprite()->getContentSize().height;
    verticalCardPosition = cardHeight * 0.6f;

    card->SetInitialPosition(cocos2d::Vec2(origin.x + horizontalCardPosition, origin.y + verticalCardPosition));
    context->addChild(card->GetSprite(), 10);

    horizontalCardPosition += cardSpacing;
  }
}

void GameBoard::PlaceCardOnBoard(std::shared_ptr<Card> &card, GameBoard::Cell *cell) {
  PlaceCard(card, cell);
  
  ControllerLocator::GetAudioController()->PlaySoundEffect(kPlaceCardSuccessfulSoundEffect);

  cocos2d::ParticleSystemQuad *particleSystem = cocos2d::ParticleSystemQuad::create(kCardPlacedParicleEffect);
  particleSystem->setPosition(cell->posX, cell->posY);
  particleSystem->setAutoRemoveOnFinish(true);
  context->addChild(particleSystem);
}

void GameBoard::PlaceCard(std::shared_ptr<Card> &card, Cell *cell) {
  if (cell->m_pCard != nullptr && cell->m_pCard->GetCardName() == kCardBlankName) {
    cell->m_pCard.reset();
  }

  cell->m_pCard = card;
  cell->m_pCard->SetPosition(cell->posX, cell->posY);
  context->addChild(card->GetSprite());

  if (card->GetCardName() != kCardBlankName) {
    cell->IsOccupied = true;
  }
}

void GameBoard::FlipCard(int row, int col) {
  if (gameboardCells[row][col].IsOccupied) {
    // Change card owner and color
    CardColor cardColor;
    if (gameboardCells[row][col].m_pCard->GetCardColor() == localPlayer->Color()) {
      cardColor = opponentPlayer->Color();
    } else {
      cardColor = localPlayer->Color();
    }
    gameboardCells[row][col].m_pCard->SetColor(cardColor);
    
    // Set particle effect for card color
    std::string cardParticleEffect;
    if(cardColor == CardColor::CARD_COLOR_BLUE) {
      cardParticleEffect = kCardBlueParicleEffect;
    } else {
      cardParticleEffect = kCardRedParicleEffect;
    }
    
    if(cardParticleEffect != "null") {
      cocos2d::ParticleSystemQuad *particleSystem = cocos2d::ParticleSystemQuad::create(cardParticleEffect);
      particleSystem->setPosition(gameboardCells[row][col].m_pCard->GetSprite()->getPosition() + (gameboardCells[row][col].m_pCard->GetSprite()->getContentSize() / 2));
      particleSystem->setAutoRemoveOnFinish(true);
      context->addChild(particleSystem);
    }
  }
}

GameBoard::Cell *GameBoard::GetCellAt(int index) {
  // Calculate row and col from index
  int row = (int) (index / kMaxBoardRows);
  int col = index % kMaxBoardRows;
  return &gameboardCells[row][col];
}

bool GameBoard::IsOccupied(int cellIndex) {
  return GetCellAt(cellIndex)->IsOccupied;
}

bool GameBoard::IsFull() {
  for (int index = 0; index < (kMaxBoardRows * kMaxBoardColumns); index++) {
    // If a cell is not occupied the game is still being played
    if (!GetCellAt(index)->IsOccupied) {
      return false;
    }
  }
  return true;
}

Player *GameBoard::GetLocalPlayer() const {
  return localPlayer.get();
}

Player *GameBoard::GetOpponentPlayer() const {
  return opponentPlayer.get();
}

Card *GameBoard::GetCardInDeckAt(int cardIndex) {
  return playerDeck.at(cardIndex).get();
}

Card *GameBoard::GetCardInBoardAt(int index) {
  int row = (int) (index / kMaxBoardRows);
  int col = index % kMaxBoardRows;
  return gameboardCells[row][col].m_pCard.get();
}

Card *GameBoard::GetCardInBoardAt(int row, int column) {
  return gameboardCells[row][column].m_pCard.get();
}

Card *GameBoard::GetCardInDeckByName(std::string &name) {
  for (auto card : playerDeck) {
    if (card->GetCardName() == name) {
      return card.get();
    }
  }
  return nullptr;
}
