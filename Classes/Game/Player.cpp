// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Game/Player.h"

Player::Player(std::string &playerName, CardColor cardColor, PlayerNumber playerNumber) {
  name = playerName;
  color = cardColor;
  avatar = nullptr;
  number = playerNumber;
}

Player::~Player() {
  avatarImageBuffer.clear();
  CCLOG("Player dtor");
}

void Player::CreatePlayerAvatar() {
  cocos2d::Image *image = new cocos2d::Image();
  image->initWithImageData((const unsigned char*) avatarImageBuffer.data(), avatarImageBuffer.size());
  cocos2d::Texture2D *texture = new cocos2d::Texture2D();
  texture->initWithImage(image);
  cocos2d::Sprite *avatarSprite = cocos2d::Sprite::createWithTexture(texture);
  avatar = avatarSprite;
}

cocos2d::Sprite *Player::AvatarImage() {
  return avatar;
}

CardColor Player::Color() {
  return color;
}

std::string Player::Name() {
  return name;
}

PlayerNumber Player::Number() {
  return number;
}

void Player::SetAvatarImageBuffer(std::vector<char> &imageBuffer) {
  avatarImageBuffer = imageBuffer;
  if(!avatarImageBuffer.empty()) {
    CreatePlayerAvatar();
  } else {
    CCLOG("Player::SetAvatarImageBuffer() image buffer empty");
  }
}

