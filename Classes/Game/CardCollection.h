// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef GAME_CARD_COLLECTION_H_
#define GAME_CARD_COLLECTION_H_

#include "Game/CardCollectionInterface.h"

class CardCollection : public CardCollectionInterface {
public:
  CardCollection();
  virtual ~CardCollection();
  virtual void AddCard(std::shared_ptr<Card> card) override;
  virtual void AddCards(std::vector<std::shared_ptr<Card>> *cards) override;
  virtual void UnlockCard(std::shared_ptr<Card> card) override;
  virtual std::vector<std::shared_ptr<Card>> *UnlockedCards() override;
private:
  std::vector<std::shared_ptr<Card>> unlockedCards;
};

#endif
