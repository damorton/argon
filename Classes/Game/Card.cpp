// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Game/Card.h"
#include "Game/Player.h"
#include "UI/GameBoardUI.h"
#include "Engine/Singleton/EngineSingleton.h"

Card::Card(CardData &cardData, unsigned long long eventTimestamp) {
  data = cardData;
  timestamp = eventTimestamp;
  selectedCardRepositionValue = 20;
  isSelected = false;
  isUnlocked = false;
  
  // Card attributes
  name = cardData.cardName;
  filename = cardData.filename;
  owner = cardData.owner;
  topValue = cardData.top;
  rightValue = cardData.right;
  bottomValue = cardData.bottom;
  leftValue = cardData.left;
  quantity = cardData.quantity;
  level = cardData.cardLevel;
  
  // Sprites
  cardSprite = CreateCard(cardData.cardName, cardData.filename);
  
  blueBackgroundSprite = cocos2d::Sprite::create(kBlueBackgroundFilename);
  blueBackgroundSprite->setPosition(cardSprite->getContentSize() / 2);
  blueBackgroundSprite->setVisible(false);
  cardSprite->addChild(blueBackgroundSprite, -1);

  redBackgroundSprite = cocos2d::Sprite::create(kRedBackgroundFilename);
  redBackgroundSprite->setPosition(cardSprite->getContentSize() / 2);
  redBackgroundSprite->setVisible(false);
  cardSprite->addChild(redBackgroundSprite, -1);
  
  selectedSprite = cocos2d::Sprite::create(kCardSelectedImageFilename);
  selectedSprite->setPosition(cardSprite->getContentSize() / 2);
  selectedSprite->setVisible(false);
  cardSprite->addChild(selectedSprite, -1);
  
  SetColor(cardData.color);
  SetLevel(level);
  
  if (cardData.cardName != kCardBlankName) {
    DisplayStats();
  }
}

Card::~Card() {
  if(cardSprite) {
    cardSprite->removeFromParentAndCleanup(true);
  }
}

bool Card::IsUnlocked() {
  return isUnlocked;
}

void Card::Unlock(){
  isUnlocked = true;
}

int Card::Quantity() {
  return quantity;
}

Card::CardLevel Card::Level() {
  return level;
}

void Card::SetQuantity(int amount) {
  quantity = amount;
}

std::string Card::Owner() {
  return owner;
}

void Card::ResetPositionToDeck() {
  cardSprite->setPosition(originPositionInDeck);
}

int Card::GetAttribute(Card::Attribute attribute) {
  switch (attribute) {
    case TOP:
      return topValue;
      break;
    case RIGHT:
      return rightValue;
      break;
    case BOTTOM:
      return bottomValue;
      break;
    case LEFT:
      return leftValue;
      break;
    default:
      CCLOG("Error no card attribute");
      break;
  }
}

cocos2d::Sprite *Card::CreateCard(std::string &cardName, std::string &cardFilename) {
  auto sprite = cocos2d::Sprite::create(filename);
  sprite->retain();
  return sprite;
}

void Card::SetLevel(Card::CardLevel cardLevel){
  level = cardLevel;
  if(level == COMMON_CARD){
    auto commonCardBackground = cocos2d::Sprite::create(kCommonCardBackgroundImage);
    commonCardBackground->setPosition(cardSprite->getContentSize() / 2);
    cardSprite->addChild(commonCardBackground, -1);
  } else if(level == RARE_CARD){
    auto rareCardBackground = cocos2d::Sprite::create(kRareCardBackgroundImage);
    rareCardBackground->setPosition(cardSprite->getContentSize() / 2);
    cardSprite->addChild(rareCardBackground, -1);
  } else if(level == EPIC_CARD){
    auto epicCardBackground = cocos2d::Sprite::create(kEpicCardBackgroundImage);
    epicCardBackground->setPosition(cardSprite->getContentSize() / 2);
    cardSprite->addChild(epicCardBackground, -1);
  }
}

void Card::SetColor(CardColor cardColor) {
  color = cardColor;
  if (color == CardColor::CARD_COLOR_BLUE) {
    blueBackgroundSprite->setVisible(true);
    redBackgroundSprite->setVisible(false);
  } else if (color == CardColor::CARD_COLOR_RED) {
    redBackgroundSprite->setVisible(true);
    blueBackgroundSprite->setVisible(false);
  }
}

std::string Card::GetCardName() {
  return name;
}

std::string Card::GetFilename() {
  return filename;
}

CardColor Card::GetCardColor() {
  return color;
}

void Card::SetPosition(float posX, float posY) {
  if (cardSprite != nullptr) {
    cardSprite->setPosition(cocos2d::Vec2(posX, posY));
  }
}

cocos2d::Sprite *Card::GetSprite() {
  if (cardSprite != nullptr) {
    return cardSprite;
  }
  return nullptr;
}

unsigned long long Card::GetTimestamp() {
  return timestamp;
}

void Card::SetSelected(bool value) {
  isSelected = value;
}

void Card::SetHighlighted(bool value) {
  selectedSprite->setVisible(value);
  isSelected = value;
}

void Card::DisplayStats() {
  cocos2d::Size spriteContentSize = cardSprite->getContentSize();
  cocos2d::Color4B textColor = cocos2d::Color4B::BLACK;
  const int valuePositionPadding = 15;

  auto topValueLabel = cocos2d::Label::createWithTTF(Utils::to_str(topValue), kFontType, kFontSizeMedium);
  topValueLabel->setTextColor(textColor);
  cocos2d::Vec2 topPos(spriteContentSize.width / 2, spriteContentSize.height - topValueLabel->getContentSize().height / 2);
  topValueLabel->setPosition(topPos);
  cardSprite->addChild(topValueLabel);

  auto rightValueLabel = cocos2d::Label::createWithTTF(Utils::to_str(rightValue), kFontType, kFontSizeMedium);
  rightValueLabel->setTextColor(textColor);
  cocos2d::Vec2 rightPos(spriteContentSize.width - valuePositionPadding, spriteContentSize.height / 2);
  rightValueLabel->setPosition(rightPos);
  cardSprite->addChild(rightValueLabel);

  auto bottomValueLabel = cocos2d::Label::createWithTTF(Utils::to_str(bottomValue), kFontType, kFontSizeMedium);
  bottomValueLabel->setTextColor(textColor);
  cocos2d::Vec2 bottomPos(spriteContentSize.width / 2, bottomValueLabel->getContentSize().height / 2);
  bottomValueLabel->setPosition(bottomPos);
  cardSprite->addChild(bottomValueLabel);

  auto leftValueLabel = cocos2d::Label::createWithTTF(Utils::to_str(leftValue), kFontType, kFontSizeMedium);
  leftValueLabel->setTextColor(textColor);
  cocos2d::Vec2 leftPos(valuePositionPadding, spriteContentSize.height / 2);
  leftValueLabel->setPosition(leftPos);
  cardSprite->addChild(leftValueLabel);
}

bool Card::IsSelected() {
  return isSelected;
}

void Card::SetEnabled(bool value) {
  cardSprite->setVisible(value);
  if (value == false) {
    SetSelected(value);
  }
}

bool Card::IsEnabled() {
  return cardSprite->isVisible();
}

void Card::SetInitialPosition(cocos2d::Vec2 initialPosition) {
  originPositionInDeck = initialPosition;
  cardSprite->setPosition(initialPosition);
}

Card::CardData Card::GetCardData() {
  return data;
}
