// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef GAME_FACTORY_CARD_FACTORY_H_
#define GAME_FACTORY_CARD_FACTORY_H_

#include "Game/Card.h"

class CardFactory {
public:
  virtual ~CardFactory(){};
  static std::shared_ptr<Card> CreateGameboardCellCard();
  static std::shared_ptr<Card> CreateCard(Card::CardData &cardData, unsigned long long eventTimestamp = 0);
  static std::shared_ptr<Card> CreateRandomCardAtLevel(Card::CardLevel cardLevel, std::vector<std::shared_ptr<Card>> *cardCatalogue);
  static std::shared_ptr<Card> CreateRewardCard(std::vector<std::shared_ptr<Card>> *cardCatalogue);
private:
  CardFactory(){};
  CardFactory(const CardFactory &){}
  CardFactory &operator=(const CardFactory &){ return *this; }
};

#endif
