// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Game/Factory/CardFactory.h"
#include "Engine/Locator/ControllerLocator.h"
#include "Engine/Singleton/EngineSingleton.h"
#include <math.h>

std::shared_ptr<Card> CardFactory::CreateGameboardCellCard(){
  Card::CardData data = { kCardBlankOwner, kCardBlankName, kCardBlankFilename, CardColor::CARD_COLOR_BLANK, 0, 0, 0, 0 };
  return std::shared_ptr<Card>(new Card(data));
}

std::shared_ptr<Card> CardFactory::CreateCard(Card::CardData &cardData, unsigned long long eventTimestamp){
  return std::shared_ptr<Card>(new Card(cardData, eventTimestamp));
}

std::shared_ptr<Card> CardFactory::CreateRewardCard(std::vector<std::shared_ptr<Card> > *cardCatalogue) {
  // Generate a number between 0.01 and 1.00
  // If the number is <= 0.05
  // create an Epic card
  // If the number is <= 0.20 and > 0.05
  // create an Rare card
  // If the number is >0.20
  // create an Common card
  std::shared_ptr<Card> result = nullptr;
  float min = 0.01f;
  float max = 1.0f;
  float randValue = static_cast<float>(std::rand());
  float num = min + randValue / static_cast<float>(RAND_MAX / (max - min));
  num = roundf(num * 100) / 100;
  if(num <= 0.01){
    result = CardFactory::CreateRandomCardAtLevel(Card::EPIC_CARD, cardCatalogue);
  } else if(num < 0.05 && num > 0.01){
    result = CardFactory::CreateRandomCardAtLevel(Card::RARE_CARD, cardCatalogue);
  } else {
    result = CardFactory::CreateRandomCardAtLevel(Card::COMMON_CARD, cardCatalogue);
  }
  return result;
}

std::shared_ptr<Card> CardFactory::CreateRandomCardAtLevel(Card::CardLevel cardLevel, std::vector<std::shared_ptr<Card>> *cardCatalogue){
  std::shared_ptr<Card> result = nullptr;
  std::vector<std::shared_ptr<Card>> cardsAtLevel;
  for(auto card : *cardCatalogue) {
    if(card->Level() == cardLevel){
      cardsAtLevel.push_back(card);
    }
  }
  
  int maxIndex = (int)cardsAtLevel.size() - 1;
  int minIndex = 0;
  int randomValue = std::rand();
  int randomCardIndex = (randomValue % (maxIndex - minIndex + 1) + minIndex);
  result = cardsAtLevel.at(randomCardIndex);
  return result;
}
