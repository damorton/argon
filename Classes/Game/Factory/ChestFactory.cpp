// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Game/Factory/ChestFactory.h"
#include "Game/Chest/FreeChest.h"
#include "Game/Chest/SilverChest.h"

std::shared_ptr<Chest> ChestFactory::Create(Chest::ChestType type, unsigned long long eventTimeStamp, unsigned long long timeChestUnlocked, Chest::State state) {
  std::shared_ptr<Chest> chest;
  
  switch (type) {
    case Chest::CHEST_FREE:
      chest = std::shared_ptr<Chest>(new FreeChest(eventTimeStamp, timeChestUnlocked, state));
      break;
    case Chest::CHEST_SILVER:
      chest = std::shared_ptr<Chest>(new SilverChest(eventTimeStamp, timeChestUnlocked, state));
      break;
    default:
      break;
  }
  
  return chest;
}

