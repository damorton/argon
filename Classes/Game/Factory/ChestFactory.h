// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef ChestFactory_H_
#define ChestFactory_H_

#include "Game/Chest/Chest.h"

class ChestFactory {
public:
  virtual ~ChestFactory(){};
  static std::shared_ptr<Chest> Create(Chest::ChestType chestType, unsigned long long eventTimeStamp, unsigned long long timeChestUnlocked = 0, Chest::State state = Chest::CHEST_LOCKED);
private:
  ChestFactory(){};
  ChestFactory(const ChestFactory &){}
  ChestFactory &operator=(const ChestFactory &){return *this;}
};

#endif
