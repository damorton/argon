// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef GAME_CARD_H_
#define GAME_CARD_H_

#include "GameDefines.h"

class Player;

class Card {
public:
  
  enum CardLevel{
    COMMON_CARD,
    RARE_CARD,
    EPIC_CARD
  };

  // Card data needed to create a new card
  struct CardData {
    std::string owner;
    std::string cardName;
    std::string filename;
    CardColor color;
    int top;
    int right;
    int bottom;
    int left;
    int quantity;
    CardLevel cardLevel;
  };

  // Attiribute contants
  enum Attribute {
    TOP,
    RIGHT,
    BOTTOM,
    LEFT
  };
  
  
  Card(CardData &cardData, unsigned long long eventTimestamp = 0);
  virtual ~Card();
  cocos2d::Sprite *GetSprite();
  void SetPosition(float posX, float posY);
  std::string GetCardName();
  std::string GetFilename();
  void SetColor(CardColor cardColor);
  CardColor GetCardColor();
  unsigned long long GetTimestamp();
  void SetSelected(bool value);
  void SetHighlighted(bool value);
  bool IsSelected();
  void SetEnabled(bool value);
  bool IsEnabled();
  int GetAttribute(Attribute attribute);
  std::string Owner();
  void Unlock();
  bool IsUnlocked();
  void ResetPositionToDeck();
  void SetInitialPosition(cocos2d::Vec2 initialPosition);
  CardData GetCardData();
  int Quantity();
  void SetQuantity(int amount);
  void SetLevel(CardLevel cardLevel);
  CardLevel Level();
  
private:
  cocos2d::Sprite *CreateCard(std::string &cardName, std::string &cardFilename);
  void DisplayStats();
  
  int topValue;
  int bottomValue;
  int leftValue;
  int rightValue;
  int selectedCardRepositionValue;
  unsigned long long timestamp;
  cocos2d::Sprite *cardSprite;
  cocos2d::Sprite *redBackgroundSprite;
  cocos2d::Sprite *blueBackgroundSprite;
  cocos2d::Sprite *selectedSprite;
  bool isSelected;
  std::string owner;
  std::string name;
  std::string filename;
  CardColor color;
  cocos2d::Vec2 originPositionInDeck;
  CardData data;
  bool isUnlocked;
  int quantity;
  CardLevel level;
};

#endif
