// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef GAME_CARD_COLLECTION_INTERFACE_H_
#define GAME_CARD_COLLECTION_INTERFACE_H_

#include <memory>
#include <vector>

class Card;

class CardCollectionInterface {
public:
  virtual ~CardCollectionInterface(){}
  virtual void AddCard(std::shared_ptr<Card> card) = 0;
  virtual void AddCards(std::vector<std::shared_ptr<Card>> *cards) = 0;
  virtual void UnlockCard(std::shared_ptr<Card> card) = 0;
  virtual std::vector<std::shared_ptr<Card>> *UnlockedCards() = 0;
};

#endif
