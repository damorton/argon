// Copyright 2016 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef GAME_GAME_H_
#define GAME_GAME_H_

#include <memory>
#include "GameDefines.h"
#include "Game/Card.h"
#include "network/HttpClient.h"
#include "Game/Player.h"

class GameBoardUI;
class Player;

class GameBoard {
public:

  // Game board cell container
  struct Cell {
    std::shared_ptr<Card> m_pCard;
    bool IsOccupied;
    float posX;
    float posY;
    ~Cell() {
      m_pCard.reset();
    }
  };

  GameBoard(cocos2d::Scene *contextScene, std::vector<Card::CardData> selectedPlayerDeck);
  virtual ~GameBoard();
  
  // Starts a new game
  void StartGame();
  
  // Ends the current game
  void EndGame();
  
  // Places a card on the board on the cell given
  void PlaceCard(std::shared_ptr<Card> &card, Cell *cell);
  void PlaceCardOnBoard(std::shared_ptr<Card> &card, Cell *cell);
  
  // Flips a card during the battle phase
  void FlipCard(int row, int col);
  
  // Returns the cell indicated by the index
  Cell *GetCellAt(int index);
  
  // Checks if the cell is occupied
  bool IsOccupied(int cellIndex);
  
  // Returns the card in the deck indicated by the index
  Card *GetCardInDeckAt(int cardIndex);
  
  // Returns the card located on the board using the row and col indexing
  Card *GetCardInBoardAt(int row, int column);
  
  // Returns the card located on the board using the index
  Card *GetCardInBoardAt(int index);
  
  // Returns the games players
  Player *GetLocalPlayer() const;
  Player *GetOpponentPlayer() const;
  
  // Checks if the board is full
  bool IsFull();
  
  // Returns the card in the deck by its name
  Card *GetCardInDeckByName(std::string &name);
  
  // Create and render the players avatars
  void DisplayPlayerAvatars();
  
  void LoadPlayerAvatars();
  
  void LoadAvatarImages(std::string &avatarUrl, PlayerNumber playerNumber);
  
  void SetAvatarImageBuffer(std::vector<char> &imageBuffer, PlayerNumber playerNumber);
  void onRequestLocalPlayerImgCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
  void onRequestOpponentPlayerImgCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
  
private:
  
  // Creates the players connected to the game room
  void CreatePlayers();
  
  // Initialises and displays the game board and cells
  void InitAndDisplayCellGrid();
  
  // Initialses and displays the players deck
  void InitAndDisplayPlayerDeck();

  Cell gameboardCells[kMaxBoardRows][kMaxBoardColumns];
  std::unique_ptr<Player> localPlayer;
  std::unique_ptr<Player> opponentPlayer;
  std::string playerUsername;
  std::string opponentUsername;
  std::vector<Card::CardData> selectedCardList;
  std::vector<std::shared_ptr<Card>> playerDeck;
  cocos2d::Scene *context;
  int playerAvatarsLoaded;
};

#endif
