// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Game/CardCollection.h"
#include "Game/Card.h"

CardCollection::CardCollection() {

}

CardCollection::~CardCollection() {
  unlockedCards.clear();
}

void CardCollection::AddCard(std::shared_ptr<Card> card) {
  if(std::find_if(unlockedCards.begin(), unlockedCards.end(), [&](const std::shared_ptr<Card> cardRef)
                  {
                    return cardRef->GetCardName() == card->GetCardName();
                  }) != unlockedCards.end()) {
                    card->SetQuantity(card->Quantity() + 1);
                  } else {
                    card->Unlock();
                    card->SetQuantity(1);
                    unlockedCards.push_back(card);
                  }
}

void CardCollection::UnlockCard(std::shared_ptr<Card> card) {
  unlockedCards.push_back(card);
}


std::vector<std::shared_ptr<Card>> *CardCollection::UnlockedCards() {
  return &unlockedCards;
}

void CardCollection::AddCards(std::vector<std::shared_ptr<Card>> *cards) {
  for(auto card : *cards) {
    AddCard(card);
  }
}
