// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Game/Chest/FreeChest.h"
#include "Engine/Utils/Utils.h"

FreeChest::~FreeChest() {
  
}

FreeChest::FreeChest(unsigned long long eventTimeStamp, unsigned long long timeChestUnlocked, State state) {
  timeStamp = eventTimeStamp;
  uuid = Utils::to_str(timeStamp);
  chestState = state;
  unlockWaitTime = 0; // 0 seconds
  
  // If we win a chest from battle it will have a 'timeChestUnlocked' value of 0
  timeUnlocked = 0;
  if(timeChestUnlocked > 0) {
    timeUnlocked = timeChestUnlocked;
    UpdateTimeRemaining();
  }
  
  Init();
}

void FreeChest::Init() {
  type = CHEST_FREE;
  timeRemaining = 0;
  costToOpenGems = 0;
  stars = 1;
  gold = 50;
  gems = 3;
  numberOfCardsInChest = 1;
  chestState = CHEST_READY_TO_OPEN;
  sprite = cocos2d::Sprite::create(kFreeChestFilename);
  if(sprite) {
    sprite->retain();
    sprite->setName(uuid);
  }
  GenerateCards();
}


