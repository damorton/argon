// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef GAME_CHEST_H_
#define GAME_CHEST_H_

#include "GameDefines.h"

class Card;
class RewardUI;

class Chest {
public:
  // Chest types for rewards
  enum ChestType {
    CHEST_FREE, // every 4 hours - instant unlock - 2 cards, 40 - 60 gold, 2 - 3 gems
    CHEST_SILVER, // won from battles if slots available - 3 hours / 18 gems cards and gold
    CHEST_BATTLE, // once per day when 5 battles won - 2 cards, 60 - 100 gold, 2 - 3 gems
    CHEST_GOLD, // won from battles if slots available - 8 hours / 48 gems card and gold
    CHEST_RUBY // won from battles if slots available - 12 hours / 72 gems cards and gold - bought in shop - instant
  };
  
  enum State {
    CHEST_LOCKED,
    CHEST_UNLOCKED,
    CHEST_READY_TO_OPEN
  };

  /**
   * Initialise the chest, this is call once the chest is instantiated.
   */
  virtual void Init() = 0;
  virtual ~Chest();
  unsigned long long GetTimeStamp();
  unsigned long long TimeUnlocked();
  ChestType GetType();
  std::vector<std::shared_ptr<Card>> *Cards();
  int Gold();
  int Gems();
  int Stars();
  cocos2d::Sprite *Sprite();
  int TimeRemainingSeconds();
  State GetState();
  int CostToOpen();
  std::string Id();
  void Unlock();
  void SetReadyToOpen();
  int UnlockWaitTimeSeconds();

protected:
  void GenerateCards();
  void UpdateTimeRemaining();
  
  int gold;
  int gems;
  int stars;
  int numberOfCardsInChest;
  int costToOpenGems;
  unsigned long long timeUnlocked;
  unsigned long long timeStamp;
  unsigned long long unlockWaitTime;
  long long timeRemaining;
  cocos2d::Sprite *sprite;
  cocos2d::Label *timeRemainingLabel;
  ChestType type;
  std::string uuid;
  std::vector<std::shared_ptr<Card>> cards;
  State chestState;
};
#endif
