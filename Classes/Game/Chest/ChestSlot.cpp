#include "Game/Chest/ChestSlot.h"
#include "Game/Chest/Chest.h"
#include "Engine/Utils/Utils.h"

const std::string kGemLabelContainer = "gem_label_container";

ChestSlot::ChestSlot(std::string spriteFilename) {
  sprite = cocos2d::Sprite::create(spriteFilename);
  labelBackground = cocos2d::Sprite::create(kChestSlotLabelBackgroundFilename);
  labelBackground->setVisible(false);
  labelBackground->setPosition(sprite->getContentSize().width / 2, sprite->getContentSize().height);
  sprite->addChild(labelBackground);
  timeRemainingLabel = cocos2d::Label::createWithTTF("UNLOCK", kFontType, kFontSizeSmall);
  labelBackground->setColor(cocos2d::Color3B(0, 180, 255)); // BLUE
  timeRemainingLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  timeRemainingLabel->retain();
  currentChest = nullptr;
  isChestSetToOpen = false;
  activeChestSlot = true;
  gemLabelInfoContainer = nullptr;
}

ChestSlot::~ChestSlot() {
  timeRemainingLabel->release();
}

std::shared_ptr<ChestSlot> ChestSlot::Create(std::string spriteFilename) {
  return std::make_shared<ChestSlot>(spriteFilename);
}

cocos2d::Sprite *ChestSlot::Sprite() {
  return sprite;
}

cocos2d::Label *ChestSlot::SlotLabel() {
  return timeRemainingLabel;
}

void ChestSlot::SetChest(std::shared_ptr<Chest> chest) {
  currentChest = chest;
  labelBackground->setVisible(true);
}

std::shared_ptr<Chest> ChestSlot::CurrentChest() {
  return currentChest;
}

void ChestSlot::Update() {
  if(currentChest){
    if(currentChest->GetState() == Chest::CHEST_UNLOCKED) {
      timeRemainingLabel->setString(Utils::seconds_to_time_format(currentChest->TimeRemainingSeconds()));
    }
    
    if(currentChest->GetState() == Chest::CHEST_UNLOCKED && activeChestSlot) {
      activeChestSlot = false;
      sprite->setColor(cocos2d::Color3B::GREEN);
      labelBackground->setColor(cocos2d::Color3B::GREEN);
      DisplayBuyWithGemsLabels();
    }
    
    if(currentChest->GetState() == Chest::CHEST_READY_TO_OPEN && !isChestSetToOpen){
      isChestSetToOpen = true;
      timeRemainingLabel->setString("OPEN");
      labelBackground->setColor(cocos2d::Color3B::YELLOW);
      sprite->setColor(cocos2d::Color3B::YELLOW);
      sprite->removeChild(gemLabelInfoContainer, true);
    }
  }
}

void ChestSlot::DisplayBuyWithGemsLabels() {
  gemLabelInfoContainer = cocos2d::Node::create();
  gemLabelInfoContainer->setName(kGemLabelContainer);
  
  auto openWithGemsButtonLabel = cocos2d::Label::createWithTTF("OPEN NOW", kFontType, kFontSizeSmallPlus);
  openWithGemsButtonLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  openWithGemsButtonLabel->setPosition(sprite->getContentSize().width * 0.50, sprite->getContentSize().height * 0.40 );
  gemLabelInfoContainer->addChild(openWithGemsButtonLabel);
  
  std::string gemCost = Utils::to_str(currentChest->CostToOpen());
  auto openWithGemsCostLabel = cocos2d::Label::createWithTTF(gemCost, kFontType, kFontSizeMedium);
  openWithGemsCostLabel->enableOutline(cocos2d::Color4B::BLACK, kTextOutlineSize);
  openWithGemsCostLabel->setPosition(sprite->getContentSize().width * 0.40, sprite->getContentSize().height * 0.20);
  gemLabelInfoContainer->addChild(openWithGemsCostLabel);
  
  auto gemSprite = cocos2d::Sprite::create(kGemFilename);
  gemSprite->setPosition(sprite->getContentSize().width * 0.70, sprite->getContentSize().height * 0.20);
  gemSprite->setScale(0.35f);
  gemLabelInfoContainer->addChild(gemSprite);
  
  sprite->addChild(gemLabelInfoContainer, 1);
  labelBackground->setColor(cocos2d::Color3B::GREEN);
}
