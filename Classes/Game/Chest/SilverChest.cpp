// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Game/Chest/SilverChest.h"
#include "Engine/Utils/Utils.h"

SilverChest::~SilverChest() {
  
}

SilverChest::SilverChest(unsigned long long eventTimeStamp, unsigned long long timeChestUnlocked, State state) {
  timeStamp = eventTimeStamp;
  uuid = Utils::to_str(timeStamp);
  chestState = state;
  unlockWaitTime = 10800000; // 3 hours
  
  // If we win a chest from battle it will have a 'timeChestUnlocked' value of 0
  timeUnlocked = 0;
  if(timeChestUnlocked > 0) {
    timeUnlocked = timeChestUnlocked;
    UpdateTimeRemaining();
  }
  
  Init();
}

void SilverChest::Init(){
  type = CHEST_SILVER;
  numberOfCardsInChest = 3;
  timeRemaining = 0;
  costToOpenGems = 0;
  stars = 1;
  gold = 21;
  gems = 3;
  costToOpenGems = 18;
  sprite = cocos2d::Sprite::create(kSilverChestFilename);
  if(sprite) {
    sprite->retain();
    sprite->setName(uuid);
  }
  GenerateCards();
}
