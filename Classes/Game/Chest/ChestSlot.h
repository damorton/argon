#ifndef UI_CHEST_SLOT_H_
#define UI_CHEST_SLOT_H_

#include "cocos2d.h"

class Chest;

class ChestSlot {
public:
  enum State {
    CHEST_SLOT_ACTIVE,
    CHEST_SLOT_EMPTY
  };
  
  ChestSlot(std::string spriteFilename);
  virtual ~ChestSlot();
  static std::shared_ptr<ChestSlot> Create(std::string spriteFilename);
  cocos2d::Sprite *Sprite();
  cocos2d::Label *SlotLabel();
  std::shared_ptr<Chest> CurrentChest();
  void SetChest(std::shared_ptr<Chest> chest);
  void Update();
  
private:
  void UpdateTimeLabel();
  void DisplayBuyWithGemsLabels();

  cocos2d::Sprite *sprite;
  cocos2d::Sprite *labelBackground;
  cocos2d::Label *timeRemainingLabel;
  int time;
  std::shared_ptr<Chest> currentChest;
  bool isChestSetToOpen;
  bool activeChestSlot;
  cocos2d::Node *gemLabelInfoContainer;
};

#endif
