// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#ifndef GAME_CHEST_FREE_CHEST_H_
#define GAME_CHEST_FREE_CHEST_H_

#include "Game/Chest/Chest.h"

class FreeChest : public Chest {
public:
  FreeChest(unsigned long long eventTimeStamp, unsigned long long timeChestUnlocked, State state);
  virtual ~FreeChest();
private:
  virtual void Init() override;
};

#endif
