// Copyright 2017 David Morton. All rights reserved.
// Use of this source code is governed by a license that can be
// found in the LICENSE file.

#include "Game/Chest/Chest.h"
#include "Game/Card.h"
#include "UI/RewardUI.h"
#include "Engine/JsonParser/JsonParser.h"
#include "Engine/Singleton/EngineSingleton.h"
#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#endif

Chest::~Chest() {
  if(sprite) {
    sprite->release();
    sprite->removeFromParent();
  }
  cards.clear();
  CCLOG("Chest dtor called");
}

void Chest::GenerateCards() {
  std::vector<std::shared_ptr<Card>> *cardCatalogue = ControllerLocator::GetConfigController()->GetCardCatalogue();
  for(int i = 0; i < numberOfCardsInChest; i++) {
    std::shared_ptr<Card> card = CardFactory::CreateRewardCard(cardCatalogue);
    cards.push_back(card);
  }
}

int Chest::CostToOpen() {
  return costToOpenGems;
}

std::string Chest::Id() {
  return uuid;
}

int Chest::TimeRemainingSeconds() {
  UpdateTimeRemaining();
  int seconds = static_cast<int>(timeRemaining / 1000);
  return seconds;
}

void Chest::UpdateTimeRemaining() {
  unsigned long long timeCreatedDelta = EngineSingleton::GetInstance()->GetTimestamp() - timeUnlocked;
  timeRemaining = unlockWaitTime - timeCreatedDelta;
  if(timeRemaining < 0) {
    timeRemaining = 0;
    chestState = CHEST_READY_TO_OPEN;
  }
}

void Chest::Unlock(){
  chestState = CHEST_UNLOCKED;
  timeUnlocked = EngineSingleton::GetInstance()->GetTimestamp();
  std::shared_ptr<GameEvent> gameEvent = GameEventFactory::Create(GameEvent::EVENT_SAVE_GAME_CONFIG);
  EngineSingleton::GetInstance()->Notify(gameEvent.get());
}

void Chest::SetReadyToOpen(){
  chestState = CHEST_READY_TO_OPEN;
}

int Chest::UnlockWaitTimeSeconds() {
  return (int)unlockWaitTime / 1000;
}

cocos2d::Sprite *Chest::Sprite() {
  return sprite;
}

Chest::ChestType Chest::GetType() {
  return type;
}

unsigned long long Chest::GetTimeStamp() {
  return timeStamp;
}

unsigned long long Chest::TimeUnlocked() {
  return timeUnlocked;
}

Chest::State Chest::GetState() {
  return chestState;
}

std::vector<std::shared_ptr<Card>> *Chest::Cards() {
  return &cards;
}

int Chest::Gold() {
  return gold;
}

int Chest::Gems() {
  return gems;
}

int Chest::Stars() {
  return stars;
}
